#include "time.h"

void *getctime_(time_t *tod);
double subctime_(time_t *tod0, time_t *tod1);

void *getctime_(time_t *tod)
{
  time(tod);
}

double subctime_(time_t *tod0, time_t *tod1)
{
  return difftime(*tod1,*tod0);
}
