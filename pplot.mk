PROJECT=int2

OBJSUF=o
FC=f90 -c -w
FFLAGS=-fast
FLINK=f90 -w -o $(PROJECT)

.PHONY: $(PROJECT) clean list

.f.$(OBJSUF):
	$(FC) $(FFLAGS) $<

.SUFFIXES: .$(OBJSUF) .f .c

vpath %.f .: ../bin

FSRCS   =   pplot_dr.f pplot.f gpheat.f prini.f

OBJS    =  $(.f=.$(OBJSUF)) $(FSRCS:.f=.$(OBJSUF)) 

$(PROJECT): $(OBJS)
	rm -f $(PROJECT)
	$(FLINK) $(OBJS)
	./$(PROJECT)

clean:
	rm -f $(OBJS)

list: $(FSRCS)
	echo $^

pack: $(FSRCS)
	cat $^ > _tmp_.pack.f
