cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c 
c        this is the end of the debugging code and the beginning of
c        the actual subroutines for the evaluation of the functions
c        P_n^m. The subroutines pnmini and pnmeva are supposed to be
c        the only user-accessible ones.
c 
c 
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c 
c 
c 
        subroutine pnmini(nmax,w)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1)
c 
c       this is the initialization subroutine for the subroutine
c       pnmeva (see) evaluating a collection of associated Legendre
c       functions at a point on the interval (-1,1).
c 
c                  input parameters:
c 
c  nmax - the highest degree of the Legendre polynomial that the
c          subroutine will be expected to evaluate
c 
c                  output parametrs:
c 
c  w - array containing various types of information to be used by
c          the subroutine pnmeva; the subroutine uses 6*nmax + 50
c          real *8 elements; these should not be changed between the
c          call to this subroutine and subsequent calls to the subroutine
c          pnmeva (see)
c 
c 
c        . . . allocate memory for the subroutine pnmini0
c 
        icmm=12
        lcmm=nmax+6
c 
        icmmp1=icmm+lcmm
        lcmmp1=nmax+6
c 
        iarrints=icmmp1+lcmmp1
        larrints=nmax+6
        larrints=larrints*2
c 
        iarrhalfs=iarrints+larrints
        larrhalfs=nmax+6
        larrhalfs=larrhalfs*2
c 
c        initialize the subroutine for the evaluation of
c        normalized assocated Legendre functions
c 
        call pnmini0(nmax+2,w(icmm),w(icmmp1),w(iarrints),
     1      w(iarrhalfs))
c
        w(1)=icmm
        w(2)=icmmp1
        w(3)=iarrints
        w(4)=iarrhalfs
        w(5)=nmax
c 
        return
        end
c 
c 
c 
c 
        subroutine pnmeva(ier,x,n,m,rs,w,ic)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1),rs(1),ic(1)
c 
c        this subroutine evaluates the functions R_k^m, with
c        k=m, m+1, . . . ,n; here R_k^m is the normalized version
c        of the associated Legendre function P_k^m. The functions
c        R_k^m are evaluated at the point x \in (-1,1) that is
c        supplied by the user.
c 
c 
c                    Input parameters:
c 
c  x - the point where the functions R_k^m are to be evaluated
c  n - the largest value of k for which the functions
c        R_k^m are to be evaluated.
c  m - the value of the second parameter for which the functions
c        R_k^m are to be evaluated.
c  w - the array produced by the initializaion subroutine pnmini(see)
c 
c                    Output parameters:
c 
c  ier - error return code;
c              ier=0 means successful execution of the subroutine
c              ier=4 means that n > m; this is a fatal error
c              ier=16 means that n > nmax specified in the entry
c              pnmini; this is a fatal error
c  rs -  values at the point x of normalized associated Legendre
c        functions P_k^m, with k=m,m+1,. . . ,n. Note that the value
c        of the k-th Legendre function is found in the k-th element
c        of the array rs; thus, the first m-1 elements of rs are not
c        changed by the subroutine.
c
c       work:
c       ic -- must be at least n+1 integer*4 elements long
c 
c        . . . retrieve the locations of various arrays in w from
c              the beginning of w
c 
        icmm=nint(w(1))
        icmmp1=nint(w(2))
        iarrints=nint(w(3))
        iarrhalfs=nint(w(4))
        nmax=nint(w(5))
c 
c        evaluate the requested normalized P_n^m with the
c        user-specified m
c 
        ier=0
        if(n .le. nmax) goto 1200
        ier=16
        return
 1200 continue
c 
        if(n .ge. m) goto 1400
        ier=4
        return
 1400 continue
c 
        call pnmeva0(x,m,n,w(icmm),w(icmmp1),w(iarrints),
     1      w(iarrhalfs),rs,ic(2))
c 
        return
        end
c 
c 
c 
c 
        subroutine pnmini0(nmax,cmm,cmmp1,arrints,arrhalfs)
        implicit real *8 (a-h,o-z)
        save
        dimension cmm(0:1),cmmp1(0:1),arrints(0:1),
     1    arrhalfs(0:1)
c 
c       construct the arrays cmm, cmmp1
c 
        done=1
        half=done/2
c 
        cmm(0)=1
        cmmp1(0)=dsqrt(half)
c 
        do 1400 i=1,nmax
c 
        cmm(i)=-cmm(i-1)*dsqrt( (2*i-done)*2*i)/(i*2)
c 
        cmmp1(i)=
     1   -cmmp1(i-1)*dsqrt( (2*i+done)*(2*i+2) )/(i+1)/2
c 
 1400 continue
c 
        do 1600 i=0,nmax
c 
        cmm(i)=cmm(i)*dsqrt(i+half)
c 
        cmmp1(i)=cmmp1(i)*dsqrt( (2*i+2)*(i+3*half) )
 1600 continue
c 
c       construct the arrays of square roots of
c       integers and half-integers
c 
        do 1800 i=0,nmax*2
c 
        arrints(i)=dsqrt(i*done)
c 
        if(i .eq. 0) goto 1800
        arrhalfs(i)=dsqrt(i-half)
 1800 continue
c 
        return
        end
c 
c 
c 
c 
        subroutine pnmeva0(x,m,n,cmm,cmmp1,arrints,arrhalfs,rs,ic)
        implicit real *8 (a-h,o-z)
        dimension cmm(0:1),cmmp1(0:1),arrints(0:1),
     1    arrhalfs(0:1),rs(1),ic(0:1)
c
c 
c       calculate the normalized P_m^m and P_(m+1)^m
c 
        done=1
c
c       rmax will help account for underflow;
c       set it to any power of 2 such that
c       1/rmax**5 is below the desired precision
c       but does not underflow
c
        rmax = (done*2)**32
c
c       form common = sqrt(1-x**2)**m,
c       accounting for underflow with ic
c
        factor = sqrt(1-x**2)
        common = 1
        ic(m) = 0
        do j = 1,m
          common = common*factor
          if(common .lt. 1/rmax) then
            common = common*rmax
            ic(m) = ic(m)+1
          endif
        enddo ! j
c 
        rmm = common*cmm(m)
c
        rmmp1 = common*x*cmmp1(m)
c 
c
c       conduct the whole recursion
c 
        rs(m)=rmm
        if(n .gt. m) then
          ic(m+1) = ic(m)
          rs(m+1) = rmmp1
        endif
c 
        do j = m+1,n-1
          rat1 = (2*j+1)*arrints(j-m+1)*arrhalfs(j+2)
     1         / ( arrhalfs(j+1)*arrints(j+m+1)*(j-m+done) )
          rat2 = arrints(j+m)*arrints(j-m)*arrhalfs(j+2)
     1         / ( arrhalfs(j)*arrints(j-m+1)*arrints(j+m+1) )
c
          ic(j+1) = ic(j)
          rs(j+1)=rat1*x*rs(j)-rat2*rs(j-1)/rmax**(ic(j-1)-ic(j))
          if(abs(rs(j+1)) .gt. rmax) then
            ic(j+1) = ic(j+1)-1
            rs(j+1) = rs(j+1)/rmax
          endif
        enddo ! j
c
        do j = m,n
          if(ic(j) .gt. 5) rs(j) = 0
          if(ic(j) .le. 5) rs(j) = rs(j)/rmax**ic(j)
        enddo ! j
c
c
        return
        end
