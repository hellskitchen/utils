c
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       End of debugging code 
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine lagfuns(x,nmax,funs,scales)
        implicit real *8 (a-h,o-z)
        real *8 funs(1),scales(1)
c
c       calculates the first nmax+1 normalized laguerre functions,
c       i.e. the laguerre polynomials * exp(-x/2).  recursion
c       coefficients are computing every time, no effort was made
c       to accelerate the subroutine
c
c       work array - scales is work array, needs to be at least
c           nmax+5 real *8 elements long
c
        done=1
        pi=atan(done)*4 
c
c       Setup for n=0
c
        thres=50
        big=exp(thres)
        scale=0
c
        do 1200 i=1,nmax+1
        scales(i)=0
 1200   continue
c
        funs(1)=1
        funs(2)=1-x
c
        do 2200 i=2,nmax
c
c       Recursion coeffiecients for Laguerre polynomials
c       (Abramovitz and Stegnun, p. 782, normalized only for alpha=0 ) 
c
        c1=(2*(i-1)+1-x)/i
        c2=-done*(i-1)/i
c
        funs(i+1)=c1*funs(i)+c2*funs(i-1)
c
cccc        call prin2('c1=*',c1,1)
cccc        call prin2('c2=*',c2,1)
cccc        call prin2('funs(i+1)=*',funs(i+1),1)
c
        scales(i+1)=scales(i)
c
        dd=abs(funs(i+1))
        if (dd .lt. big) goto 2200
c
        funs(i+1)=funs(i+1)/big
        funs(i)=funs(i)/big
        scales(i+1)=scales(i+1)+1
        scales(i)=scales(i)+1
c
 2200 continue
c
c       now take into account the scaling control
c
        do 2400 i=1,nmax+1
c
        vallog=scales(i)*thres+log(abs(funs(i)))-x/2
        val=exp(vallog)
        if (funs(i) .lt. 0) val=-exp(vallog)
c
        funs(i)=val
 2400   continue
c
        return
        end
c
c
c
c
c
        subroutine lagfunder(x,n,val,der)
        implicit real *8 (a-h,o-z)
c
c       calculate n^th degree laguerre function and its
c       derivative
c
c
        done=1
        two=2
        pi=atan(done)*4 
c
c       Setup for n=0
c
        thres=50
        big=exp(thres)
cccc        big=1d40
        scale=0
c
        fnminus=0
        fn=1
c
        do 2050 i=0,n-1
c
c       Recursion coeffiecients for Laguerre polynomials
c       (Abramovitz and Stegnun, p. 782, normalized only for alpha=0 ) 
c
        a1=i+1
        a2=2*i+1
        a3=-1
        a4=i
c
        fnplus= ((a2+a3*x)*fn-a4*fnminus)/a1
c        
cccc        call prin2('fnplus=*',fnplus,1)
c
        fnminus=fn
        fn=fnplus
c
        if (abs(fn)>big) then
        fn=fn/big
        fnminus=fnminus/big
        scale=scale+1      
        endif
c
 2050 continue
c
c       now take into account the scaling control
c
        vallog=scale*thres+log(abs(fn))-x/2
        val=exp(vallog)
        if (fn .lt. 0) val=-exp(vallog)  
c
        two=2 
        derp=(n*fn-n*fnminus)/x
        derlog=scale*thres+log(abs(derp))-x/2
c
        diffnormlog2=-x/2-log(two)+scale*thres+log(abs(fn)) 
c         
        der=-exp(diffnormlog2)
        if (fn .lt. 0) der=-der 
c       
        if (derp .lt. 0) der=der-exp(derlog)
        if (derp .gt. 0) der=der+exp(derlog) 
c
        return
        end 
c
c
c
c
c
        subroutine lagwhts(n,xs,whts,w)
        implicit real *8 (a-h,o-z)
        real *8 xs(1),whts(1),w(1)
c
c       this subroutine constructs the roots and weights of the
c       laguerre gaussian quadrature, w needs to be as big as n
c
        call lagrts(n,xs,w)
c
c       w contains the derivatives, now make the weights
c
        do 1400 i=1,n
        whts(i)=exp(-xs(i))/xs(i)/w(i)**2
cccc        vlog=-xs(i)-log(xs(i))-log(w(i)**2)
cccc        whts(i)=exp(vlog)
 1400   continue

c
        return
        end
c
c
c
c
c
        subroutine lagrts(n,xs,polders)
        implicit real *8 (a-h,o-z)
        real *8 xs(1),polders(1)
c
c       this subroutine calculates the n roots of the n^th degree
c       laguerre polynomial.  outputs the roots in xs, and outputs
c       the derivative of the n^th degree laguerre polynomial at the
c       roots in polders
c
c       need to get an estimate of the last root, start with an
c       approximation of the first root, then march through the ode
c       to get to the last root
c
        done=1
        pi=atan(done)*4 
c
c       coefficients of diff eq for Laguerre functions
c
        a0=0
        b0=done*(2*n+1)/2
        c0=-done/4
c
        a1=0
        b1=1
        c1=0
c
        a2=0
        b2=0
        c2=1
c
c       start left of first zero
c       use Pruefer to march to first zero
c
        start=done/(4*n)
c
        x=start
        call lagfunder(x,n,val,der)
c
        p=a2+b2*x+c2*x**2
        r=a0+b0*x+c0*x**2
c
        phistart=atan(sqrt(p/r)*der/val)     
        step=pi/2+phistart
c
        m=4 
        call lag_odemarch
     1      (start,phistart,m,step,a2,b2,c2,a1,b1,c1,a0,b0,c0,zero)
c
cccc        call prin2('first zero after lag_odemarch, zero=*',zero,1)
c
c       Increase precision with Newton
c
c       Note: recurrence relation for Laguerre not accurate
c       for small x.
c
        x=zero
c
        do 1200 i=1,10 000
c
        call lagfunder(x,n,val,der)
        h=-val/der
        x=x+h
c
        if (abs(val/der) .lt. 1d-9) goto 1400
c
 1200   continue
 1400   continue
c
c       do 2 more newtons for good measure
c
        do 1600 i=1,2
        call lagfunder(x,n,val,der)
        h=-val/der
        x=x+h
 1600   continue
c
        xs(1)=x
c
        call lagfunder(x,n,val,der)
c
cccc        call prin2('first zero after newton, x=*',x,1)
cccc        call prin2('after newton, val/der=*',val/der,1) 
c
c       now lag_odemarch to get the rest of the roots
c
        do 2000 k=1,n-1
c
        phistart=pi/2     
        step=pi
        m=4 
        start=x
        call lag_odemarch
     1      (start,phistart,m,step,a2,b2,c2,a1,b1,c1,a0,b0,c0,zero)
c
cccc        call prin2('after lag_odemarch, zero=*',zero,1)
c
        x=zero
        xs(k+1)=x
 2000   continue
c
c       Increase precision with Newton
c
c       Note: recurrence relation for Laguerre not accurate
c       for small x.
c
        x=xs(n)
c
        do 2200 i=1,10 000
        call lagfunder(x,n,val,der)
        h=-val/der
c
        x=x+h
        if (abs(val/der/x) .le. 1d-9) goto 2400
c
 2200   continue
 2400   continue
c
        do 2600 i=1,2
        call lagfunder(x,n,val,der)
        h=-val/der
        x=x+h
 2600   continue
c
        xs(n)=x
c
        call lagfunder(x,n,val,der)
cccc        call prin2('largest zero after newton, x=*',x,1)
cccc        call prin2('after newton, val/der=*',val/der,1) 
c
        valstart=val
        derstart=der
        start=x
        ext=0
c
        call lag_specfunzeros(n,start,valstart,derstart,
     1      ext,a2,b2,c2,a1,b1,c1,a0,b0,c0,xs,polders)
c
        return
        end
c
c
c
c
c
       subroutine lag_specfunzeros(Nzeros,start,valstart,derstart,
     1     ext,a2,b2,c2,a1,b1,c1,a0,b0,c0,zeros,polders)
       implicit real *8 (a-h,o-z)
       dimension zeros(1),ders(10000),polders(1)                   
c
c      Calculates roots of special functions 
c 
c      input: 
c          Nzeros - number of non-neg roots
c          start - extreme value or root <= all non-neg roots
c          ext - 0 if start is root, 1 if start is extrem value 
c   
c          a0-c2: coefficients in ODE for special function u:
c
c          (a2+b2*x+c2*x**2) * u''(x) + (a1+b1*x+c1*x**2) * u'(x)
c          + (a0+b0*x+c0*x**2) * u(x) =0; 
c
c      output: 
c          zeros - non-neg roots
c
c      differs from other specfunzeros in that it starts at the
c      largest root and works backwards (i.e. to the left)
c
       one=1
       pi=atan(one)*4 
       z0=0
c
c      number of derivatives in taylor expansion:
c
       nders=30
c
c      number of steps in ODE from one root to next:
c
       n=4
c
c      Setup inital value at Extremum or Zero
c
       zeros(nzeros)=start
       der=derstart
       polders(nzeros)=der
cccc       call prin2('derstart=*',derstart,1)
c
       l=nzeros
       fext=0
c
       k=0
       phib=pi/2
c
       dl=l
ccc       call prin2('dl=*',dl,1)
       dll=sqrt(dl*(dl+1))
c
       nmed=nzeros-1
c
       do 500 i=1,nmed
c
cc          call lag_odemarch(zeros(k),phib,n,-pi,a2,b2,c2,a1,
cc     1                 b1,c1,a0,b0,c0,zero)
c
           zero=zeros(l-k-1)
c
cc          call prin2('lag_odemarch, zero=*',zero,1)
cc          call prin2('zero from pruefer=*',zero,1)
c
          z0=0
cc          scale=abs(zeros(k)-zero)
          scale=1
c
          call lag_specfunders(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      zeros(l-k),z0,scale,der,ders,nders)
c
cccc          call prin2('ders=*',ders,nders)
c
          call lag_newton(zero,zeros(l-k),z0,ders,scale,nders,uprime)
c         
          der=uprime          
          k=k+1
          zeros(l-k)=zero
          polders(l-k)=der
c
cc          call prin2('zero from newton=*',zero,1)
c
 500   continue
c
       return
       end
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine lag_odemarch
     1    (start,phistart,n,step,a2,b2,c2,a1,b1,c1,a0,b0,c0,zero)
       implicit real *8 (a-h,o-z)
c   
c
       one=1
       pi=atan(one)*4 
c
cccc       CALL prini(6,13)
c      
C      Solve ODE of form dt/dphi=lag_fode(phi,t) using a second order scheme
C      in Interval [start,stop]
c
c 
       h=-step/n
       phib=phistart
       tb=start
c
       call lag_fode(tb,phib,a2,b2,c2,a1,b1,c1,a0,b0,c0,derb)
c
       do 1000 i=1,n 
c
       phie=phib+h
       ttemp=tb+derb*h
       call lag_fode(ttemp,phie,a2,b2,c2,a1,b1,c1,a0,b0,c0,dere)
       te=tb+(derb+dere)*h*0.5
c     
       derb=dere

cc       call prin2('derb=*',derb,1)
       tb=te
       phib=phie

cc       call prin2('te=*',te,1)
c
 1000  continue 
c
       zero=te
c
       return
       end
c 
cccccccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine lag_fode(x,phi,a2,b2,c2,a1,b1,c1,a0,b0,c0,val)
       implicit real *8 (a-h,o-z)
       save
       dimension stab(10000)
       data ifcalled/0/
c
c
c      a0-c2: coefficients in ode for special function u:
c
c      (a2+b2*x+c2*x**2) * u''(x) + (a1+b1*x+c1*x**2) * u'(x)
c      + (a0+b0*x+c0*x**2) * u(x) =0; 
c
       if (ifcalled .eq. 1) goto 1000
c
       pi=atan(1d0)*4 
       nn=1000
c
       h=2*pi/nn
       hh=1/h
       do 1200 i=1,nn
c        
          xx=-pi+i*h
          stab(i)=sin(xx)
c
 1200  continue
c
       ifcalled=ifcalled+1
 1000  continue
c
       xx2=x*x
c
       p=a2+b2*x+c2*xx2
       q=a1+b1*x+c1*xx2
       r=a0+b0*x+c0*xx2
c
c
c
       pprime=b2+2*c2*x
       rprime=b0+2*c0*x
c
       temp1=(pprime*r-rprime*p-2*r*q)/(4*p*r)


cc       temp1=(pprime*r-2*r*q)
       tphi=2*phi
       kk=(tphi-pi)*hh
       temp1=temp1*stab(kk)
       temp2=4*p*r
       val=temp2/(temp1-temp2*sqrt(r/p))
c    
       return
       end
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
        subroutine lag_specfunders(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      x,phi,scale,der,ders,nders)
c
        implicit real *8 (a-h,o-z)
        dimension ders(1),facs(100)
        data ifcalled/0/
c
c        This subroutine uses recursion to evaluate the 
c        coefficients of the Taylor series for the user-specified 
c        special function at the point x, given the values of the
c        function and its first derivative. The said special function 
c        is defined as the solution of the equation 
c
c        (a2+b2*x+c2*x**2) * phi''(x) + (a1+b1*x+c1*x**2) * phi'(x)
c        + (a0+b0*x+c0*x**2) * phi(x) =0;                               (1)
c
c        with the coefficients a0,b0,c0,a1,b1,c1,a2,b2,c2 specified
c        by the user. The user also specifies the point in R where 
c        the derivatives are to be evaluated, and the values of 
c        phi and phi' at the point x.
c
c                   Input parameters:
c
c  a0,b0,c0,a1,b1,c1,a2,b2,c2 - the coefficients in (1) above
c  x - the point where the derivatives are to be calculated
c  phi - the value of the function at the point x
c  der - the value of the first derivative at the point x
c  nders - the number of derivatives to be returned
c
c        . . . evaluate the third and fourth derivatives
c
c
        if (ifcalled .eq. 1) goto 4100
c
        facs(1)=1
        do 4200 i=2,100
           facs(i)=facs(i-1)*i
 4200   continue
c
        do 4300 i=1,100
           facs(i)=1d0/facs(i)
 4300   continue
c
cc        call prin2('facs=*',facs,100)
c
        ifcalled=1
c
 4100   continue
c
        xx2=x*x
c
        p=a2+b2*x+c2*xx2
        q=a1+b1*x+c1*xx2
        r=a0+b0*x+c0*xx2
c
        coe10=b0 + 2*c0*x
        coe11=a0 + b1 + x*(b0 + 2*c1 + c0*x)
        coe12=a1 + b2 + x*(b1 + 2*c2 + c1*x)
        coe13=a2 + x*(b2 + c2*x)
c
        i0=0
        ders(i0)=phi
        ders(1)=der
c
        ders(2)=(-(r*phi+q*der)/p)
c
        if(nders .eq. 2) return
c
        der2=ders(2)
c
        der3=-(coe10*phi+coe11*der+coe12*der2)/coe13
        der3=-(coe10*phi+coe11*der+coe12*der2)/coe13
        ders(3)=der3
c
        if(nders .eq. 3) return
c
        coe20=2*c0
        coe21=2*(b0 + c1 + 2*c0*x)
        coe22=a0 + 2*b1 + 2*c2 + b0*x + 4*c1*x + c0*xx2
        coe23=a1 + 2*b2 + x*(b1 + 4*c2 + c1*x)
        coe24=a2 + x*(b2 + c2*x)
c
        der4=-(coe20*phi+coe21*der+coe22*der2+coe23*der3)/coe24
        ders(4)=der4
c
        if(nders .eq. 4) return
c
        ders(1)=ders(1)*scale
        tscale2=scale*scale
        ders(2)=ders(2)*tscale2
        tscale3=tscale2*scale
        ders(3)=ders(3)*tscale3
        tscale4=tscale3*scale
        ders(4)=ders(4)*tscale4
c
c
c        calculate higher derivatives
c
        dp2=a2+b2*x+c2*xx2
        hdp2=1/dp2
c
        cc2=2*c2
c
        do 2600 i=3,nders-2
c
        n=i
        dn=i
        dnm1=dn-1d0
c
        dnn2=dn*dnm1
c
c
        dp1= dn*(b2+2*c2*x) +a1+b1*x+c1*xx2
        d=dnn2*c2+dn*(b1+2*c1*x)+a0+b0*x+c0*xx2
        dm1=dnn2*c1+dn*(b0+2*c0*x)
        dm2=dnn2*c0
c
        dd=dp1*ders(n+1)*scale+d*ders(n)*tscale2
     1     +dm1*ders(n-1)*tscale3+dm2*ders(n-2)*tscale4
c
c
c 
        ders(n+2)=-dd*hdp2
c
c
 2600 continue
c
c

        do 2800 i=1,nders
        ders(i)=ders(i)*facs(i)
 2800   continue
c
        return
        end
c
c
c
c
c
       subroutine lag_evalu(x,x0,u0,ders,scale,nders,uval,uprime)
       implicit real *8 (a-h,o-z)
       dimension ders(1)
c
c      This subroutine evaluates a taylor expansion around (x0,u0) at x,  
c      given the derivates (ders), number of derivative temrs (nders)
c
c      returns: uval - value at x,  uprime - first derivative at x 
c
       h=(x-x0)
       temp1=1/scale
       h=h*temp1
       uval=u0
       uprime=0
c 
       hi=h
       him1=1
c     
       do 1000 i=1,nders
c
       uprime=uprime+ders(i)*him1*i
       uval=uval+ders(i)*hi
c   
       him1=hi
       hi=hi*h
c
 1000 continue        
c
       uprime=uprime*temp1
c
       return
       end  
c
cccccccccccccccccccccccccccccccccccccccccc
c
       subroutine lag_newton(approx,zero,zeroval,ders,
     1     scale,nders,uprime)
       implicit real *8 (a-h,o-z)
       dimension ders(1)
       z0=zeroval
       uprime=0
c
 1000 call lag_evalu(approx,zero,z0,ders,scale,nders,uval,uprime)
       h=-uval/uprime
       approx=approx+h
       if (abs(uval/uprime/approx)>1d-11) goto 1000
c
       do 2000 i=1,2      
c
       call lag_evalu(approx,zero,z0,ders,scale,nders,uval,uprime)
       h=-uval/uprime
       approx=approx+h
c
 2000 continue
c
       call lag_evalu(approx,zero,z0,ders,scale,nders,uval,uprime)
c       
       return
       end
