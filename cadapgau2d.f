c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       end of the debugging codes, beginning of the 2d
c       adaptive routines
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine cadapgau2d(ier,xa,xb,ya,yb,k,eps,
     1      cfunc,par1,par2,par3,par4,par5,par6,maxrec,
     2      numint,cint)
        implicit real *8 (a-h,o-z)
        real *8 xs(1000),whts(1000),stack(6,100000),cvals(100000)
        complex *16 par1(1),par2(1),par3(1),par4(1),par5(1),
     1      par6(1),cint,ima,cd,cdiff,cval1,cval2,cval3,cval4
        external cfunc
c
c
c       this routine calculates the integral of cfunc over
c       the rectangle defined by [xa,xb] \times [ya,yb] to
c       ABSOLUTE precision eps using tensor product gaussian
c       quadratures with k one-dimensional nodes
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        ifwhts=1
        call legewhts(k,xs,whts,ifwhts)
c
        nnmax=100000
        maxdepth=60
c 
        stack(1,1)=xa
        stack(2,1)=xb
        stack(3,1)=ya
        stack(4,1)=yb
        stack(5,1)=-1
        stack(6,1)=0
c
        call coneint2d(xa,xb,ya,yb,cfunc,par1,par2,par3,par4,
     1      par5,par6,xs,whts,k,cd)
c
        numint=1
        cvals(numint)=cd
        cint=0
        maxrec=0
        ier=0
c
        do 4000 ijk=1,maxdepth
c
        nnn=numint
        ifdone=1
        do 3000 i=1,nnn
c
c       check the i^th entry in the stack and process if necessary
c
        if (stack(5,i) .gt. 0) goto 3000
c
        stack(5,i)=1
c
        x1=stack(1,i)
        x2=stack(2,i)
        y1=stack(3,i)
        y2=stack(4,i)
c
        xhalf=(x1+x2)/2
        yhalf=(y1+y2)/2
c
        call coneint2d(x1,xhalf,y1,yhalf,cfunc,par1,par2,par3,par4,
     1      par5,par6,xs,whts,k,cval1)
c
        call coneint2d(x1,xhalf,yhalf,y2,cfunc,par1,par2,par3,par4,
     1      par5,par6,xs,whts,k,cval2)
c
        call coneint2d(xhalf,x2,y1,yhalf,cfunc,par1,par2,par3,par4,
     1      par5,par6,xs,whts,k,cval3)
c
        call coneint2d(xhalf,x2,yhalf,y2,cfunc,par1,par2,par3,par4,
     1      par5,par6,xs,whts,k,cval4)

c
c       check accuracy
c
        cdiff=(cvals(i)-cval1-cval2-cval3-cval4)
        if (abs(cdiff) .lt. eps) then
          cint=cint+cval1+cval2+cval3+cval4
          cvals(i)=cval1+cval2+cval3+cval4
        endif
c
        if (abs(cdiff) .ge. eps) then
c
          ifdone=0
c
          numint=numint+1
          cvals(numint)=cval1
c
          stack(1,numint)=x1
          stack(2,numint)=xhalf
          stack(3,numint)=y1
          stack(4,numint)=yhalf
          stack(5,numint)=-1
          stack(6,numint)=-1
c
          numint=numint+1
          cvals(numint)=cval2
c
          stack(1,numint)=x1
          stack(2,numint)=xhalf
          stack(3,numint)=yhalf
          stack(4,numint)=y2
          stack(5,numint)=-1
          stack(6,numint)=-1
c
          numint=numint+1
          cvals(numint)=cval3
c
          stack(1,numint)=xhalf
          stack(2,numint)=x2
          stack(3,numint)=y1
          stack(4,numint)=yhalf
          stack(5,numint)=-1
          stack(6,numint)=-1
c
          numint=numint+1
          cvals(numint)=cval4
c
          stack(1,numint)=xhalf
          stack(2,numint)=x2
          stack(3,numint)=yhalf
          stack(4,numint)=y2
          stack(5,numint)=-1
          stack(6,numint)=-1
c
        endif
c
 3000 continue
c
        if (ifdone .eq. 1) goto 4200
c
 4000 continue
c
        if (ijk .ge. maxdepth) then
          call prinf('maxdepth reached, maxdepth=*',maxdepth,1)
          maxrec=maxdepth
          ier=8
          return
        endif
c
        if (numint .ge. nnmax) then
          call prinf('nnmax reached, numint=*',numint,1)
          ier=16
          return
        endif
c
 4200 continue
c
        maxrec=ijk-1
c
        return
        end
c
c
c
c
c
        subroutine coneint2d(xa,xb,ya,yb,cfunc,par1,par2,par3,par4,
     1      par5,par6,xs,whts,k,cint)
        implicit real *8 (a-h,o-z)
        real *8 xs(1),whts(1)
        complex *16 ima,cint,cd,par1(1),par2(1),par3(1),par4(1),
     1      par5(1),par6(1)
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        ux=(xb-xa)/2
        vx=(xb+xa)/2
c
        uy=(yb-ya)/2
        vy=(yb+ya)/2
c
        cint=0
c
        do 2000 i=1,k
        x=ux*xs(i)+vx
c
        do 1800 j=1,k
        y=uy*xs(j)+vy
c
        call cfunc(x,y,par1,par2,par3,par4,par5,par6,cd)
        cint=cint+cd*whts(i)*whts(j)
c
 1800 continue
 2000 continue
c
        cint=cint*ux*uy
c
        return
        end
