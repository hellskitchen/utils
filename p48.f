c 
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc  
c 
c
c        This is the end of the debugging code, and the beginning of
c        the matrix manipulation code proper
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc  
c
c 
c        This file contains 19 user-callable subroutines, but the
c        definition of the user is peculiar. Following is a brief
c        description of the so-called user-callable subroutines in 
c        this file.
c
c                    Top level routines:
c
c   p48_skeletonize - skeletonizes the user-supplied matrix a
c        and returns its skeleton and all the relevant 
c        information in the array w. The data can be retrieved 
c        from the said array w via the subroutines p48_unpack,
c        p48_unpk, p48_vec (see)
c
c   p48_structures_multiply - multipies the matrices stored in the
c        structures w1, w2, producing the output structure w3. 
c
c   p48_invert - inverts the matrix
c
c                  I+A,                                           (1)
c
c        with the matrix A supplied by the user in array w, and
c        returns the matrix B in array w2, such that
c
c                  (I+B)(I+A) = I                                 (2)
c
c   p48_recompress - recompresses the user-supplied structure w1,
c        returning the recompressed structure in the array w3, which 
c        is also used as a work array 
c
c   p48_vec - applies to the user-supplied vector x the compressed 
c        version of the matrix a constructed via a preceding call to 
c        one of the subroutines p48_skeletonize, p48_structures_multiply,
c        etc. 
c
c   p48_structures_add - adds the matrices stored in the
c        structures w1, w2, producing the output structure w3. 
c
c   p48_structures_add_right - constructs the skeleton of the
c        matrix
c
c         C = (I+B) \circ A                                    (1)
c
c        with A, B two matrices whose skeletons have been
c        supplied by the user in the arrays w1, w2 respec-
c        tively. The product matrix is returned in the 
c        array w3, also in the skeleton form. Needless to
c        say, the dimensionalities of the matrices must 
c        agree in the obvious way.
c
c   p48_structures_add_left - constructs the skeleton of the
c        matrix
c
c         C = A \circ (I+B),                                   (1)
c
c        with A, B two matrices whose skeletons have been
c        supplied by the user in the arrays w1, w2 respec-
c        tively. The product matrix is returned in the 
c        array w3, also in the skeleton form. Needless to
c        say, the dimensionalities of the matrices must 
c        agree in the obvious way.
c
c   p48_transpose - for the user-specified operator a stored in 
c        a skeletonized form in the array w1, this subroutine 
c        constructs and returns in array w2 the skeletonization 
c        of the adjoint operator A^*.
c
c   p48_mult_coef - multiplies the user-specified operator by 
c        the user-specified coefficient
c
c   p48_sign_change - changes the sign of the user-specified
c        operator
c
c   p48_skeleton_expand - expands the skeletonization of the 
c        matrix from the compressed format into three regular
c        matrices. It also returns the arrays icols, noticols,
c        irows, notirows; needless to say, it uses subroutines
c        p48_expand_evalmatr, p48_expand_expandmatr.
c
c   p48_combine_locally - constructs a somewhat specialized 
c        "expand" matrix expand for the matrix A.
c     
c        Explanation: A has the dimensionality A(n,m1+m2), and
c        consists of two matrices A1(n,m1), A2(n,m2) put together
c        side by side. The matrix expand produced by this
c        subroutine has dimensionality (ncols3,m1+m2), with
c
c                   ncols1=nc1+nc2;
c
c        expand is indexed by the array icols, and the first nc1
c        elements of icols point to the first m1 columns in A,
c        while the remaining nc2 elements in icols point to the
c        last m2 columns in A.
c
C     NOTE: THIS IS A VERY INEFFICIENT SUBROUTINE!
C
c                    Lower level routines:
c
c   p48_squeeze_expandmatr - converts the input "expand" matrix in 
c        the regular form (i.e. containing the identity submatrix 
c        in the appropriate positions) into the compressed form 
c        of the said "expand" matrix 
c
c   p48_squeeze_evalmatr - converts the input "eval" matrix in 
c        the regular form (i.e. containing the identity submatrix 
c        in the appropriate positions) into the compressed form 
c        of the said "eval" matrix 
c
c   p48_expand_expandmatr - converts the input "expand" matrix in 
c        the compressed form into the regular  (i.e. containing 
c        the identity submatrix in the appropriate positions)
c        form of the said "expand" matrix 
c
c   p48_expand_evalmatr - converts the input "eval" matrix in 
c        the compressed form into the regular  (i.e. containing 
c        the identity submatrix in the appropriate positions)
c        form of the said "eval" matrix 
c
c   p48_expand1_eval2 - evaluates (somewhat efficiently) the 
c        product of an "expand" matrix with an "eval" matrix, 
c        under the obvious assumption that the appropriate 
c        dimensionalities are the same.
c
c   p48_eval1_eval2 - multiplies the matrix eval1 from the 
c        structure stored in the array w1 by the matrix eval2
c        from the structure stored in the array w2. PLEASE
c        NOTE THAT THE OUTPUT OF THIS SUBROUTINE IS THE MATRIX
C        EVAL33 IN THE UNCOMPRESSED FORM!
c
c   p48_expand2_expand1 - multiplies the matrix expand2 from the 
c        structure stored in the array w2 by the matrix expand1
c        from the structure stored in the array w1. PLEASE
c        NOTE THAT THE OUTPUT OF THIS SUBROUTINE IS THE MATRIX
C        EXPAND33 IN THE UNCOMPRESSED FORM!
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c 
c 
c 
c 
        subroutine p48_combine_locally(ier,n,a1,m1,a2,m2,eps,
     1      expand,icols,ncols3,nc1,nc2,
     2      w,lenw,lused)
        implicit real *8 (a-h,o-z)
        save
        dimension a1(n,m1),a2(n,m2),icols(1),
     1       expand(1),w(1)
c
c        This subroutine constructs a somewhat specialized "expand"
c        matrix expand for the matrix A.
c     
c        Explanation: A has the dimensionality A(n,m1+m2), and
c        consists of two matrices A1(n,m1), A2(n,m2) put together
c        side by side. The matrix expand produced by this
c        subroutine has dimensionality (ncols3,m1+m2), with
c
c                   ncols3=nc1+nc2;
c
c        expand is indexed by the array icols, and the first nc1
c        elements of icols point to the first m1 columns in A,
c        while the remaining nc2 elements in icols point to the
c        last m2 columns in A.
c
C     NOTE: THIS IS A VERY INEFFICIENT SUBROUTINE!
C
c
c                       Input parameters:
c
c  a1, a2 - the matrices whose combination is to be skeletonized; 
c        not damaged by this subroutine in any way
c  n,m1,m2 - the dimensionalities of the matrices a1, a2
c  eps - the precision to which a is to be skeletonized
c  lenw - the length of the work array w provided by the user
c  
c                       Output parameters:
c
c  ier - error return code:
c    ier=0 means successful execution
c    ier > 0 means that the amount lenw of space provided by the user
c        in the work array w is insufficient
c  keep - the length (in real *8 elements) of the part of the array 
c  lused - length (in real *8 elements) of the part of the array 
c        w that actually has been used by the subroutine
c  
c                       Work arrays:
c
c  w - must be sufficiently large
c
        k=n
        if(m1 .gt. k) k=m1
        if(m2 .gt. k) k=m2
c
        l=k+2
        iicols1=1
        iicols2=iicols1+k
        iicols3=iicols2+k
c
        iw=iicols3+k
        lenw2=lenw-iw-2
c
        call p48_combine_local(ier,n,a1,m1,a2,m2,eps,
     1      expand,icols,ncols3,nc1,nc2,
     2      w(iicols1),w(iicols2),w(iicols3),w(iw),lenw2,lused)
c
        lused=lused+iw+1
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_combine_local(ier,n,a1,m1,a2,m2,eps,
     1      expand_final,icols4,ncols3,nc1,nc2,
     2      icols1,icols2,icols3,w,lenw,lused)
        implicit real *8 (a-h,o-z)
        save
        dimension a1(n,m1),a2(n,m2),
     1       icols1(1),icols2(1),icols4(1),icols3(1),
     2       expand_final(1),w(1)
c
         ier=0
c
c        skeletonize each of matrices a1, a2, and unpack
c        the obtained "expand" matrices
c
c        . . . matrix a1
c
        call p48_skeletonize(jer,a1,n,m1,eps,
     1      w,lenw,keep,lused1)
c        
        lused=lused1
        if(jer .ne. 0) then
            ier=4096
            return
        endif
c
        call p48_unpk(n17,m17,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w)
c
        itype=3
        ifijs=0
c
        iexpand1_out=keep+2
c
        call p48_expand_expandmatr(w(iexpand1),w(iexpand1_out),
     1      w(iicols1),w(inoticols1),ncols1,m1,itype,ijs,
     2      nijs7,ifijs)
c
        call p48_int_copy(w(iicols1),icols1,ncols1)
        call p48_copy(w(iexpand1_out),w,ncols1*m1)
c
        iexpand1_out=1
        iw=ncols1*m1+11
c
c        . . . matrix a2
c
        lenw2=lenw-iw-2
        call p48_skeletonize(jer,a2,n,m2,eps,
     1      w(iw),lenw2,keep2,lused2)
c       
        ll=iw+lused2
        if(ll .gt. lused) lused=ll
c    
        if(jer .ne. 0) then
            ier=2048
            return
        endif
c        
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w(iw))
c
        itype=3
        ifijs=0
c
        iexpand2_out=iw+keep2+12
c
        lexpand2_out=ncols2*m2+12
        ll=iexpand2_out+lexpand2_out
c
        if(ll .gt. lenw) then
            ier=1526
            return
        endif
c
        if(ll .gt. lused) lused=ll
c
        call p48_expand_expandmatr(w(iw+iexpand2-1),w(iexpand2_out),
     1      w(iw+iicols2-1),w(iw+inoticols2-1),ncols2,m2,itype,ijs,
     2      nijs7,ifijs)
c
        call p48_int_copy(w(iw+iicols2-1),icols2,ncols2)
        iexpand2_out_2=iw
c
        call p48_copy(w(iexpand2_out),w(iexpand2_out_2),ncols2*m2)        
        iexpand2_out=iexpand2_out_2
c
        ia3=iexpand2_out+m2*ncols2+12
c
c        construct the composite matrix to be recompressed
c
        call p48_a3_bld(n,a1,m1,a2,m2,icols1,icols2,
     1      ncols1,ncols2,w(ia3),iii)
c
c        reskeletonize the composite matrix
c
        iw3=ia3+n*iii+12
c
        lenw3=lenw-iw3-2
        call p48_skeletonize(jer,w(ia3),n,iii,eps,
     1      w(iw3),lenw3,keep3,lused3)
c        
        if(jer .ne. 0) then
            ier=1024
            return
        endif
c
        if(lused3+iw3 .gt. lused) lused=lused3+iw3
c        
        iw3_2=ia3
        call p48_copy(w(iw3),w(iw3_2),keep3+12)
        iw3=iw3_2
c
        call p48_unpk(n3,m3,eps3,
     1      ncols3,nrows3,iicols3,inoticols3,iirows3,inotirows3,
     2      iexpand3,ieval3,iaskel3,w(iw3) )
c
        itype=3
        ifijs=0
c
        iexpand3_out=iw3+keep3+12
c
        call p48_expand_expandmatr(w(iw3+iexpand3-1),w(iexpand3_out),
     1      w(iw3+iicols3-1),w(iw3+inoticols3-1),ncols3,m3,itype,ijs,
     2      nijs7,ifijs)
c
        call p48_int_copy(w(iw3+iicols3-1),icols3,ncols3)
c
        iexpand3_out_2=iw3
        call p48_copy(w(iexpand3_out),w(iexpand3_out_2),m3*ncols3+12)
c
        iexpand3_out=iexpand3_out_2
        iexpand_big=iexpand3_out+m3*ncols3+12
        lexpand_big=(ncols1+ncols2)*(m1+m2)+12
        lleft=lenw-iexpand_big
c        
        if(lleft .lt. lexpand_big) then
            ier=512
            return
        endif
c        
c        produce the combined expand matrix
c
        call p48_combine_them(n,m1,m2,
     1      w(iexpand1_out),ncols1,
     2      w(iexpand2_out),ncols2,
     3      w(iexpand3_out),ncols3,
     4      w(iexpand_big),expand_final,
     5      icols1,icols2,icols3,icols4,nc1,nc2)
c
        return
        end
c 
c 
c 
c 
c 
  
        subroutine p48_a3_bld(n,a1,m1,a2,m2,icols1,icols2,
     1      ncols1,ncols2,a3,iii)
        implicit real *8 (a-h,o-z)
        save
        dimension a1(n,m1),a2(n,m2),a3(n,1),icols1(1),icols2(1)
c
c        construct the composite matrix to be recompressed
c
        iii=0
        do 2400 i=1,ncols1
        iii=iii+1
        ii=icols1(i)
        do 2200 j=1,n
c
        a3(j,iii)=a1(j,ii)
 2200 continue
c
 2400 continue
c
        do 2800 i=1,ncols2
        iii=iii+1
        ii=icols2(i)
        do 2600 j=1,n
c
        a3(j,iii)=a2(j,ii)
 2600 continue
c
 2800 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_combine_them(n,m1,m2,
     1      expand1_out,ncols1,
     2      expand2_out,ncols2,
     3      expand3_out,ncols3,
     4      expand_big,expand_final,
     5      icols1,icols2,icols3,icols4,nc1,nc2)
        implicit real *8 (a-h,o-z)
        save
        dimension expand1_out(ncols1,1),expand2_out(ncols2,1),
     1      expand3_out(ncols3,1),icols1(1),
     2      expand_big(ncols1+ncols2,1),expand_final(ncols3,1),
     3      icols2(1),icols3(1),icols4(1)
c
c       stack the matrices expand1, expand2 together, and multiply 
c       them things by the matrix expand3, obtaining the "composite"
c       matrix expand_final
c
        call p48_setzero(expand_big,(m1+m2)*(ncols1+ncols2))
c
        do 2400 i=1,m1
        do 2200 j=1,ncols1
c
        expand_big(j,i)=expand1_out(j,i)
 2200 continue
 2400 continue
c
        do 2800 i=1,m2
        do 2600 j=1,ncols2
c
        expand_big(j+ncols1,i+m1)=expand2_out(j,i)
 2600 continue
 2800 continue
c
        call p48_matmult(expand3_out,expand_big,expand_final,
     1      ncols3,ncols1+ncols2,m1+m2)
c
c        extract the composite skeleton
c
        do 3200 i=1,ncols3
c
        jj=icols3(i)
        if(jj .le. ncols1) icols4(i)=icols1(jj)
        if(jj .gt. ncols1) icols4(i)=icols2(jj-ncols1)+m1
 3200 continue
c
c        . . . reorder
c
        call p48_reord2(expand_final,ncols3,m1,m2,icols4,
     1      expand_big,nc1,nc2)
c
        return
        end
c
c
c
c
c
        subroutine p48_reord2(expand_final,ncols3,m1,m2,icols4,w2,
     1      nc1,nc2)
        implicit real *8 (a-h,o-z)
        save
        dimension expand_final(ncols3,m1+m2),icols4(1),
     1      w2(ncols3,1),iw1(10000),iw2(10000)
c
c        reorder the "expand" array so that the columns
c        corresponding to the first subinterval are first
c
        ii1=0
        ii2=0
c
c       . . . .split
c
        do 2000 i=1,ncols3
c
        j=icols4(i)
        if(j .gt. m1) goto 1400
c
        ii1=ii1+1
        do 1200 jj=1,m1+m2
c
        expand_final(ii1,jj)=expand_final(i,jj)
 1200 continue
c
        iw1(ii1)=j
        goto 2000
c
 1400 continue
c
        ii2=ii2+1
c         
        do 1600 jj=1,m1+m2
c
        w2(ii2,jj)=expand_final(i,jj)
 1600 continue
c
        iw2(ii2)=j
c
 2000 continue
c
c        . . . merge
c
        do 2400 i=1,ii1
c
        icols4(i)=iw1(i)
 2400 continue
c
c        . . . merge
c
        do 2800 i=1,ii2
c
        do 2600 j=1,m1+m2
c
        expand_final(i+ii1,j)=w2(i,j)
 2600 continue
c
        icols4(i+ii1)=iw2(i)
 2800 continue
c
        nc1=ii1
        nc2=ii2
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_transpose(w1,w2,keep2,w3)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),w3(1)
c
c        For the user-specified operator a stored in a 
c        skeletonized form in the array w1, this subroutine 
c        constructs and returns in array w2 the skeletonization 
c        of the adjoint operator A^*.
c
c                 Input parameters:
c
c  w1 - the array containing the skeletonization of the matrix
c        to be transposed
c                  
c                 Output parameters:
c
c  w2 - array containing the skeletonization of the matrix whose
c        skeletonization was supplied in array w1. Must be at
c        least as long as the array w1
c  keep - the number of elements in array w2 that should not be 
c        altered between the call to this subroutine and 
c        subsequent calls to subroutines using the adjoint matrix
c        p48_vec, etc.
c  
c                 Work arrays:
c
c  w3 - must be at least as long as the array w1
c
c
c        . . . unpack the structure in arrays w1
c
        call p48_unpk(n1,m1,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
c       allocate memory and reformat the structure
c
        m2=n1
        n2=m1
        ncols2=nrows1
        nrows2=ncols1
c
        iwork=1
        lwork=ncols1*nrows1+10
c
        iwork2=iwork+lwork
        lwork2=(m2-ncols2)*ncols2+10
c
        iwork3=iwork2+lwork2
        lwork3=(n2-nrows2)*nrows2+10
c
        iw2=iwork3+lwork3
c
        eps2=eps1
c
        iicols2=iirows1
        iirows2=iicols1
c
        inoticols2=inotirows1
        inotirows2=inoticols1
c
        iexpand2=ieval1
        ieval2=iexpand1
c
        call p48_transp(w1(iaskel1),nrows1,ncols1,w3(iwork))
        call p48_transp(w1(iexpand2),m2-ncols2,ncols2,w3(iwork2))
        call p48_transp(w1(ieval2),nrows2,n2-nrows2,w3(iwork3))
c
        lenw2=1000 000
c
c       pack the transpose of the structure in array w2
c
        call p48_pack(ier,n2,m2,eps2,
     1      ncols2,w1(iicols2),w1(inoticols2),nrows2,w1(iirows2),
     2      w1(inotirows2),w3(iwork2),w3(iwork3),w3(iwork),
     3      w2,lenw2,keep2)
c
        return
        end
c
c
c
c
c
        subroutine p48_invert(ier,w,w2,lenw2,keep,lused,rcond)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1),w2(1)
c
c        This subroutine inverts the matrix
c
c                  I+A,                                           (1)
c
c        with the matrix A supplied by the user in array w, and
c        returns the matrix B in array w2, such that
c
c                  (I+B)(I+A) = I                                 (2)
c
c                
c                  Input parameters:
c
c  w - the array containing the structure defining the matrix A
c  lenw2 - the amount of space provided by the user in array w2,
c        in real *8 elements
c
c                  Output parameters:
c
c  ier - error return code:
c     ier=0 means successful execution
c     ier=64 means that the amount of space provided by the user 
c        in array w2 is insufficient (see lenw2 above)
c  keep - the part of the array w2 (in real *8 elements) that
c        should not be changes between the call to this subroutine
c        and the subsequent calls to the subroutines p48_unpack, 
c        p48_unpk, p48_vec (see)
c  lused - the part of array w2 (in real *8 elements) that was 
c        actually used by the subroutine
c  rcond - the condition number of the matrix the subbroutine had
c        to invert (or rather, a fairly crude extimate of the said
c        condition number)
c
c        . . . unpack array w
c
        call p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
c
c       . . . allocate memory
c
        ninire=2
        ninire = sizeof(x)/sizeof(i)
        
        ll=ncols
        if(nrows .gt. ll) ll=nrows
c
        iexpand_out=1
        lexpand_out=ncols*m+10
c
        ieval_out=iexpand_out+lexpand_out
        leval_out=nrows*n+10
c
        iww1=ieval_out+leval_out
        lww1=ll**2+10
c
        iww2=iww1+lww1
        lww2=ll**2+10
c
        iijs_expand=iww2+lww2
        lijs_expand=2*(ncols+nrows)/ninire+3
c
        iijs_eval=iijs_expand+lijs_expand
        lijs_eval=2*(ncols+nrows)/ninire+3
c
        iirc=iijs_eval+lijs_eval
        lirc=n/ninire+3
c
        iw2=iirc+lirc 
c
        lenw22=lenw2-iw2-2
c
        call p48_invert0(jer,w,w2(iexpand_out),w2(ieval_out),
     1      w2(iww1),w2(iww2),ncols,w2(iw2),lenw22,
     2      w2(iijs_eval),w2(iijs_expand),w2(iirc),keep,rcond)
c
        lused=iw2+keep
        if(jer .ne. 0) ier=64
c
c        perform garbage collection
c
        call p48_copy(w2(iw2),w2,keep)
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_invert0(ier,w,expand_out,eval_out,
     1      ww1,ww2,ncols,w2,lenw2,ijs_eval,ijs_expand,irc,
     2      keep,rcond)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1),eval_out(1),expand_out(1),
     1      ww2(ncols,ncols),ww1(1),w2(1),ijs_eval(2,1),
     2      ijs_expand(2,1),irc(1)
c
c        . . . unpack array w
c
        call p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
c
c       evaluate the product expand \circ eval
c
        call p48_expand1_eval2(w(inoticols),ncols,
     1      w(inotirows),m,nrows,
     2      ww1,expand_out,eval_out,w,w,
     3      ijs_eval,ijs_expand,irc)
c
c       evaluate the product 
c       ( expand(k,n) \circ eval(n,k) ) \cirk askel(k,k)
c
        call p48_matmult(ww1,w(iaskel),ww2,ncols,nrows,ncols)
c
c       add I to ww, and invert the result
c
        do 2200 i=1,ncols
c
        ww2(i,i)=ww2(i,i)+1
 2200 continue
c
        call orthom(ww2,ncols,expand_out,rcond)
        call p48_matmult(w(iaskel),ww2,ww1,nrows,ncols,ncols)
        call p48_signchange(ww1,ww2,nrows*ncols)
c
c        pack them things in array w2
c
        call p48_pack(jer,n,n,eps,
     1      ncols,w(iicols),w(inoticols),nrows,w(iirows),
     2      w(inotirows),w(iexpand),w(ieval),ww2,
     3      w2,lenw2,keep)
c
        if(jer .ne. 0) ier=64
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_structures_add_right(ier,wa,wb,
     1      wc,lenwc,keepc,lusedc)
        implicit real *8 (a-h,o-z)
        save
        dimension wa(1),wb(1),wc(1)    
c
c        This subroutine evaluates the product 
c
c          C= (I+B)*A                                              (1)
c
c              Input parameters:
c
c  wa - array containing the skeleton of the matrix A in (1)
c  wb - array containing the skeleton of the matrix B in (1)
c  lenwc - the length of array wc (in real *8 words)
c
c              Output parameters:
c
c  ier - error return code:
c     ier=0 means successful execution
c     ier=32 or 64 means that the user supplied array ws is too short
c  wc - array containing the matrix C in (1); please note that this
c        is also a work array (see below)
c  keepc - the number of elements in array wc that must be unchanged
c        between the call to this subroutine and subsequent use of
c        the array wc
c  lusedc - the total length (in real *8 elements) of the part of 
c        the array wc actually used by this subroutine
c
c              Work arrays:
c
c  wc - plese note that this is also an output parameter (see above);
c        must be sufficiently large.
c          
c
c        . . . transpose the matrices A, B
c
        ier=0
        keepa=wa(6)
        keepb=wb(6)
c
        keepab_max=keepa
        if(keepb .gt. keepab_max) keepab_max=keepb
c
        iwa_tran=1
        iwb_tran=iwa_tran+keepa+keepb+10
        ic=iwb_tran+keepb+keepa+10
c
        lenwc_left=lenwc-ic-1
c
        if(lenwc_left .lt. keepab_max) then
            ier=64
            return
        endif
c
        call p48_transpose(wa,wc(iwa_tran),keeptrana,wc(ic))
        call p48_transpose(wb,wc(iwb_tran),keeptranb,wc(ic))
c
c       construct the transposed product
c
        call p48_structures_add_left(jer,wc(iwa_tran),wc(iwb_tran),
     1      wc(ic),lenwc_left,keepc_tran,lusedc_tran)
c
        if(jer .ne. 0) then
            ier=32
            return
        endif
c
        call p48_transpose(wc(ic),wc(iwa_tran),keeptrana,wc(iwb_tran) )
c
        call p48_copy(wc(iwa_tran),wc,keepc_tran)
c
        keepc=keepc_tran
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_structures_add_left(ier,w1,w2,
     1      w3,lenw3,keep3,lused3)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),w3(1)
c
c        This subroutine constructs the skeleton of the
c        matrix
c
c         C = A \circ (I+B),                                   (1)
c
c        with A, B two matrices whose skeletons have been
c        supplied by the user in the arrays w1, w2 respec-
c        tively. The product matrix is returned in the 
c        array w3, also in the skeleton form. Needless to
c        say, the dimensionalities of the matrices must 
c        agree in the obvious way.
c
c                Input parameters:
c
c  w1 - the array containing the skeleton of the matrix A in (1)
c  w2 - the array containing the skeleton of the matrix B in (1)
c  lenw3 - the length (in real *8 words) of the array w3 supplied
c        by the user
c  
c                Output parameters:
c
c  ier - error return code:
c     ier=0 means successful execution
c     ier=4096 means that the dimensionalities of matrices in arrays
c        w1, w2 are incompatible. This is a fatal error.
c     ier=(any other positive number) means that the amount of space
c        provided by the user in array w3 (as specified by the 
c        parameter lenw3 above) is insufficient. This is a fatal error.
c  w3 - array containing the skeleton of the matrix C in (1) above;
c        please note that this is also a work array
c  keep3 - the part of array w3 (in real *8 words) actually occupied
c        by the skeleton of matrix C. This part should not be changed
c        between the call to this subroutine and subsequent calls to 
c        the subroutines (p48_vec, etc.) that use the skeleton.
c  lused3 - the part (in real *8 words) of the array w3 that has been
c        actually used by this subroutine. 
c    
c                Work arrays:
c
c  w3 - must be sufficiently large; please note that this is also
c        an output parameter.
c
c        . . . unpack the structures in arrays w1, w2
c
        ier=0
c
        call p48_unpk(n1,m1,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)


        call prinf('ncols1=*',ncols1,1)
        call prinf('ncols2=*',ncols2,1)
c
c        Check that the dimensionalities of the matrices agree
c
        ifbomb=0
        if(m1 .ne. n2) ifbomb=1
        if(m1 .ne. m2) ifbomb=1
        if(ifbomb .eq. 1) then
            ier=4096
            return
        endif
c
c       allocate memory
c
        nm_max=n1
        if(m1 .gt. n1) nm_max=m1
        if(m2 .gt. nm_max) nm_max=m2
        if(n2 .gt. nm_max) nm_max=n2
c
        ncolbig=ncols1
        if(ncols2 .gt. ncolbig) ncolbig=ncols2
        if(nrows1 .gt. ncolbig) ncolbig=nrows1
        if(nrows2 .gt. ncolbig) ncolbig=nrows2
c
        iirc=1
        lirc=ncolbig+10+nm_max
c
        iijs_eval=iirc+lirc
        lijs_eval=4*ncolbig+10
c
        iijs_expand=iijs_eval+lijs_eval
        lijs_expand=4*ncolbig+10
c
        iirows_out=iijs_expand+lijs_expand
        lirows_out=ncolbig+10
c
        inotirows_out=iirows_out+lirows_out
        lnotirows_out=nm_max+10
c
        iw3=inotirows_out+lnotirows_out
        lenw33=lenw3-iw3-1
c
        call p48_structures_add_left0(ier,w1,w2,
     1      w3(iw3),lenw33,keep3,lused3,
     2      w3(iirc),w3(iijs_eval),w3(iijs_expand),
     3      w3(iirows_out),w3(inotirows_out))

        if(ier .ne. 0) then
            call prinf('after ...add_left0, ier=*',ier,1)
            stop
        endif
c
        call p48_copy(w3(iw3),w3,keep3)
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_structures_add_left0(ier,w1,w2,
     1      w3,lenw3,keep3,lused3,
     2      irc,ijs_eval,ijs_expand,irows_out,notirows_out)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),w3(1),irc(1),ijs_eval(1),
     1      ijs_expand(1),irows_out(1),notirows_out(1)
c
c        . . . unpack the structures in arrays w1, w2
c
        ier=0
c
        call p48_unpk(n1,m1,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
c        multiply expand1 by eval2
c
        nm_max=n1
        if(m1 .gt. n1) nm_max=m1
        if(m2 .gt. nm_max) nm_max=m2
        if(n2 .gt. nm_max) nm_max=n2
c
        ncolbig=ncols1
        if(ncols2 .gt. ncolbig) ncolbig=ncols2
        if(nrows1 .gt. ncolbig) ncolbig=nrows1
        if(nrows2 .gt. ncolbig) ncolbig=nrows2
c
        iww1=1
        lww1=ncolbig**2 +10
c
        iexpand_out=iww1+lww1
        lexpand_out=ncolbig*nm_max +10
c
        ieval_out=iexpand_out+lexpand_out
        leval_out=ncolbig*nm_max+10
c
        lused3=ieval_out+leval_out
        if(lused3 .gt. lenw3) then
            ier=1024
            return
        endif
c
        eps=eps1
        if(eps2 .gt. eps1) eps=eps2
c
        call p48_expand1_eval2(w1(inoticols1),ncols1,
     1      w2(inotirows2),m1,nrows2,
     2      w3(iww1),w3(iexpand_out),w3(ieval_out),w1,w2,
     3      ijs_eval,ijs_expand,irc)
c
        iw3=iww1+lww1
        lw3=ncolbig**2+10
c
c        multiply (expand1 * eval2) by askel2
c
        call p48_matmult(w3,w2(iaskel2),w3(iw3),
     1      ncols1,nrows2,ncols2)
c
        call p48_copy(w3(iw3),w3,lw3)
        iw3=1
c
        iexpand_out=iw3+lw3
        lexpand_out=ncolbig*nm_max+10
c
        iw7=iexpand_out+lexpand_out
        lw7=ncolbig*nm_max+10
c
        ltot=iw7+lw7
        if(ltot .gt. lused3) lused3=ltot
        if(lused3 .gt. lenw3) then
            ier=1024
            return
        endif
c
c       expand the matrix expand2
c
        itype=3
        ifijs=0
        call p48_expand_expandmatr(w2(iexpand2),w3(iexpand_out),
     1      w2(iicols2),w2(inoticols2),ncols2,m2,itype,
     2      ijs_expand,nijs7,ifijs)
c
c        multiply (expand1 * eval2 * askel2) by expand2
c                
        call p48_matmult(w3(iw3),w3(iexpand_out),w3(iw7),
     1      ncols1,ncols2,m2)
c
c        expand the matrix expand1
c
        itype=3
        ifijs=0
        call p48_expand_expandmatr(w1(iexpand1),w3(iexpand_out),
     1      w1(iicols1),w1(inoticols1),ncols1,m1,itype,
     2      ijs_expand,nijs7,ifijs)
c
c        add the obtained matrix (expand1 * eval2 * askel2 * expand2
c        to the matrix expand1
c
        call p48_add(w3(iw7),w3(iexpand_out),w3(iw7),ncols1*m2)
c
c        multiply the result from the left by askel1
c
        call p48_matmult(w1(iaskel1),w3(iw7),w3(iexpand_out),
     1      nrows1,ncols1,m2)
c
c        skeletonize the obtained (nrows1*m2) matrix expand_out
c
        lenw7=lenw3-iw7
c
        call p48_skeletonize(ier,w3(iexpand_out),nrows1,m2,eps,
     1      w3(iw7),lenw7,keep7,lused7)
c
        if(ier .ne. 0) then
            ier=512
            return
        endif
c
        lll=lused7+iw7
        if(lll .gt. lused3) lused3=lll
c
c       unpack the array w7
c
        call p48_unpk(n7,m7,eps7,
     1      ncols7,nrows7,iicols7,inoticols7,iirows7,inotirows7,
     2      iexpand7,ieval7,iaskel7,w3(iw7))
c
        call p48_copy(w3(iw7),w3,keep7)
        lw3=keep7+10
c
        ieval33=iw3+lw3        
        leval33=ncolbig*nm_max+10
c
        iexpand_out=ieval33+leval33
        lexpand_out=ncolbig*nm_max+10
c
        iw377=iexpand_out+lexpand_out
        lw377=ncolbig**2+10
c
        iw333=iw377+lw377
c
        if(iw333 .ge. lenw3) then
            ier=256
            return
        endif
c
c       . . .  multiply
c
        call p48_eval1_eval2(nrows7,nrows1,n1,
     1      w1,w3,w3(inotirows7),w3(iexpand_out),w3(iw377),w3(ieval33),
     2      w1(inotirows1),w1(iirows1),w3(iirows7))
c
c        construct the arrays irows, notirows to be used for 
c        the squizeeing of the matrix eval33
c
        call p48_irows3_bld(w1(iirows1),w3(iirows7),irows_out,nrows7,
     1      n1,notirows_out)
c
c       . . . squeeze
c
        call p48_squeeze_evalmatr(w3(ieval33),w3(iexpand_out),
     1      irows_out,notirows_out,nrows7,n1)
c
c       pack the newly obtained structure
c
        lenw333=lenw3-iw333
        call p48_pack(ier,n1,m2,eps,
     1      ncols7,w3(iicols7),w3(inoticols7),nrows7,irows_out,
     2      notirows_out,w3(iexpand7),w3(iexpand_out),w3(iaskel7),
     3      w3(iw333),lenw333,keep3)
c
        call p48_copy(w3(iw333),w3,keep3)
c
        lll=iw333+keep3
        if(lll .gt. lused3) lused3=lll
c


        call prinf('and nrows7=*',nrows7,1)
        call prinf('and ncols7=*',ncols7,1)


        return
        end
c 
c 
c 
c 
c 
        subroutine p48_irows3_bld(irows,irows7,irows_out,nrows7,
     1      m,notirows_out)
        implicit real *8 (a-h,o-z)
        save
        dimension irows(1),irows7(1),irows_out(1),notirows_out(1)
c
c       construct the array irows_out
c
        do 1600 i=1,nrows7
c
        j=irows7(i)
        irows_out(i)=irows(j)
 1600 continue
c
c       construct the array notirows_out
c
        ii=0
        do 2400 i=1,m
c
        do 2200 j=1,nrows7
c
        if(irows_out(j) .eq. i) goto 2400
c
 2200 continue
c
        ii=ii+1
        notirows_out(ii)=i
 2400 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_structures_add(ier,w1,w2,
     1      w3,lenw3,keep3,lused3)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),w3(1)
c
c        This subroutine adds the matrices stored in the
c        structures w1, w2, producing the output structure w3. 
c        
c                   Input parameters:
c
c  w1,w2 - structures to be added
c  lenw3 - the length of the array w3 that is to contain the 
c        output structure (and also serve as a work array)
c
c                   Output parameters:
c
c  ier - error return code:
c     ier=0 means successful execution
c     ier=32 means that the two structures can not be multiplied
c        due to a dimensionality disareement (m1 \neq n2)
c     ier=64 - the amount of space provided by the user in the
c        array w3 (as specified by lenw3) is insufficient
c  keep3 - the part of array w3 (in real *8 elements) that is
c        accupied by the compressed version of the product matrix
c  lused3 - the amount of memory in w3 (in real *8 words) that was
c        actually used by the subroutine
c
c
c        . . . unpack the structures
c
        ier=0
c
        call p48_unpk(n,m,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
        if( (n2 .ne. n) .or. (m2 .ne. m) ) then
            ier=1024
            return
        endif
c
        eps=eps1
        if(eps2 .gt. eps1) eps=eps2
c
c        allocate memory
c
        iww=1
c
        ll=(ncols1+ncols2)*m
        ll2=(nrows1+nrows2)*n
        if(ll .lt. ll2) ll=ll2
        ll=ll+10
c
        lww=ll 
c
        jexpand1=iww+lww 
        lexpand1=ll 
c
        jexpand2=jexpand1+lexpand1
        lexpand2=ll 
c
        iaskel=jexpand2+lexpand2
        laskel=(ncols1+ncols2)**2 +10
c
        irnorms=iaskel+laskel
        lrnorms=m+2
c
        jicols=irnorms+lrnorms 
        licols=m+2 
c
        jnoticols=jicols+licols
        lnoticols=m+2 
c
        jicols_out=jnoticols+lnoticols
        licols_out=n+2 
c
        jnoticols_out=jicols_out+licols_out
        lnoticols_out=n+2 +m
c
        iw8=jnoticols_out+lnoticols_out
        lw8=(ncols1+ncols2)*m+10
c
        iw7=iw8+lw8
        lenw7=lenw3-iw7-1
c
c        expand the "eval" matrices
c
        itype=3
        ifijs=0
c
        call p48_expand_expandmatr(w1(iexpand1),w3(jexpand1),
     1      w1(iicols1),w1(inoticols1),ncols1,m,itype,
     2      ijs_expand,nijs_expand,ifijs)
c
        call p48_expand_expandmatr(w2(iexpand2),w3(jexpand2),
     2      w2(iicols2),w2(inoticols2),ncols2,m,itype,
     1      ijs_expand,nijs_expand,ifijs)
c
c        construct the structure representing the sum of matrices
c
        call p48_structures_add0(jer,m,ncols1,w3(jexpand1),
     1      ncols2,w3(jexpand2),w3(iww),eps,w3(iaskel),
     2      w1,w2,w3(iw7),lenw7,
     3      w3(irnorms),w3(jicols),w3(jnoticols),w3(jicols_out),
     4      w3(jnoticols_out),w3(iw8),iwout,keepout)
c
        if(jer .ne. 0) then
            ier=64
            goto 4200
        endif
c
        call p48_copy(w3(iw7+iwout-1),w3,keepout)
c
 4200 continue
c
        keep3=keepout
        lused3=iw7+iwout+keepout
c
        return
        end
c 
c 
c 
c 
c
        subroutine p48_structures_add0(ier,m,ncols1,expand1,
     1      ncols2,expand2,ww,eps,askel,w1,w2,w7,lenw7,
     2      rnorms,icols,noticols,icols_out,
     3      noticols_out,w8,iwout,keepout)
        implicit real *8 (a-h,o-z)
        save
        dimension expand1(ncols1,m),expand2(ncols2,m),
     1      ww(ncols1+ncols2,m),rnorms(1),icols(1),
     2      noticols(1),askel(ncols1+ncols2,1),
     3      w7(1),w1(1),w2(1),noticols_out(1),
     4      icols_out(1),w8(1)
c
c        merge the "expand" matrices
c
        ier=0
c
        do 1300 i=1,m
c
        do 1200 j=1,ncols1
c
        ww(j,i)=expand1(j,i)
 1200 continue
 1300 continue
c
        do 1600 i=1,m
        do 1400 j=1,ncols2
c
        ww(j+ncols1,i)=expand2(j,i)
 1400 continue
 1600 continue
c
        nn=ncols1+ncols2
c
c        reskeletonize them things
c
        ifinterp=1
c
        call p48_interp(ww,nn,m,eps,ifinterp,
     1      rnorms,ncols,icols,noticols,w7,w7(1+nn*m))
c
c        construct the proxy matrix
c
        do 2600 i=1,ncols
c
        ii=icols(i)
        do 2400 j=1,nn
c
        askel(j,i)=ww(j,ii)
 2400 continue
 2600 continue
c
c        expand www into a full matrix
c
        itype=3
        ifijs=0
c
        call p48_expand_expandmatr(w7,w8,
     1      icols,noticols,ncols,m,itype,
     2      ijs_expand,nijs_expand1,ifijs)
c
c        unpack the structures w1, w2
c
        call p48_unpk(n,m,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
c        multiply askel by askel1, askel2
c
        call p48_mult1_tricky(askel,w1(iaskel1),w2(iaskel2),w7,
     1      ncols1,ncols2,ncols,nrows1,nrows2)
c
c        multiply by eval1, eval2
c
        itype=3
        ifijs=0
        call p48_expand_evalmatr(w1(ieval1),expand1,
     1      w1(iirows1),w1(inotirows1),nrows1,n,itype,
     2      ijs_eval,nijs_eval1,ifijs)
c
        call p48_expand_evalmatr(w2(ieval2),expand2,
     1      w2(iirows2),w2(inotirows2),nrows2,n,itype,
     2      ijs_eval,nijs_eval2,ifijs)
c
        call p48_mult2_tricky(w7,expand1,expand2,ww,
     1      ncols1,ncols2,ncols,nrows1,nrows2,n)
c
c        skeletonize the obtained matrix ww(n,ncols)
c
        call p48_skeletonize(ier,ww,n,ncols,eps,
     1      w7,lenw7,keep7,lused7)
c
        if(jer .ne. 0) then
            ier=256
            return
        endif        
c
c       . . . unpack the result of skeletonization
c
        call p48_unpk(n7,m7,eps,
     1      ncols7,nrows7,iicols7,inoticols7,iirows7,inotirows7,
     2      iexpand7,ieval7,iaskel7,w7)
c
c       multiply expand7(ncols7,ncols) by expand3(ncols,m),
c       obtaining the final "expand" matrix
c
        itype=3
        ifijs=0
        call p48_expand_expandmatr(w7(iexpand7),expand2,
     1      w7(iicols7),w7(inoticols7),ncols7,m7,itype,
     2      ijs_expand,nijs_expand1,ifijs)
c
        call p48_matmult(expand2,w8,ww,ncols7,ncols,m)
c
c        obtain the "icols" vector for the final "expand"
c
        call p48_icols3_bld(icols,w7(iicols7),icols_out,ncols7,
     1      m,noticols_out)
c
c       squeeze the final "expand" matrix
c
        call p48_squeeze_expandmatr(ww,expand1,
     1      icols_out,noticols_out,ncols7,m)
c
c        pack the new structure
c
        iwout=keep7+1
        lenwout=lenw7-iwout
c
        call p48_pack(jer,n,m,eps,
     1      ncols7,icols_out,noticols_out,nrows7,w7(iirows7),
     2      w7(inotirows7),expand1,w7(ieval7),w7(iaskel7),
     3      w7(iwout),lenwout,keepout)
c
        if(jer .ne. 0) ier=64
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_icols3_bld(icols,icols7,icols_out,ncols7,
     1      m,noticols_out)
        implicit real *8 (a-h,o-z)
        save
        dimension icols(1),icols7(1),icols_out(1),noticols_out(1)
c
c       construct the array icols_out
c
        do 1600 i=1,ncols7
c
        j=icols7(i)
        icols_out(i)=icols(j)
 1600 continue
c
c       construct the array noticols_out
c
        ii=0
        do 2400 i=1,m
c
        do 2200 j=1,ncols7
c
        if(icols_out(j) .eq. i) goto 2400
c
 2200 continue
c
        ii=ii+1
        noticols_out(ii)=i
 2400 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_mult2_tricky(baskel,eval1,eval2,baskel_out,
     1      ncols1,ncols2,ncols,nrows1,nrows2,n)
        implicit real *8 (a-h,o-z)
        save
        dimension baskel(nrows1+nrows2,ncols),eval1(n,nrows1),
     1      eval2(n,nrows2),baskel_out(n,ncols)
c
c        apply eval1
c
        do 2800 i=1,ncols
c
        do 1600 j=1,n
c
        baskel_out(j,i)=0
        do 1400 k=1,nrows1
c
        baskel_out(j,i)=baskel_out(j,i)+eval1(j,k)*baskel(k,i)
 1400 continue
 1600 continue
c
        do 2600 j=1,n
c
        do 2400 k=1,nrows2
c
        baskel_out(j,i)=baskel_out(j,i)+
     1      eval2(j,k)*baskel(k+nrows1,i)
 2400 continue
 2600 continue
c
 2800 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_mult1_tricky(askel,askel1,askel2,askel_out,
     1      ncols1,ncols2,ncols,nrows1,nrows2)
        implicit real *8 (a-h,o-z)
        save
        dimension askel(ncols1+ncols2,ncols),askel1(nrows1,ncols1),
     1      askel2(nrows2,ncols2),askel_out(nrows1+nrows2,ncols)
c
c        apply askel1
c
        do 2800 i=1,ncols
c
        do 1600 j=1,nrows1
c
        askel_out(j,i)=0
        do 1400 k=1,ncols1
c
        askel_out(j,i)=askel_out(j,i)+askel1(j,k)*askel(k,i)
 1400 continue
 1600 continue
c
        do 2600 j=1,nrows2
c
        jj=j+nrows1
        askel_out(jj,i)=0
c
        do 2400 k=1,ncols2
c
        askel_out(jj,i)=askel_out(jj,i)+askel2(j,k)*askel(k+ncols1,i)
 2400 continue
 2600 continue
c
 2800 continue
c
        return
        end
c
c
c
c
c
        subroutine p48_recompress(ier,w1,eps,w3,lenw3,keep3,lused3)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w3(1)
c
c        This subroutine recompresses the user-supplied structure w1,
c        returning the recompressed structure in the array w3, which 
c        is also used as a work array (see below)
c
c                Input parameters:
c
c  w1- the array containing the structure to be rscompressed
c  eps - the precision to which the structure is to be recompressed
c  lenw3 - the length of the output/work array w3 (in real *8 words)
c  
c                Output parameters:
c
c  w3 - the recompressed version of the structure w1
c  keep3 - the amount of space in array w3 (in real *8 words) 
c        occupied by the recompressed structure
c
c  lused3 - the amount of space in array w3 actually used by the 
c        subroutine (this will be several times greater than keep3)
c     
c
c        . . . unpack the input matrix
c
        ier=0
c
        call p48_unpk(n,m,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
c        skeletonize the matrix askel and unpack its compressed
c        representation
c
        iw2=1
c    
        call p48_skeletonize(jer,w1(iaskel1),nrows1,ncols1,eps,
     1      w3(iw2),lenw3,keep,lused)
c
        lused3=iw2+lused+2
c
        if(jer .ne. 0) then
            ier=16
            return
        endif
c
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w3(iw2))
c
c        allocate memory
c
        ll=n*nrows1
        ll2=m*ncols1
        if(ll2 .gt. ll) ll=ll2



        nk=n
        if(m .gt. nk) nk=m
        i=ncols1
        if(nrows1 .gt. i) i=nrows1
c
        ll=nk*i+10

c
        lll=ncols1
        if(nrows1 .gt. lll) lll=nrows1
c
        llll=n
        if(m .gt. n) llll=m
c        
        iexpand33=iw2+keep+2    
        lexpand33=ll+2
c
        ieval33=iexpand33+lexpand33
        leval33=ll+2
c
        iexpand11=ieval33+leval33
        lexpand11=ll+2
c
        iicols3=iexpand11+lexpand11
        licols3=lll+1
c
        iirows3=iicols3+licols3
        lirows3=lll+1
c
        inoticols3=iirows3+lirows3
        lnoticols3=llll+1
c
        inotirows3=inoticols3+lnoticols3
        lnotirows3=llll+1
c
        iw3=inotirows3+lnotirows3
        lenw33=lenw3-iw3-2
c
        call p48_recompress0(jer,w1,w3(iw2),w3(iw3),lenw3,
     1      w3(iexpand33),w3(ieval33),w3(iexpand11),w3(iicols3),
     2      w3(iirows3),w3(inoticols3),w3(inotirows3),keep3)
c
        lused4=iw3+keep3+2
        if(lused4 .gt. lused3) lused3=lused4
c
        if(jer .ne. 0) then
            ier=1024
            return
        endif
c
        call p48_copy(w3(iw3),w3,keep3+2)
c
        return
        end       
c 
c 
c 
c 
c 
        subroutine p48_recompress0(ier,w1,w2,w3,lenw3,
     1      expand33,eval33,expand11,icols3,irows3,
     2      noticols3,notirows3,keep3)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),w3(1),
     1      icols3(1),noticols3(1),irows3(1),notirows3(1),
     2      expand33(1),expand11(1),eval33(1)
c
c        unpack the input matrix
c
        ier=0
c
        call p48_unpk(n,m,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
c        unpack the compressed representation of the compressed askel
c
        call p48_unpk(n2,m2,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
c        multiply the matrices expand22, expand11
c
        call p48_expand2_expand1(ncols2,ncols1,m,
     1      w1,w2,w2(iicols2),w2(inoticols2),expand11,w3,expand33,
     2      w1(iicols1),w1(inoticols1) )
c
c        multiply the matrices eval11, eval22
c       
        call p48_eval1_eval2(nrows2,nrows1,n,
     1      w1,w2,w2(inotirows2),expand11,w3,eval33,
     2      w1(inotirows1),w1(iirows1),w2(iirows2) )
c
c       construct the arrays icols3, irows3
c
        call p48_recompress_indices(
     1      w1(iicols1),ncols1,w1(iirows1),nrows1,
     2      w2(iicols2),ncols2,w2(iirows2),nrows2,
     3      icols3,ncols3,irows3,nrows3,
     4      noticols3,notirows3,n,m)
c
c        squeeze the matrices expand33, eval33
c
        call p48_squeeze_expandmatr(expand33,w3,
     1      icols3,noticols3,ncols3,m)
c
        call p48_copy(w3,expand33,(m-ncols3)*ncols3)
c        
        call p48_squeeze_evalmatr(eval33,w3,
     1      irows3,notirows3,nrows3,n)
c
        call p48_copy(w3,eval33,(n-nrows3)*nrows3)
c
c        pack the data obtained for the product matrix
c        in the array w3
c
        call p48_pack(jer,n,m,eps,
     1      ncols3,icols3,noticols3,nrows3,irows3,
     2      notirows3,expand33,eval33,w2(iaskel2),
     3      w3,lenw3,keep3)
c
        if(jer .ne. 0) ier=32
        return
        end
c
c
c 
c 
c 
        subroutine p48_mult_coef(coef,w)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1)
c
c        This subroutine multiplies by the coefficient coef
c        the skeletonized matrix supplied by the user in the
c        array w
c        
c
c        . . . unpack the structure in array w
c
        call p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
c
c        . . . multiply
c
        do 1200 i=1,ncols*nrows
c
        w(iaskel+i-1)=w(iaskel+i-1)*coef
 1200 continue
c
        return
c
c
c
c
        entry p48_sign_change(w)
c
c        This subroutine multiplies by -1 the skeletonized 
c        matrix supplied by the user in the array w
c        
c        . . . unpack the structure in array w
c
        call p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
c
c        . . . change sign
c
        do 2200 i=1,ncols*nrows
c
        w(iaskel+i-1)=-w(iaskel+i-1)
 2200 continue
        return
        end
c
c
c
c
c
        subroutine p48_squeeze_expandmatr(expand_in,expand_out,
     1      icols,noticols,ncols,m)
        implicit real *8 (a-h,o-z)
        save
        dimension icols(ncols),noticols(m-ncols),
     1      expand_out(ncols,1),expand_in(ncols,m)
c
c        This subroutine converts the input "expand" matrix in 
c        the regular form (i.e. containing the identity submatrix 
c        in the appropriate positions) into the compressed form 
c        of the said "expand" matrix 
c
c                   Input parameters:
c
c  expand_in - "expand" matrix in the compressed form
c  icols - the integer array containing the columns of the 
c        matrix stored in the structure w that are elements of
c        the column skeleton of the said matrix
c  noticols - the integer array containing the columns of the 
c        matrix stored in the structure w that are NOT elements of
c        the column skeleton of the said matrix
c  ncols - the number of elements in the column skeleton of the 
c        matrix in w
c  m - the second dimension of the matrix whose skeleton we are
c        dealing with
c
c                   Output parameters:
c
c  expand_out - the "expand" matrix in the regular form 
c
c        expand the "new" expand matrix, obtaining the 
c        "old" one
c
        do 2400 i=1,m-ncols
c
        ii=noticols(i)
c
        do 2000 j=1,ncols
c
        expand_out(j,i)=expand_in(j,ii)
 2000 continue
c
 2400 continue
c
        return
        end
c
c
c
c
c
        subroutine p48_squeeze_evalmatr(eval_in,eval_out,
     1      irows,notirows,nrows,n)
        implicit real *8 (a-h,o-z)
        save
        dimension irows(nrows),notirows(n-nrows),
     1      eval_out(n-nrows,nrows),eval_in(n,nrows)
c
c        This subroutine converts the input "eval" matrix in 
c        the regular form (i.e. containing the identity submatrix 
c        in the appropriate positions) into the "eval" matrix in 
c        the compressed form 
c
c                   Input parameters:
c
c  eval_in - "eval" matrix in the regular form
c  irows - the integer array containing the rows of the matrix 
c        stored that are elements of the row skeleton of the said 
c        matrix
c  notirows - the integer array containing the rows of the matrix 
c        that are NOT elements of the row skeleton of the said 
c        matrix
c  nrows - the number of elements in the row skeleton of the 
c        matrix 
c  n - the second dimension of the matrix whose skeleton we are
c        dealing with
c
c                   Output parameters:
c
c  eval_out - the "eval" matrix in the regular form 
c
c
c        . . . expand the "new" eval matrix, obtaining the 
c              "old" one
c
        do 2400 i=1,n-nrows
c
        ii=notirows(i)
c
        do 2000 j=1,nrows
c
        eval_out(i,j)=eval_in(ii,j)
 2000 continue
c
 2400 continue
c
        return
        end
c
c
c
c
c
        subroutine p48_expand_expandmatr(expand_in,expand_out,
     1      icols,noticols,ncols,m,itype,ijs,nijs7,ifijs)
        implicit real *8 (a-h,o-z)
        save
        dimension icols(ncols),noticols(m-ncols),
     1      expand_in(ncols,m-ncols),expand_out(ncols,m),
     2      ijs(2,1)
c
c        This subroutine converts the input "expand" matrix in 
c        the compressed form into the regular  (i.e. containing 
c        the identity submatrix in the appropriate positions)
c        form of the said "expand" matrix 
c
c                   Input parameters:
c
c  expand_in - "expand" matrix in the regular form
c  icols - the integer array containing the columns of the 
c        matrix stored in the structure w that are elements of
c        the column skeleton of the said matrix
c  noticols - the integer array containing the columns of the 
c        matrix stored in the structure w that are NOT elements of
c        the column skeleton of the said matrix
c  ncols - the number of elements in the column skeleton of the 
c        matrix in w
c  m - the second dimension of the matrix whose skeleton we are
c        dealing with
c  itype - integer parameter telling the subroutine which entries
c        in the expanded matrix are to be returned (and which are 
c        to be set to zero). Explanation:
c     itype=1 means that only the identity submatrix is to be returned
c     itype=2 means that only the actual interpolation part is to be
c       returned, with the identity submatrix set to zero
c     itype=3 means that the whole uncompressed interpolation 
c       matrix is to be returned
c  ifijs - tells the subroutine whether the parameters ijs, nijs7
c        are to be returned:
c     ifijs=1 means that the parameters ijs, nijs7 are to be returned
c     ifijs=0 means that the parameters ijs, nijs7 are NOT to be returned
c
c                   Output parameters:
c
c  expand_out - the "expand" matrix in the uncompressed form 
c  ijs - integer array dimensioned (2,nijs7) containing the locations
c       in the matrix expand_out of the elements set to 1 (a part of 
c       the identity submatrix). The element ijs(1,i) contains the 
c       row where 1 is located, and ijs(2,i) contains the column
c  nijs7 - the number of rows in array ijs 
c
c
c        . . . expand the "new" expand matrix, obtaining the 
c              "old" one
c
        call p48_setzero(expand_out,m*ncols)
c
        nijs=0
        do 2400 i=1,m
c
c        if this column of a is an element of the skeleton,
c        insert 1 in the appropriate position in the i-th
c        column, and be done
c
        if(ifijs .eq. 0) goto 1400
c
c        the user ahs requested that array ijs be created. 
c        act accordingly
c
        do 1200 j=1,ncols
c
        if( icols(j) .eq. i ) then
            if(itype .ne. 2) expand_out(j,i)=1
            nijs=nijs+1
            ijs(1,nijs)=j
            ijs(2,nijs)=i
            goto 2400
        endif
 1200 continue
c
        goto 1700
c
 1400 continue
c
c        the user ahs requested that the construction of array
c        ijs be skipped. act accordingly
c
        do 1600 j=1,ncols
c
        if( icols(j) .eq. i ) then
            if(itype .ne. 2) expand_out(j,i)=1
            goto 2400
        endif
 1600 continue
c
 1700 continue
c
c       this column is not in the skeleton; find its
c       location in the array noticols
c
        if(itype .eq. 1) goto 2200
c
        do 1800 j=1,m-ncols
c
        if(noticols(j) .eq. i ) jj=j
 1800 continue
c
        do 2000 j=1,ncols
c
        expand_out(j,i)=expand_in(j,jj)
 2000 continue
c
 2200 continue
c
 2400 continue
c
        if(ifijs .ne. 0) nijs7=nijs
c
        return
        end
c
c
c
c
c
        subroutine p48_expand_evalmatr(eval_in,eval_out,
     1      irows,notirows,nrows,n,itype,ijs,nijs7,ifijs)
        implicit real *8 (a-h,o-z)
        save
        dimension irows(nrows),notirows(n-nrows),
     1      eval_in(n-nrows,nrows),eval_out(n,nrows),
     2      ijs(2,1)
c
c        This subroutine converts the input compressed "eval" 
c        into the "eval" matrix in the regular form (i.e. 
c        containing the identity submatrix in the appropriate 
c        positions).
c
c                   Input parameters:
c
c  eval_in - "eval" matrix in the compressed form
c  irows - the integer array containing the rows of the matrix 
c        stored that are elements of the row skeleton of the said 
c        matrix
c  notirows - the integer array containing the rows of the matrix 
c        that are NOT elements of the row skeleton of the said 
c        matrix
c  nrows - the number of elements in the row skeleton of the 
c        matrix 
c  n - the second dimension of the matrix whose skeleton we are
c        dealing with
c
c                   Output parameters:
c
c  eval_out - the "eval" matrix in the regular form 
c  itype - integer parameter telling the subroutine which entries
c        in the expanded matrix are to be returned (and which are 
c        to be set to zero). Explanation:
c     itype=1 means that only the identity submatrix is to be returned
c     itype=2 means that only the actual interpolation part is to be
c       returned, with the identity submatrix set to zero
c     itype=3 means that only the whole uncompressed interpolation 
c       matrix is to be returned
c  ifijs - tells the subroutine whether the parameters ijs, nijs7
c        are to be returned:
c     ifijs=1 means that the parameters ijs, nijs7 are to be returned
c     ifijs=0 means that the parameters ijs, nijs7 are NOT to be returned
c
c
c        . . . expand the "new" eval matrix, obtaining the 
c              "old" one
c
        call p48_setzero(eval_out,n*nrows)
c
        nijs=0
        do 2400 i=1,n
c
c        if this row of a is an element of the skeleton,
c        insert 1 in the appropriate position in the i-th
c        column, and be done
c
        if(ifijs .eq. 0) goto 1400
c
c        the user ahs requested that array ijs be created. 
c        act accordingly
c
        do 1200 j=1,nrows
c
        if( irows(j) .eq. i ) then
            if(itype .ne. 2) eval_out(i,j)=1
            nijs=nijs+1
            ijs(1,nijs)=i
            ijs(2,nijs)=j
c   
            goto 2400
        endif
 1200 continue
c
        goto 1700
c
 1400 continue
c
c        the user ahs requested that the construction of array
c        ijs be skipped. act accordingly
c
        do 1600 j=1,nrows
c
        if( irows(j) .eq. i ) then
            if(itype .ne. 2) eval_out(i,j)=1
c   
            goto 2400
        endif
 1600 continue
c
 1700 continue
c
c       this row is not in the skeleton; find its
c       location in the array notirows
c
        if(itype .eq. 1) goto 2200
c
        do 1800 j=1,n-nrows
c
        if(notirows(j) .eq. i ) jj=j
 1800 continue
c
        do 2000 j=1,nrows
c
        eval_out(i,j)=eval_in(jj,j)
 2000 continue
c
 2200 continue
c
 2400 continue
c
        if(ifijs .ne. 0) nijs7=nijs
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_recompress_indices(
     1      icols1,ncols1,irows1,nrows1,
     2      icols2,ncols2,irows2,nrows2,
     3      icols3,ncols3,irows3,nrows3,
     4      noticols3,notirows3,n,m)
        implicit real *8 (a-h,o-z)
        save
        dimension icols1(1),irows1(1),icols2(1),irows2(1),
     1      icols3(1),irows3(1),noticols3(1),notirows3(1)
c
c        construct the composite arrays icols, irows
c
        do 1200 i=1,ncols2
c
        j=icols2(i)
        icols3(i)=icols1(j)
 1200 continue
c
        do 1400 i=1,nrows2
c
        j=irows2(i)
        irows3(i)=irows1(j)
 1400 continue
c
        ncols3=ncols2
        nrows3=nrows2
c
c        construct the arrays noticols3 and notirows3
c
        ii=0
        do 2400 i=1,m
c
        do 2200 j=1,ncols3
c
        if(i .eq. icols3(j)) goto 2400
 2200 continue
c
        ii=ii+1
        noticols3(ii)=i
c
 2400 continue
c
        ii=0
        do 3400 i=1,n
c
        do 3200 j=1,nrows3
c
        if(i .eq. irows3(j)) goto 3400
 3200 continue
c
        ii=ii+1
        notirows3(ii)=i
c
 3400 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_eval1_eval2(nrows2,nrows1,n,
     1      w1,w2,notirows2,eval11,eval22,eval33,
     2      notirows1,irows1,irows2)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),eval22(nrows1,nrows2), 
     1       eval11(n,nrows1),eval33(n,nrows2),notirows2(1), 
     2       irows2(1),notirows1(1),irows1(1)
c
c        This subroutine multiplies the matrix eval1 from the 
c        structure stored in the array w1 by the matrix eval2
c        from the structure stored in the array w2. PLEASE
c        NOTE THAT THE OUTPUT OF THIS SUBROUTINE IS THE MATRIX
C        EVAL33 IN THE UNCOMPRESSED FORM!
c
c                       Input parameters:
c
c  nrows2 - the number of elements in the row skeleton of the 
c        matrix stored in array w2
c  nrows1 - the number of elements in the row skeleton of the 
c        matrix stored in array w1
c  n - the first dimension of the matrix in w1
c  w1 - the array containing the first structure to be multiplied
c  w2 - the array containing the second structure to be multiplied
c  notirows2 - the integer array containing the rows of the matrix 
c        in w2 that are NOT elements of the row skeleton of the said 
c        matrix 
c  notirows1 - the integer array containing the rows of the matrix 
c        in w1 that are NOT elements of the row skeleton of the said 
c        matrix 
c  irows1 - the integer array containing the rows of the matrix 
c        stored in array w1 that are elements of the row skeleton of 
c        the said matrix
c  irows2 - the integer array containing the rows of the matrix 
c        stored in array w2 that are elements of the row skeleton of 
c        the said matrix
c
c                       Output parameters:
c
c  eval33 - UNCOMPRESSED the "eval" matrix corresponding to the product 
c        of the matrices in arrays w1, w2; in other words, eval33 is an
c        array of dimensionality (n,nrows2)
c
c                       Work arrays:
c
c  eval11 - must be at least n*nrows1 real *8 elements long
c  eval22 - must be at least nrows2*nrows1 real *8 elements long
c  
c
c        . . . unpack arrays w1, w2
c
        call p48_unpk(n7,m,eps,
     1      ncols17,nrows17,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(n2,m2,eps,
     1      ncols27,nrows27,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
        call p48_setzero(eval33,n*nrows2)
c
        itype=2
        ifijs=0
        call p48_expand_evalmatr(w1(ieval1),eval11,
     1      w1(iirows1),w1(inotirows1),nrows1,n,itype,
     2      ijs_eval1,nijs_eval1,ifijs)
c
        itype=3
        call p48_expand_evalmatr(w2(ieval2),eval22,
     1      w2(iirows2),w2(inotirows2),nrows2,n2,itype,
     2      ijs_eval2,nijs_eval2,ifijs)
c
c        account for the intercation (2,2) 
c
        do 1600 i=1,n-nrows1
        ii=notirows1(i)
c
        do 1400 j=1,n2-nrows2
        jj=notirows2(j)
c
        do 1200 ll=1,nrows2
c
        eval33(ii,ll)=eval33(ii,ll)+eval11(ii,jj)*eval22(jj,ll)
 1200 continue
 1400 continue
 1600 continue
c
c        account for the interaction (1,3) 
c
        do 2600 j=1,nrows1
c
        i=irows1(j)
cccc        do 2400 k=1,n
        do 2400 k=1,nrows2
c
        eval33(i,k)=eval33(i,k)+eval22(j,k)
 2400 continue
 2600 continue
c
c        account for the intercation (2,1)
c
        do 3600 i=1,nrows2
c
        ii=irows2(i)
        do 3400 j=1,n
c
        eval33(j,i)=eval33(j,i)+eval11(j,ii)
 3400 continue
 3600 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_expand2_expand1(ncols2,ncols1,m,
     1      w1,w2,icols2,noticols2,expand11,expand22,expand33,
     2      icols1,noticols1)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),
     1      expand33(ncols2,m),noticols2(1),
     2      expand11(ncols1,m),expand22(ncols2,ncols1),icols2(1),
     3      noticols1(1),icols1(1)
c
c        This subroutine multiplies the matrix expand2 from the 
c        structure stored in the array w2 by the matrix expand1
c        from the structure stored in the array w1. PLEASE
c        NOTE THAT THE OUTPUT OF THIS SUBROUTINE IS THE MATRIX
C        EXPAND33 IN THE UNCOMPRESSED FORM!
c
c                       Input parameters:
c
c  ncols2 - the number of elements in the column skeleton of the 
c        matrix stored in array w2
c  ncols1 - the number of elements in the column skeleton of the 
c        matrix stored in array w1
c  m - the second dimension of the matrix in w1
c  w1 - the array containing the first structure to be multiplied
c  w2 - the array containing the second structure to be multiplied
c  noticols2 - the integer array containing the colsumns of the 
c        matrix in w2 that are NOT elements of the column skeleton 
c        of the said matrix 
c  noticols1 - the integer array containing the columns of the 
c        matrix in w1 that are NOT elements of the columns skeleton 
c        of the said matrix 
c  icols1 - the integer array containing the columnss of the matrix 
c        stored in array w1 that are elements of the col skeleton of 
c        the said matrix
c  icols2 - the integer array containing the columnss of the matrix 
c        stored in array w2 that are elements of the col skeleton of 
c        the said matrix
c
c                       Output parameters:
c
c  expand33 - UNCOMPRESSED the "expand" matrix corresponding to the 
c        product of the matrices in arrays w1, w2; in other words, 
c        expand33 is an array of dimensionality (n,ncols2)
c
c                       Work arrays:
c
c  expand11 - must be at least m*ncols1 real *8 elements long
c  expand22 - must be at least ncols2*ncols1 real *8 elements long
c  
c
c        . . . unpack arrays w1, w2
c
        call p48_unpk(n1,m1,eps,
     1      ncols17,nrows17,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(n2,m2,eps,
     1      ncols27,nrows27,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
c       account for the case (2,2)
c
        call p48_setzero(expand33,m*ncols2)
c
        itype=3
        ifijs=0
        call p48_expand_expandmatr(w1(iexpand1),expand11,
     1      w1(iicols1),w1(inoticols1),ncols1,m,itype,
     2      ijs_expand1,nijs_expand1,ifijs)
c
        itype=2
        call p48_expand_expandmatr(w2(iexpand2),expand22,
     1      w2(iicols2),w2(inoticols2),ncols2,m2,itype,
     2      ijs_expand2,nijs_expand2,ifijs)
c
        do 1600 i=1,m2-ncols2
        ii=noticols2(i)
c
        do 1400 j=1,m-ncols1
c
        jj=noticols1(j)
c
        do 1200 ll=1,ncols2
c
        expand33(ll,jj)=
     1      expand33(ll,jj)+expand22(ll,ii)*expand11(ii,jj)
 1200 continue
 1400 continue
 1600 continue
c
c        account for the interaction type (1,3)
c
        do 2600 j=1,ncols2
c
        i=icols2(j)
        do 2400 k=1,m
c
        expand33(j,k)=expand33(j,k)+expand11(i,k)
 2400 continue
 2600 continue
c
c       . . . and for the case (2,1)
c
        do 3600 i=1,ncols1
c
        ii=icols1(i)
        do 3400 j=1,ncols2
c
        expand33(j,ii)=expand33(j,ii)+expand22(j,i)
 3400 continue
 3600 continue
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_structures_multiply(ier,w1,w2,
     1      w3,lenw3,keep3,lused)
        implicit real *8 (a-h,o-z)
        save
        dimension w1(1),w2(1),w3(1)
c
c        This subroutine multipies the matrices stored in the
c        structures w1, w2, producing the output structure w3. 
c        
c                   Input parameters:
c
c  w1,w2 - structures to be multiplied
c  lenw3 - the length of the array w3 that is to contain the 
c        output structure (and also serve as a work array)
c
c                   Output parameters:
c
c  ier - error return code:
c     ier=0 means successful execution
c     ier=32 means that the two structures can not be multiplied
c        due to a dimensionality disareement (m1 \neq n2)
c     ier=64 - the amount of space provided by the user in the
c        array w3 (as specified by lenw3) is insufficient
c  keep3 - the part of array w3 (in real *8 elements) that is
c        accupied by the compressed version of the product matrix
c  lused - the amount of memory in w3 (in real *8 words) that was
c        actually used by the subroutine
c
c        . . . unpack the input matrices
c
        ier=0
c
        call p48_unpk(n,m,eps1,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(m2,k,eps2,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
        if(m2 .ne. m) then
            ier=32
            return
        endif
c
c        allocate memory
c
        ninire=2
        ninire = sizeof(x)/sizeof(i)

        iirc=1
        lirc=m/ninire+3
c
        iijs_eval=iirc+lirc
        lijs_eval=2*(ncols1+nrows2)/ninire+3
c
        iijs_expand=iijs_eval+lijs_eval
        lijs_expand=2*(ncols1+nrows2)/ninire+3
c
        iww1=iijs_expand+lijs_expand
        lww1=ncols1*nrows2+2
c
        iexpand11=iww1+lww1
        lexpand11=ncols1*m+2
c
        ieval22=iexpand11+lexpand11
        leval22=nrows2*m+2
c
        iiw3=ieval22+leval22
c
        lenw33=lenw3-iiw3-2   
c
c        calculate expand1 \times eval2 
c
        call p48_expand1_eval2(w1(inoticols1),ncols1,
     1      w2(inotirows2),m,nrows2,w3(iww1),
     2      w3(iexpand11),w3(ieval22),w1,w2,
     3      w3(iijs_eval),w3(iijs_expand),w3(iirc) )
c
c       multiply in askel1 and askel2 : 
c
c       b1(nrows1,ncols1) * ww1(ncols1,nrows2) * b2(nrows2,ncols2)
c
        call p48_matmult(w1(iaskel1),w3(iww1),w3(ieval22),
     1      nrows1,ncols1,nrows2)
c
        call p48_matmult(w3(ieval22),w2(iaskel2),w3(iww1),
     1      nrows1,nrows2,ncols2)
c
c        pack the product matrix into the "standard" structure
c
        eps=eps1
        if(eps2 .gt. eps) eps=eps2
c
        call p48_pack(jer,n,k,eps,
     1      ncols2,w2(iicols2),w2(inoticols2),nrows1,w1(iirows1),
     2      w1(inotirows1),w2(iexpand2),w1(ieval1),w3(iww1),
     3      w3(iiw3),lenw33,keep3)
c     
        if(jer .ne. 0) then
            ier=64
            return
        endif
c
        call p48_copy(w3(iiw3),w3,keep3)
c
        lused=keep3+10+iiw3
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_expand1_eval2(noticols1,ncols1,
     1      notirows2,m,nrows2,
     2      ww1,expand1_out,eval2_out,w1,w2,
     3      ijs_eval,ijs_expand,irc)
        implicit real *8 (a-h,o-z)
        save
        dimension noticols1(m-ncols1),notirows2(m-nrows2),
     1      irc(1),ww1(ncols1,nrows2),w1(1),w2(1),
     2      expand1_out(ncols1,1),eval2_out(m,1),
     3      ijs_expand(2,1),ijs_eval(2,1)
c
c        This subroutine evaluates (somewhat efficiently) the product
c        of an "expand" matrix with an "eval" matrix, under the obvious
c        assumption that the appropriate dimensionalities are the same.
c
c        PLEASE NOTE THAT ON INPUT, THE MATRICES ARE GIVEN AS PARTS
c        OF THE STRUCTURES CONTAINING THEM, BUT ON OUTPUT, THEIR 
C        PRODUCT IS GIVEN AS A SIMPLE (UNCOMPRESSED IN ANY WAY) MATRIX.
c
c                        Input parameters:
c
c  noticols1 - the integer array containing the columns of the 
c        matrix stored in the structure w1 that are NOT elements of
c        the column skeleton of the said matrix
c  ncols1 - the number of elements in the column skeleton of the 
c        matrix in w1      
c  notirows2 - the integer array containing the rows of the 
c        matrix stored in the structure w2 that are NOT elements of
c        the row skeleton of the said matrix
c  nrows2 - the number of elements in the row skeleton of the 
c        matrix in w2
c  w1 - the structure containing the matrix whose "expand" matrix
c        is the first one to be multiplied
c  w2 - the structure containing the matrix whose "eval" matrix
c        is the second one to be multiplied
c
c                        Output parameters:
c
c  ww1 - the ncols1 \times nrows2 matrix that is the product
c        of the matrices expand1 and eval2
c
c                        Work arrays:
c
c  expand1_out - must be at least ncols1*m real *8 locations long
c  eval2_out - must be at least nrows2*m real *8 locations long
c  ijs_eval - must be at least 2*(ncols1+nrows2)+3 integer *4 
c        locations long
c  ijs_expand - must be at least 2*(ncols1+nrows2)+3 integer *4 
c        locations long
c  irc - must be at least m+1 integer *4 locations long
c
c        . . . unpack arrays w1, w2
c
        call p48_unpk(n,m,eps,
     1      ncols1,nrows1,iicols1,inoticols1,iirows1,inotirows1,
     2      iexpand1,ieval1,iaskel1,w1)
c
        call p48_unpk(m,k,eps,
     1      ncols2,nrows2,iicols2,inoticols2,iirows2,inotirows2,
     2      iexpand2,ieval2,iaskel2,w2)
c
c        convert the matrices expand1, eval2 to their
c        "conventional" form
c
        itype=2
        ifijs=1
        call p48_expand_expandmatr(w1(iexpand1),expand1_out,
     1      w1(iicols1),w1(inoticols1),ncols1,m,itype,
     2      ijs_expand,nijs_expand,ifijs)
c
        itype=3
        call p48_expand_evalmatr(w2(ieval2),eval2_out,
     1      w2(iirows2),w2(inotirows2),nrows2,m,itype,
     2      ijs_eval,nijs_eval,ifijs)
c
c        find the locations where noticols1 and notirows2 
c        intersect
c
        nn=0
        do 1400 i=1,m-ncols1
        do 1200 j=1,m-nrows2
c
        if(noticols1(i) .ne. notirows2(j)) goto 1200
c
        nn=nn+1
        irc(nn)=noticols1(i)
 1200 continue
 1400 continue
c
c       . . . multiply
c
        do 2000 i=1,ncols1
        do 1800 j=1,nrows2
c
        ww1(i,j)=0
 1800 continue
 2000 continue
c
        do 2800 ll=1,nn
c
        ii=irc(ll)
        do 2600 i=1,ncols1
        do 2400 j=1,nrows2
c
        ww1(i,j)=ww1(i,j)+expand1_out(i,ii)*eval2_out(ii,j)
 2400 continue
 2600 continue
c
 2800 continue
c
c        evaluate the (1,3) part of the operator
c
        do 3400 l=1,nijs_expand
c
        i=ijs_expand(2,l)
        j=ijs_expand(1,l)
c
        do 3200 k=1,nrows2
c
        ww1(j,k)=ww1(j,k)+eval2_out(i,k)
 3200 continue
c
 3400 continue
c
c        evaluate the (2,1) part of the operator
c
        do 4400 l=1,nijs_eval
c
        i=ijs_eval(1,l)
        j=ijs_eval(2,l)
c
        do 4200 k=1,ncols1
c
        ww1(k,j)=ww1(k,j)+expand1_out(k,i)
 4200 continue
c
 4400 continue

        return
        end
c 
c 
c 
c 
c 
        subroutine p48_skeleton_expand(w,n,m,icols,noticols,ncols,
     1      irows,notirows,nrows,
     2      eval_out,askel,expand_out)
        implicit real *8 (a-h,o-z)
        save
        dimension icols(ncols),noticols(m-ncols),irows(ncols),
     1      notirows(m-ncols),askel(ncols,ncols),
     2      expand_out(1),eval_out(1),w(1)
c
c        This subroutine expands the skeletonization of the 
c        matrix from the compressed format into three regular
c        matrices. It also returns the arrays icols, noticols,
c        irows, notirows. 
c
c                        Input parameters:
c
c  w - the array containing the skeletonization of the matrix to
c        be expanded
c
c                        Output parameters:
c
c  n,m - the dimensionalities of the matrix to be expanded
c  icols - the integer array containing the columns of the 
c        matrix stored in the structure w that are elements of
c        the column skeleton of the said matrix
c  noticols - the integer array containing the columns of the 
c        matrix stored in the structure w that are NOT elements of
c        the column skeleton of the said matrix
c  ncols1 - the number of elements in the column skeleton of the 
c        matrix in w
c  irows - the integer array containing the rows of the 
c        matrix stored in the structure w that are elements of
c        the row skeleton of the said matrix
c  notirows - the integer array containing the rows of the 
c        matrix stored in the structure w that are NOT elements of
c        the row skeleton of the said matrix
c  nrows - the number of elements in the row skeleton of the 
c        matrix in w
c  eval_out - the "eval" matrix for the skeletonized matrix
c        contained in w
c  askel - the skeleton (submatrix) of the matrix stored in w 
c  expand_out - the "expand" matrix for the skeletonized matrix
c        contained in w
c
c        . . . unpack the array components of the skeleton 
c
        call p48_unpack(n,m,eps,
     1      ncols,icols,noticols,nrows,irows,notirows,
     2      expand_out,eval_out,askel,w)
c
c        unpack the scalar components of the skeleton 
c
        call p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
c
c       expand the matrix expand into a full-size one
c
        itype=3
        ifijs=0
        call p48_expand_expandmatr(w(iexpand),expand_out,
     1      w(iicols),w(inoticols),ncols,m,itype,
     2      ijs_expand,njs_expand,ifijs)
c
c        expand the matrix eval into a full-size one
c
        call p48_expand_evalmatr(w(ieval),eval_out,
     1      w(iirows),w(inotirows),nrows,n,itype,
     2      ijs_eval,njs_eval,ifijs)
c
c        extract the matrix askel
c
        call p48_copy(w(iaskel),askel,ncols*nrows)
c
        return
        end
c
c 
c 
c 
c 
        subroutine p48_skeletonize(ier,a,n,m,eps,
     1      w,lenw,keep,lused)
        implicit real *8 (a-h,o-z)
        save
        dimension a(n,m),w(1)
c
c        This subroutine skeletonizes the user-supplied matrix
c        a and returns its skeleton and all the relevant 
c        information in the array w. The data can be retrieved 
c        from the said array w via the subroutines p48_unpack,
c        p48_unpk, p48_vec (see)
c
c                       Input parameters:
c
c  a - the matrix to be skeletonized; not damaged by this 
c        subroutine in any way
c  n,m - the dimensionalities of a 
c  eps - the precision to which a is to be skeletonized
c  lenw - the length of the array w provided by the user
c  
c                       Output parameters:
c
c  ier - error return code:
c    ier=0 means successful execution
c    ier=64 means that the amount lenw of space provided by the user
c        in the array w is insufficient
c  w - the first keep elements of array w contain the information to 
c        be used by the subroutines p48_unpack, p48_unpk, 
c        p48_vec (see)
c  keep - the length (in real *8 elements) of the part of the array 
c        w that must be unchanged between the call to this subroutine
c        and the subsequent calls to the subroutines p48_unpack, 
c        p48_unpk, p48_vec 
c  lused - length (in real *8 elements) of the part of the array 
c        w that actually has been used by the subroutine
c  
c                       Work arrays:
c
c  w - is both work array and output parameter (see the difference 
c        between keep and lused above)
c
c
c        . . . allocate memory
c
        ier=0
        ninire=2
        ninire = sizeof(x)/sizeof(i)

        mn=m
        if(n .gt. m) mn=n
        mn=mn+4
c
        iicols=1
        licols=mn/ninire+2
c
        inoticols=iicols+licols
        lnoticols=mn/ninire+2
c
        iirows=inoticols+lnoticols+2
        lirows=mn/ninire+2
c
        inotirows=iirows+lirows 
        lnotirows=mn/ninire+2
c
        iexpand=inotirows+lnotirows
        lexpand=n*m+2
c
        ieval=iexpand+lexpand
        leval=n*m+2+ m**2/4+m+100 
        leval=n*m*2+ m**2/4+m+100 
c
        iaskel=ieval+leval
        laskel=n*m+2
c
        irnorms=iaskel+laskel
        lrnorms=mn
c
        iwork = irnorms+lrnorms
        lwork = n*m
c
        iw2=iwork+lwork
c
        lenw2=lenw-iw2-2       
c
cccc        call prinf('iw2=*',iw2,1)
c
c        compress the user-supplied matrix a
c
        call p48_skeletonize0(a,n,m,eps,
     1      w(iicols),w(inoticols),ncols,w(iirows),
     2      w(inotirows),nrows,w(iexpand),w(ieval),w(iaskel),
     3      w(irnorms),w(iwork))
c
c        store the skeletonized matrix in array w
c
        call p48_pack(ier,n,m,eps,
     1      ncols,w(iicols),w(inoticols),nrows,w(iirows),
     2      w(inotirows),w(iexpand),w(ieval),w(iaskel),
     3      w(iw2),lenw2,keep)
c
cccc        call prinf('keep=*',keep,1)
c
        lused=iw2+keep
        if(ier .ne. 0) return
c
c       perform garbage collection
c
        call p48_copy(w(iw2),w,keep)
c
        return
        end
c 
c 
c 
c 
c 
      subroutine p48_skeletonize0(a,n,m,eps,
     1      icols,noticols,ncols,irows,notirows,nrows,
     2      expand,eval,askel,rnorms,work)
        implicit real *8 (a-h,o-z)
        save
        dimension a(n,m),rnorms(1),icols(1),
     1      noticols(1),irows(1),notirows(1),
     2      eval(1),expand(1),askel(1),work(1)
c
c        skeletonize the columns
c
        ifinterp=1
        call p48_interp(a,n,m,eps,ifinterp,
     1      rnorms,ncols,icols,noticols,expand,work)

cccc        call prin2('rnorms = *', rnorms, ncols)

cccc        call prin2('


c
c       . . . extract a_{|skel}
c
        ii=0
        do 2600 i=1,n
        do 2400 j=1,ncols
c
        ii=ii+1
        jj=icols(j)
        askel(ii)=a(i,jj)
 2400 continue
 2600 continue
c
c        now, skeletonize the rows
c
        ifinterp=1
        eps2=0
cccc        eps2=eps*10000
cccc        eps2=eps/10000
cccc        eps2=eps
        call p48_interp(askel,ncols,n,eps2,ifinterp,
     1      rnorms,nrows,irows,notirows,eval,work)
c
cccc        nrows=ncols

        call p48_tranit(eval,nrows,n-nrows,askel)
c
c        construct the proxy matrix
c
        call p48_proxycrea(a,n,m,ncols,icols,nrows,irows,askel)
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1)
c
c        This subroutine retrieves from the user-supplied 
c        array w the locations in the said array of various
c        parts of the skeleton structure stored in w. It is
c        assumed that the array w has been created via a 
c        preceding call to the subroutine p48_pack (see)
c
c                    Input parameters:
c
c  w - array containing the skeleton structure
c
c                       Output parameters:
c
c  n - the second dimension of the matrix whose skeleton we are
c        dealing with
c  m - the second dimension of the matrix whose skeleton we are
c        dealing with
c  eps - the accuracy to which the matrix was compressed
c  ncols - the number of elements in the column skeleton of the 
c        matrix 
c  nrows - the number of elements in the row skeleton of the 
c        matrix 
c  iicols - the location in w of the integer array containing the 
c        columns of the matrix stored in the structure w that are 
c        elements of the column skeleton of the said matrix
c  inoticols - the location in w of the integer array containing 
c        the columns of the matrix stored in the structure w that 
c        are NOT elements of the column skeleton of the said matrix
c  iirows - the location in w of the integer array containing the 
c        rows of the matrix stored that are elements of the row 
c        skeleton of the said matrix
c  inotirows - the location in w of the integer array containing 
c        the rows of the matrix that are NOT elements of the row 
c        skeleton of the said matrix
c  iexpand - the location in w of the matrix "expand" in the 
c        compressed form (not containing the identity submatrix)
c  ieval - the location in w of the matrix "eval" in the compressed 
c        form (not containing the identity submatrix)
c  iaskel - the location in w of the skeleton submatrix of the matrix 
c        whose skeletonization we are dealing with
c
c        . . . unpack the pointers, etc. 
c
        n=w(1)
        m=w(2)
        ncols=w(3)
        nrows=w(4)
        eps=w(5)
c
        iicols=w(11)
        inoticols=w(13)
        iirows=w(15)
        inotirows=w(17)
        iexpand=w(19)
        ieval=w(21)
        iaskel=w(23)
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_unpack_silly(n,m,eps,
     1      ncols,icols,noticols,nrows,irows,notirows,
     2      iexpand,ieval,iaskel,w)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1),icols(1),noticols(1),
     1      irows(1),notirows(1)
c
c       This is an obvious hybrid between the subroutines 
c       p48_unpack, p48_unpk
c
c
c        . . . unpack the discrete information
c
        n=w(1)
        m=w(2)
        ncols=w(3)
        nrows=w(4)
        eps=w(5)
c
        iicols=w(11)
        licols=w(12)
c
        inoticols=w(13)
        lnoticols=w(14)
c
        iirows=w(15)
        lirows=w(16)
c
        inotirows=w(17)
        lnotirows=w(18)
c
        iexpand=w(19)
        lexpand=w(20)
c
        ieval=w(21)
        leval=w(22)
c
        iaskel=w(23)
        laskel=w(24)
c
c       copy the arrays
c
        call p48_int_copy(w(iicols),icols,ncols)
        call p48_int_copy(w(inoticols),noticols,m-ncols)
c
        call p48_int_copy(w(iirows),irows,nrows)
        call p48_int_copy(w(inotirows),notirows,n-nrows)
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_unpack(n,m,eps,
     1      ncols,icols,noticols,nrows,irows,notirows,
     2      expand,eval,askel,w)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1),icols(1),noticols(1),
     1      irows(1),notirows(1),expand(1),eval(1),askel(1)
c
c        This subroutine retrieves from the user-supplied 
c        array w the various parts of the skeleton structure 
c        stored in w. It is assumed that the array w has been 
c        created via a preceding call to the subroutine 
c        p48_pack (see). PLEASE note the connection between
c        this subroutine and the subroutine p48_unpk.
c
c                    Input parameters:
c
c  w - array containing the skeleton structure
c
c                       Output parameters:
c
c  n - the second dimension of the matrix whose skeleton we are
c        dealing with
c  m - the second dimension of the matrix whose skeleton we are
c        dealing with
c  eps - the accuracy to which the matrix was compressed
c  ncols - the number of elements in the column skeleton of the 
c        matrix 
c  icols - the integer array containing the 
c        columns of the matrix stored in the structure w that are 
c        elements of the column skeleton of the said matrix
c  noticols - the integer array containing 
c        the columns of the matrix stored in the structure w that 
c        are NOT elements of the column skeleton of the said matrix
c  nrows - the number of elements in the row skeleton of the 
c        matrix 
c  irows - the integer array containing the 
c        rows of the matrix stored that are elements of the row 
c        skeleton of the said matrix
c  notirows - the integer array containing 
c        the rows of the matrix that are NOT elements of the row 
c        skeleton of the said matrix
c  expand - the the matrix "expand" in the 
c        compressed form (not containing the identity submatrix)
c  eval - the matrix "eval" in the compressed 
c        form (not containing the identity submatrix)
c  askel - the skeleton submatrix of the matrix 
c        whose skeletonization we are dealing with
c
c        . . . unpack the pointers, etc. 
c
c        unpack the discrete information
c
        n=w(1)
        m=w(2)
        ncols=w(3)
        nrows=w(4)
        eps=w(5)
c
        iicols=w(11)
        licols=w(12)
c
        inoticols=w(13)
        lnoticols=w(14)
c
        iirows=w(15)
        lirows=w(16)
c
        inotirows=w(17)
        lnotirows=w(18)
c
        iexpand=w(19)
        lexpand=w(20)
c
        ieval=w(21)
        leval=w(22)
c
        iaskel=w(23)
        laskel=w(24)
c
c       copy the arrays
c
        call p48_int_copy(w(iicols),icols,ncols)
        call p48_int_copy(w(inoticols),noticols,m-ncols)
c
        call p48_int_copy(w(iirows),irows,nrows)
        call p48_int_copy(w(inotirows),notirows,n-nrows)
c
        call p48_copy(w(iexpand),expand,lexpand)
        call p48_copy(w(ieval),eval,leval)
c
        call p48_copy(w(iaskel),askel,laskel)
c
        return
        end
c
c 
c 
c 
c 
        subroutine p48_pack(ier,n,m,eps,
     1      ncols,icols,noticols,nrows,irows,notirows,
     2      expand,eval,askel,w,lenw,keep)
        implicit real *8 (a-h,o-z)
        save
        dimension w(1),icols(1),noticols(1),
     1      irows(1),notirows(1),expand(1),eval(1),askel(1)
c
c        This subroutine stores the user-supplied skeleton 
c        structure for a compressed  matrix in the array,
c        from which said array the said structure can be 
c        extracted via the subroutines p48_unpk, p48_unpack,
c        or p48_unpack_silly (see). 
c
c                    Input parameters:
c
c  n - the first dimension of the matrix whose skeleton we are
c        dealing with
c  m - the second dimension of the matrix whose skeleton we are
c        dealing with
c  eps - the accuracy to which the matrix was compressed
c  ncols - the number of elements in the column skeleton of the 
c        matrix 
c  icols - the integer array containing the columns of the 
c        matrix stored in the structure w that are elements of
c        the column skeleton of the said matrix
c  noticols - the integer array containing the columns of the 
c        matrix stored in the structure w that are NOT elements of
c        the column skeleton of the said matrix
c  nrows - the number of elements in the row skeleton of the 
c        matrix 
c  irows - the integer array containing the rows of the matrix 
c        stored that are elements of the row skeleton of the said 
c        matrix
c  notirows - the integer array containing the rows of the matrix 
c        that are NOT elements of the row skeleton of the said 
c        matrix
c  expand - the matrix "expand" in the compressed form (not containing
c        the identity submatrix)
c  eval - the matrix "eval" in the compressed form (not containing
c        the identity submatrix)
c  askel - the skeleton sunmatrix of the matrix whose skeletonization 
c        we are 
c  lenw - the length of the user-supplied array w (in real *8 words)
c
c                       Output parameters:
c
c  ier - error return code
c    ier=0 means successful execution
c    ier=64 means that the amount of space provided by the user in 
c        array w was insufficient
c  w - the array containing the constructed structure
c  keep - the actual amount of space in array w occupied by the 
c        structure
c
c        . . . allocate storage
c
        ier=0
        ninire=2
        ninire = sizeof(x)/sizeof(i)
c
        w(1)=n+0.1
        w(2)=m+0.1
        w(3)=ncols+0.1
        w(4)=nrows+0.1
        w(5)=eps
c
        iicols=41
        licols=ncols/ninire+2
c
        inoticols=iicols+licols
        lnoticols=(m-ncols)/ninire+2
c
        iirows=inoticols+lnoticols+2
        lirows=nrows/ninire+2
c
        inotirows=iirows+lirows 
        lnotirows=(n-nrows)/ninire+2
c
        iexpand=inotirows+lnotirows
        lexpand=(m-ncols)*ncols+2 
c
        ieval=iexpand+lexpand 
        leval=(n-nrows)*nrows+2
c
        iaskel=ieval+leval 
        laskel=ncols*nrows+2
c
c        store all this information in array w
c
        w(11)=iicols+0.1 
        w(12)=licols+0.1
c
        w(13)=inoticols+0.1
        w(14)=lnoticols+0.1
c
        w(15)=iirows+0.1
        w(16)=lirows+0.1
c
        w(17)=inotirows+0.1
        w(18)=lnotirows+0.1
c
        w(19)=iexpand+0.1
        w(20)=lexpand+0.1
c
        w(21)=ieval+0.1
        w(22)=leval+0.1
c
        w(23)=iaskel+0.1
        w(24)=laskel+0.1
c
        keep=iaskel+laskel
        w(6)=keep+0.1
c
        if(keep .gt. lenw) then
            ier=64
            return
        endif
c
c       copy the arrays
c
        call p48_int_copy(icols,w(iicols),ncols)
        call p48_int_copy(noticols,w(inoticols),m-ncols)
c
        call p48_int_copy(irows,w(iirows),nrows)
        call p48_int_copy(notirows,w(inotirows),n-nrows)
c
        call p48_copy(expand,w(iexpand),lexpand)
        call p48_copy(eval,w(ieval),leval)
c
        call p48_copy(askel,w(iaskel),laskel)
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_vec(w,x,y,w2)
        implicit real *8 (a-h,o-z)
        save
        dimension x(1),y(1),w(1),w2(1)
c
c        This subroutine applies to the user-supplied vector x
c        the compressed version of the matrix a constructed via
c        a preceding call to the subroutine p48_skeletonize
c        (see). Please note that this is a memory management 
c        routine. All real work is done by the subroutine 
c        p48_vec0 (see)
c
c                 Input parameters:
c
c  w - the array of data created by the subroutine p48_skeletonize
c  x - array of length m to which the matrix is to be applied
c
c                 Output parameters:
c
c  y - vector of length n which is the result of applying the matrix 
c        a to the vector x
c
c                 Work array:
c
c  w2 - must be at least 2*max(n,m)+2 real *8 elements long
c
c
c       . . . unpack the compressed matrix
c
        call p48_unpk(n,m,eps,
     1      ncols,nrows,iicols,inoticols,iirows,inotirows,
     2      iexpand,ieval,iaskel,w)
c
c        . . . apply the matrix to the vector
c
        iw1=1
        nm=m
        if(nm .lt. n) nm=n
        iw2=iw1+nm+1
c
        call p48_vec0(n,m,w(iicols),ncols,w(inoticols),
     1       w(iirows),w(inotirows),w(iexpand),w(ieval),w(iaskel),
     2       x,y,w2(iw1),w2(iw2),nrows )
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_vec0(n,m,icols,ncols,noticols,
     1      irows,notirows,expand,eval,askel,x,y,w,w2,nrows)
        implicit real *8 (a-h,o-z)
        save
        dimension icols(ncols),noticols(n-ncols),irows(nrows),
     1      notirows(m-nrows),askel(nrows,ncols),
     2      expand(ncols,m-ncols),eval(n-nrows,nrows),x(1),y(1),
     3      w(1),w2(1)
c
c        apply expand to x
c
        do 1200 i=1,m-ncols
c
        j=noticols(i)
        w(i)=x(j)
 1200 continue
c
        call p48_matvec(expand,ncols,m-ncols,w,w2)
c
        do 1400 i=1,ncols
c
        j=icols(i)
        w2(i)=w2(i)+x(j)
 1400 continue
c
c        apply askel to w2
c
        call p48_matvec(askel,nrows,ncols,w2,w)
c
c        apply eval to w
c
        call p48_matvec(eval,n-nrows,nrows,w,w2)
c
        do 1600 i=1,n-nrows
c
        j=notirows(i)
        y(j)=w2(i)
 1600 continue
c
        do 1800 i=1,nrows
c
        j=irows(i)
        y(j)=w(i)
 1800 continue
c
        return
        end

c 
c 
c 
c 
c 
        subroutine p48_scap(x,y,n,prod)
        implicit real *8 (a-h,o-z)
        save
        dimension x(1),y(1)
c 
        prod=0
        do 1200 i=1,n
        prod=prod+x(i)*(y(i))
 1200 continue
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_proxycrea(a,n,m,ncols,icols,nrows,irows,b)
        implicit real *8 (a-h,o-z)
        save
        dimension a(n,m),b(nrows,ncols),icols(1),irows(1)
c
        do 1400 i=1,ncols
        do 1200 j=1,nrows
c
        jj=irows(j)
        ii=icols(i)
        b(j,i)=a(jj,ii)
 1200 continue
 1400 continue
c
        return
        end
c 
c 
c 
c 
c 
c 
        subroutine p48_int_copy(x,y,n)
        implicit real *8 (a-h,o-z)
cccc        integer*4 x(1),y(1)
        integer x(1),y(1)
c 
        do 2400 i=1,n
        y(i)=x(i)
 2400 continue
        return
        end
c 
c 
c 
c 
c 
c 
        subroutine p48_copy(x,y,n)
        implicit real *8 (a-h,o-z)
        save
        dimension x(1),y(1)
c 
        do 2400 i=1,n
        y(i)=x(i)
 2400 continue
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_tranit(a,n,m,b)
        implicit real *8 (a-h,o-z)
        save
        dimension a(n,m),b(m,n)
c
        do 1400 i=1,m
        do 1200 j=1,n
c
        b(i,j)=a(j,i)
 1200 continue
 1400 continue
c
        call p48_copy(b,a,n*m)
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_matmult(a,x,c,n,m,k)
        implicit real *8 (a-h,o-z)
        save
        dimension a(n,m),x(m,k),c(n,k)
c 
        do 1600 i=1,n
        do 1400 j=1,k
c 
        cd=0
        do 1200 ll=1,m
c 
        cd=cd+a(i,ll)*x(ll,j)
 1200 continue
c 
        c(i,j)=cd
 1400 continue
 1600 continue
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_rleasts2(u,t,n,m,ncols,y,x,work,ipivots)
        implicit real *8 (a-h,o-z)
        save
        real *8 u(n,ncols),t(ncols,ncols)
        dimension x(m),y(n),work(ncols),ipivots(1)
c 
c        . . . apply to the right-hand side the matrux  U^*
c 
        do 1400 i=1,ncols
        d=0
        do 1200 j=1,n
        d=d+(u(j,i))*y(j)
 1200 continue
        work(i)=d
 1400 continue
c 
c       apply to the vector work the inverse of the matrix t
c 
        x(ncols)=work(ncols)/t(ncols,ncols)
c        
        do 2000 i=ncols-1,1,-1
c 
        d=0
        do 1600 j=i+1,ncols
        d=d+t(i,j)*x(j)
 1600 continue
c 
        x(i)=(work(i)-d)/t(i,i)
 2000 continue
c 
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_rleastsq(a,n,m,b,v,ncols,rnorms,eps,ipivots,
     1      ifvmatr)
        implicit real *8 (a-h,o-z)
        save
        real *8 a(n,m),b(n,m),v(1)
        dimension rnorms(1),ipivots(1)
c
        call p48_copy(a,b,n*m)
c 
c        apply the gram-schmidt process (with pivoting) to b
c 
        call p48_reskel_piv2(b,n,m,eps,rnorms,ipivots,ncols)
c 
c        project the original matrix on the obtained orthogonal
c        columns
c 
        if(ifvmatr .eq. 0) return
c
        do 1600 i=1,ncols**2
        v(i)=0
 1600 continue
c
        ii=0
        do 2400 j=1,ncols
        do 2200 i=1,ncols
c 
        ii=ii+1
c
        if(i .gt. j) goto 2200
        jj=ipivots(j)
c
        call p48_scap(a(1,jj),b(1,i),n,prod)
        v(ii)=prod
 2200 continue
 2400 continue
c
        return
        end  
c 
c 
c 
c 
c 
        subroutine p48_reskel_piv2(b,n,m,eps,rnorms,ipivots,ncols)
        implicit real *8 (a-h,o-z)
        save
        dimension rnorms(1),ipivots(1)
        real *8 b(n,m),cd
c 
c        . . . initialize the array of pivots
c 
        do 1100 i=1,m
        ipivots(i)=i
 1100 continue
c 
c       . . . prepare the array of values of norms
c             of columns
c 
        done=1
        dtot=0
        do 1400 i=1,m
c 
        d=0
        do 1200 j=1,n
        d=d+b(j,i)*(b(j,i))
 1200 continue
        rnorms(i)=sqrt(d)
        dtot=dtot+d
 1400 continue
c 
        thresh=dtot*eps**2
        thresh=sqrt(thresh)
c 
c       . . . conduct gram-schmidt iterations
c
        nmmin=m
        if (n .lt. m) nmmin=n
c 
cccc        do 4000 i=1,m
        do 4000 i=1,nmmin
c 
c       find the pivot
c 
        ipivot=i
        rn=rnorms(i)
c 
        do 2200 j=i+1,m
        if(rnorms(j) .le. rn) goto 2200
        rn=rnorms(j)
        ipivot=j
 2200 continue
 2400 continue
c 
c       put the column number ipivot in the i-th place
c 
        do 2600 j=1,n
        cd=b(j,i)
        b(j,i)=b(j,ipivot)
        b(j,ipivot)=cd
 2600 continue
c 
        iijj=ipivots(i)
        ipivots(i)=ipivots(ipivot)
        ipivots(ipivot)=iijj
c 
        d=rnorms(ipivot)
        rnorms(ipivot)=rnorms(i)
        rnorms(i)=d
c 
c       orthogonalize the i-th column to all preceding ones
c 
        if(i .eq. 1) goto 2790
        do 2780 j=1,i-1
c 
        call p48_scap(b(1,i),b(1,j),n,cd)
c 
        do 2770 l=1,n
        b(l,i)=b(l,i)-b(l,j)*cd
 2770 continue
 2780 continue
 2790 continue
c 
c       normalize the i-th column
c 
        call p48_scap(b(1,i),b(1,i),n,cd)
c 
        d=cd
        if(d .lt. thresh**2 ) return
c 
        ncols=i
c 
        d=done/dsqrt(d)
        do 2800 j=1,n
        b(j,i)=b(j,i)*d
 2800 continue
c 
        if(i .eq. m) goto 3400
c 
c        orthogonalize everything else to it
c 
        do 3300 iijjkk=1,2

        do 3200 j=i+1,m
c 
        if(rnorms(j) .lt. thresh) goto 3200
c 
        call p48_scap(b(1,i),b(1,j),n,cd)
c 
        cd=(cd)
c 
        rrn=0
        do 3000 l=1,n
        b(l,j)=b(l,j)-b(l,i)*cd
        rrn=rrn+b(l,j)*(b(l,j))
 3000 continue
        rnorms(j)=dsqrt(rrn)
 3200 continue

 3300 continue
 3400 continue
c 
 4000 continue
c 
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_setzero(x,n)
        implicit real *8 (a-h,o-z)
        save
        real *8 x(1),y(1)
c
        do 1200 i=1,n
c
        x(i)=0
 1200 continue
c
        return
c
c
c
c
        entry p48_signchange(x,y,n)
c
        do 2200 i=1,n
c
        y(i)=-x(i)
 2200 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_matvec(a,n,m,x,y)
        implicit real *8 (a-h,o-z)
        save
        real *8 a(n,m),x(m),y(n),cd
c 
c        apply the matrix a to the vector x obtaining y
c 
        do 1400 i=1,n
        cd=0
        do 1200 j=1,m
        cd=cd+a(i,j)*x(j)
 1200 continue
        y(i)=cd
 1400 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_add(x,y,z,n)
        implicit real *8 (a-h,o-z)
        save
        real *8 z(n),x(n),y(n)
c 
c        add up arrays x,y
c 
        do 1400 i=1,n
        z(i)=x(i)+y(i)
 1400 continue
c
        return
        end
c 
c 
c 
c 
c 
        subroutine p48_transp(a,n,m,b)
        implicit real *8 (a-h,o-z)
        save
        dimension a(n,m),b(m,n)
c
        do 1400 i=1,m
        do 1200 j=1,n
c
        b(i,j)=a(j,i)
 1200 continue
 1400 continue
c
        return
        end
c
c
c
c
c
        subroutine p48_interp(a,m,n,eps,ifinterp,
     1                        rnorms,krank,ipiv,notpiv,proj,work)
c
c       computes the column skeletonization of a,
c       _i.e._, lists in ipivots the indices of krank columns of a
c       such that 
c
c       a(j,ipiv(k))  =  a(j,ipiv(k))
c
c       for all j = 1, ..., m; k = 1, ..., krank, and
c
c                          krank
c       a(j,notpiv(k))  =  Sigma  a(j,ipiv(l)) * proj(l,k)           (*)
c                           l=1
c
c                       +  epsilon(j,k)
c
c       for all j = 1, ..., m; k = 1, ..., n-krank,
c
c       for some matrix epsilon dimensioned epsilon(krank,n-krank)
c       such that the greatest singular value of epsilon
c       <= the greatest singular value of a * eps.
c
c       input:
c       a -- matrix to be skeletonized
c       m -- first dimension of a and the first factor in the dimension
c            required of proj
c       n -- second dimension of a, as well as the dimension required
c            of list, and the second factor in the dimension required
c            of proj
c       eps -- relative precision of the resulting skeletonization
c       ifinterp -- set to 1 to calculate proj;
c                   set to 0 to omit calculating proj
c
c       output:
c       rnorms -- elements on the diagonal of the leftmost uppermost
c                 k x k block of a
c       krank -- numerical rank
c       ipiv -- list of the indices of the krank columns of a
c               through which the other columns of a are expressed
c       notpiv -- describes the permutation of proj
c                 required to reconstruct a as indicated in (*) above
c       proj -- interpolation matrix, dimensioned
c               as proj(krank,n-krank) upon termination
c               of this routine
c               (proj is returned when ifinterp = 1
c                and is not returned when ifinterp = 0)
c       work -- the pivoted QR decomposition,
c               as created by the routine qrhhpiv,
c               of the matrix a
c               (see the routine qrhhpiv
c                for a description of how
c                the QR decomposition is stored)
c
        implicit none
        integer m,n,krank,j,k,ipiv(n),notpiv(n),iswap,ifinterp
c
c       note: a is dimensioned as a(m,n)
c
        real*8 a(m,n),eps,rnorms(n),proj(m,n),work(m,n)
c
c
c       copy a into work
c
        do j = 1,m
          do k = 1,n
            work(j,k) = a(j,k)
          enddo ! k
        enddo ! j
c
c
        call p48_qrhhpiv(eps,m,n,work,krank,ipiv,rnorms)

cc        call prinf('m = *', m, 1)
cc        call prinf('n = *', n, 1)
cc        call prinf('krank = *', krank, 1)
cc        call prinf('ipiv = *', ipiv, krank)


cccc        stop
c
c
c       build the list of columns chosen in a
c       by multiplying together the permutations in list,
c       with the permutation swapping 1 and list(1) taken rightmost
c       in the product, that swapping 2 and list(2) taken next
c       rightmost, ..., that swapping krank and list(krank) taken
c       leftmost
c
        do k = 1,n
          rnorms(k) = k
        enddo ! k
c
        if(krank .gt. 0) then
          do k = 1,krank
c
c           swap rnorms(k) and rnorms(ipiv(k))
c
            iswap = rnorms(k)
            rnorms(k) = rnorms(ipiv(k))
            rnorms(ipiv(k)) = iswap
c
          enddo ! k
        endif
c
        do k = 1,n
          ipiv(k) = rnorms(k)
        enddo ! k
c
        if(krank .lt. n) then
          do k = 1,n-krank
            notpiv(k) = ipiv(k+krank)
          enddo ! k
        endif
c
c
c       fill rnorms with the entries on the diagonal
c       of the leftmost uppermost k x k block of proj(1+m*n:2*m*n)
c
        do k = 1,krank
          rnorms(k) = work(k,k)
        enddo ! k
c
c
        if(ifinterp .eq. 1) then
          if(krank .gt. 0) then
            call lssolve(m,n,work,krank,proj)
          endif
        endif
c
c
        return
        end
c
c
c
c
c
        subroutine lssolve(m,n,a,krank,proj)
c
c       backsolves for proj satisfying R_11 proj ~ R_12,
c       where R_11 = a(1:krank,1:krank)
c       and R_12 = a(1:krank,krank+1:n).
c
c       input:
c       m -- first dimension of a
c       n -- second dimension of a; also,
c            n-krank is the second dimension of proj
c       a -- trapezoidal input matrix
c       krank -- first dimension of proj; also,
c                n-krank is the second dimension of proj
c
c       output:
c       proj -- desired backward-accurate solution
c
        implicit none
        integer m,n,krank,j,k,l
        real*8 a(m,n),proj(krank,n-krank),sum
c
c
        do k = 1,n-krank
          do j = krank,1,-1
c
            sum = 0
c
            if(j .lt. krank) then
              do l = j+1,krank
                sum = sum+a(j,l)*proj(l,k)
              enddo ! l
            endif
c
            proj(j,k) = (a(j,krank+k)-sum) / a(j,j)
c
          enddo ! j
        enddo ! k
c
c
        return
        end
c
c
c
c
c
        subroutine p48_qrhhpiv(eps,m,n,a,krank,ind,ss)
c
c       computes the pivoted QR decomposition
c       of the matrix input into a, using Householder transformations,
c       _i.e._, transforms the matrix a from its input value in
c       to the matrix out with entry
c
c                               m
c       out(j,indprod(k))  =  Sigma  q(l,j) * in(l,k),
c                              l=1
c
c       for all j = 1, ..., krank, and k = 1, ..., n,
c
c       where in = the a from before the routine runs,
c       out = the a from after the routine runs,
c       out(j,k) = 0 when j > k (so that out is triangular),
c       q(1:m,1), ..., q(1:m,krank) are orthonormal,
c       indprod is the product of the permutations given by ind,
c       (as computable via the routine permmult,
c       with the permutation swapping 1 and ind(1) taken leftmost
c       in the product, that swapping 2 and ind(2) taken next leftmost,
c       ..., that swapping krank and ind(krank) taken rightmost),
c       and with the matrix out satisfying
c
c                   krank
c       in(j,k)  =  Sigma  q(j,l) * out(l,indprod(k))  +  epsilon(j,k),
c                    l=1
c
c       for all j = 1, ..., m, and k = 1, ..., n,
c
c       for some matrix epsilon such that
c       the root-sum-square of the entries of epsilon
c       <= the root-sum-square of the entries of in * eps.
c       Well, technically, this routine outputs the Householder vectors
c       (or, rather, their second through last entries)
c       in the part of a that is supposed to get zeroed, that is,
c       in a(j,k) with m >= j > k >= 1.
c
c       input:
c       eps -- relative precision of the resulting QR decomposition
c       m -- first dimension of a and q
c       n -- second dimension of a
c       a -- matrix whose QR decomposition gets computed
c
c       output:
c       a -- triangular (R) factor in the QR decompositon
c            of the matrix input into the same storage locations, 
c            with the Householder vectors stored in the part of a
c            that would otherwise consist entirely of zeroes, that is,
c            in a(j,k) with m >= j > k >= 1
c       krank -- numerical rank
c       ind(k) -- index of the k^th pivot vector;
c                 the following code segment will correctly rearrange
c                 the product b of q and the upper triangle of out
c                 so that b matches the input matrix in
c                 to relative precision eps:
c
c                 copy the non-rearranged product of q and out into b
c                 set k to krank
c                 [start of loop]
c                   swap b(1:m,k) and b(1:m,ind(k))
c                   decrement k by 1
c                 if k > 0, then go to [start of loop]
c
c       work:
c       ss -- must be at least n real*8 words long
c
c       _N.B._: This routine outputs the Householder vectors
c       (or, rather, their second through last entries)
c       in the part of a that is supposed to get zeroed, that is,
c       in a(j,k) with m >= j > k >= 1.
c
        implicit none
        integer n,m,ind(n),krank,k,j,kpiv,mm,nupdate,ifrescal
        real*8 a(m,n),ss(n),eps,feps,ssmax,scal,ssmaxin,rswap
c
c
        feps = .1d-16
c
c
c       compute the sum of squares of the entries in each column of a,
c       the maximum of all such sums, and find the first pivot
c       (column with the greatest such sum)
c
        ssmax = 0
        kpiv = 1
c
        do k = 1,n
c
          ss(k) = 0
          do j = 1,m
            ss(k) = ss(k)+a(j,k)**2
          enddo ! j
c
          if(ss(k) .gt. ssmax) then
            ssmax = ss(k)
            kpiv = k
          endif
c
        enddo ! k
c
        ssmaxin = ssmax
c
        nupdate = 0
c
c
c       while ssmax > eps**2*ssmaxin, krank < m, and krank < n,
c       do the following block of code,
c       which ends at the statement labeled 2000
c
        krank = 0
 1000   continue
c
        if(ssmax .le. eps**2*ssmaxin
     1   .or. krank .ge. m .or. krank .ge. n) goto 2000
        krank = krank+1
c
c
          mm = m-krank+1
c
c
c         perform the pivoting
c
          ind(krank) = kpiv
c
c         swap a(1:m,krank) and a(1:m,kpiv)
c
          do j = 1,m
            rswap = a(j,krank)
            a(j,krank) = a(j,kpiv)
            a(j,kpiv) = rswap
          enddo ! j
c
c         swap ss(krank) and ss(kpiv)
c
          rswap = ss(krank)
          ss(krank) = ss(kpiv)
          ss(kpiv) = rswap
c
c
          if(krank .lt. m) then
c
c
c           compute the data for the Householder transformation
c           which will zero a(krank+1,krank), ..., a(m,krank)
c           when applied to a, replacing a(krank,krank)
c           with the first entry of the result of the application
c           of the Householder matrix to a(krank:m,krank),
c           and storing entries 2 to mm of the Householder vector
c           in a(krank+1,krank), ..., a(m,krank)
c           (which otherwise would get zeroed upon application
c           of the Householder transformation)
c
            call p48_house(mm,a(krank,krank),a(krank,krank),
     1                     a(krank+1,krank),scal)
            ifrescal = 0
c
c
c           apply the Householder transformation
c           to the lower right submatrix of a
c           with upper leftmost entry at position (krank,krank+1)
c
            if(krank .lt. n) then
              do k = krank+1,n
                call p48_houseapp(mm,a(krank+1,krank),a(krank,k),
     1                            ifrescal,scal,a(krank,k))
              enddo ! k
            endif
c
c
c           update the sums-of-squares array ss
c
            do k = krank,n
              ss(k) = ss(k)-a(krank,k)**2
            enddo ! k
c
c
c           find the pivot (column with the greatest sum of squares
c           of its entries)
c
            ssmax = 0
            kpiv = krank+1
c
            if(krank .lt. n) then
c
              do k = krank+1,n
c
                if(ss(k) .gt. ssmax) then
                  ssmax = ss(k)
                  kpiv = k
                endif
c
              enddo ! k
c
            endif ! krank .lt. n
c
c
c           recompute the sums-of-squares and the pivot
c           when ssmax first falls below
c           sqrt((1000*feps)^2) * ssmaxin
c           and when ssmax first falls below
c           ((1000*feps)^2) * ssmaxin
c
            if(
     1       (ssmax .lt. sqrt((1000*feps)**2) * ssmaxin
     2        .and. nupdate .eq. 0) .or.
     3       (ssmax .lt. ((1000*feps)**2) * ssmaxin
     4        .and. nupdate .eq. 1)
     5      ) then
c
              nupdate = nupdate+1
c
              ssmax = 0
              kpiv = krank+1
c
              if(krank .lt. n) then
c
                do k = krank+1,n
c
                  ss(k) = 0
                  do j = krank+1,m
                    ss(k) = ss(k)+a(j,k)**2
                  enddo ! j
c
                  if(ss(k) .gt. ssmax) then
                    ssmax = ss(k)
                    kpiv = k
                  endif
c
                enddo ! k
c
              endif ! krank .lt. n
c
            endif
c
c
          endif ! krank .lt. m
c
c
        goto 1000
 2000   continue
c
c
        return
        end
c
c
c
c
c
        subroutine p48_houseapp(n,vn,u,ifrescal,scal,v)
c
c       applies the Householder matrix
c       identity_matrix - scal * vn * transpose(vn)
c       to the vector u, yielding the vector v;
c
c       scal = 2/(1 + vn(2)^2 + ... + vn(n)^2)
c       when vn(2), ..., vn(n) don't all vanish;
c
c       scal = 0
c       when vn(2), ..., vn(n) do all vanish
c       (including when n = 1).
c
c       input:
c       n -- size of vn, u, and v, though the indexing on vn goes
c            from 2 to n
c       vn -- components 2 to n of the Householder vector vn;
c             vn(1) is assumed to be 1 
c       u -- vector to be transformed
c       ifrescal -- set to 1 to recompute scal from vn(2), ..., vn(n);
c                   set to 0 to use scal as input
c       scal -- see the entry for ifrescal in the decription
c               of the input
c
c       output:
c       scal -- see the entry for ifrescal in the decription
c               of the input
c       v -- result of applying the Householder matrix to u;
c            it's O.K. to have v be the same as u
c            in order to apply the matrix to the vector in place
c
        implicit none
        save
        integer n,k,ifrescal
        real*8 vn(2:*),scal,u(n),v(n),fact,sum
c
c
c       get out of this routine if n = 1
c
        if(n .eq. 1) then
          v(1) = u(1)
          return
        endif
c
c
        if(ifrescal .eq. 1) then
c
c
c         calculate (vn(2))^2 + ... + (vn(n))^2
c
          sum = 0
          do k = 2,n
            sum = sum+vn(k)**2
          enddo ! k
c
c
c         calculate scal
c
          if(sum .eq. 0) scal = 0
          if(sum .ne. 0) scal = 2/(1+sum)
c
c
        endif
c
c
c       calculate fact = scal * transpose(vn) * u
c
        fact = u(1)
c
        do k = 2,n
          fact = fact+vn(k)*u(k)
        enddo ! k
c
        fact = fact*scal
c
c
c       subtract fact*vn from u, yielding v
c      
        v(1) = u(1) - fact
c
        do k = 2,n
          v(k) = u(k) - fact*vn(k)
        enddo ! k
c
c
        return
        end
c
c
c
c
        subroutine p48_house(n,x,rss,vn,scal)
c
c       constructs the vector vn with vn(1) = 1
c       and the scalar scal such that
c       H := identity_matrix - scal * vn * transpose(vn) is orthogonal
c       and Hx = +/- e_1 * the root-sum-square of the entries of x
c       (H is the Householder matrix corresponding to x).
c
c       input:
c       n -- size of x and vn, though the indexing on vn goes
c            from 2 to n
c       x -- vector to reflect into its first component
c
c       output:
c       rss -- first entry of the vector resulting from the application
c              of the Householder matrix to x;
c              its absolute value is the root-sum-square
c              of the entries of x
c       vn -- entries 2 to n of the Householder vector vn;
c             vn(1) is assumed to be 1
c       scal -- scalar multiplying vn * transpose(vn);
c
c               scal = 2/(1 + vn(2)^2 + ... + vn(n)^2)
c               when vn(2), ..., vn(n) don't all vanish;
c
c               scal = 0
c               when vn(2), ..., vn(n) do all vanish
c               (including when n = 1)
c
        implicit none
        save
        integer n,k
        real*8 x(n),rss,sum,v1,scal,vn(2:*),x1
c
c
        x1 = x(1)
c
c
c       get out of this routine if n = 1
c
        if(n .eq. 1) then
          rss = x1
          scal = 0
          return
        endif
c
c
c       calculate (x(2))^2 + ... (x(n))^2
c       and the root-sum-square value of the entries in x
c
c
        sum = 0
        do k = 2,n
          sum = sum+x(k)**2
        enddo ! k
c
c
c       get out of this routine if sum = 0;
c       flag this case as such by setting v(2), ..., v(n) all to 0
c
        if(sum .eq. 0) then
c
          rss = x1
          do k = 2,n
            vn(k) = 0
          enddo ! k
          scal = 0
c
          return
c
        endif
c
c
        rss = x1**2 + sum
        rss = sqrt(rss)
c
c
c       determine the first component v1
c       of the unnormalized Householder vector
c       v = x - rss * (1 0 0 ... 0 0)^T
c
c       if x1 <= 0, then form
c       x1-rss
c       directly, since that expression cannot involve any cancellation
c
        if(x1 .le. 0) v1 = x1-rss
c
c       if x1 > 0, then use the fact that
c       x1-rss = -sum / (x1+rss),
c       in order to avoid potential cancellation
c
        if(x1 .gt. 0) v1 = -sum / (x1+rss)
c
c
c       compute the vector vn and the scalar scal such that vn(1) = 1
c       in the Householder transformation
c       identity_matrix - scal * vn * transpose(vn)
c
        do k = 2,n
          vn(k) = x(k)/v1
        enddo ! k
c
c       scal = 2
c            / ( vn(1)^2 + vn(2)^2 + ... + vn(n)^2 )
c
c            = 2
c            / ( 1 + vn(2)^2 + ... + vn(n)^2 )
c
c            = 2*v(1)^2
c            / ( v(1)^2 + (v(1)*vn(2))^2 + ... + (v(1)*vn(n))^2 )
c
c            = 2*v(1)^2
c            / ( v(1)^2 + (v(2)^2 + ... + v(n)^2) )
c
        scal = 2*v1**2 / (v1**2+sum)
c
c
        return
        end
c
c
c
c
        subroutine housemat(n,vn,scal,h)
c
c       fills h with the Householder matrix
c       identity_matrix - scal * vn * transpose(vn).
c
c       input:
c       n -- size of vn and h, though the indexing of vn goes
c            from 2 to n
c       vn -- entries 2 to n of the vector vn;
c             vn(1) is assumed to be 1
c       scal -- scalar multiplying vn * transpose(vn)
c
c       output:
c       h -- identity_matrix - scal * vn * transpose(vn)
c
        implicit none
        save
        integer n,j,k
        real*8 vn(2:*),h(n,n),scal,factor1,factor2
c
c
c       fill h with the identity matrix
c
        do j = 1,n
          do k = 1,n
c
            if(j .eq. k) h(k,j) = 1
            if(j .ne. k) h(k,j) = 0
c
          enddo ! k
        enddo ! j
c
c
c       subtract from h the matrix scal*vn*transpose(vn)
c
        do j = 1,n
          do k = 1,n
c
            if(j .eq. 1) factor1 = 1
            if(j .ne. 1) factor1 = vn(j)
c
            if(k .eq. 1) factor2 = 1
            if(k .ne. 1) factor2 = vn(k)
c
            h(k,j) = h(k,j) - scal*factor1*factor2
c
          enddo ! k
        enddo ! j
c
c
        return
        end
