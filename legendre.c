#include <stdio.h>
#include <legendre.h>




void leg_weights(int n, double *xs, double *weights)
{
  // this routine constructs the n-point Gaussian quadrature
  // rule using vladimir's fast scheme
  //
  // input:
  //   n - the number of nodes/weights to return
  //
  // ouput:
  //   xs - roots of P_n
  //   weights - corresponding quadrature weights

  int k, i, ifodd;
  double d, d2, done;

  // determine the number of taylor coefficients to use
  
  k = 30.0;
  d = 1.0;
  d2 = d + 1.0e-24;

  // update k if using quad-precision

  if(d2 != d) {
    k = 54;
  }

  // construct an array of initial approximations to the roots

  i = n/2;
  ifodd = n - 2*i;

  done = 1;
        pi=datan(done)*4
        h=pi/(2*n)
        ii=0
        do 1100 i=1,n
c
        if(i .lt. n/2+1) goto 1100
        ii=ii+1
        t=(2*i-1)*h
        ts(ii)=-dcos(t)
1100  CONTINUE
c
c       starting from the center, find roots one after another
c
        pol=1
        der=0
c
        x0=0
        call legepol(x0,n,pol,der)
        x1=ts(1)
c
        n2=(n+1)/2
c
        pol3=pol
        der3=der
        do 2000 kk=1,n2
c
        if( (ifodd .eq. 1) .and. (kk .eq. 1) ) then
            ts(kk)=x0
            whts(kk)=der
            x0=x1
            x1=ts(kk+1)
            pol3=pol
            der3=der
            goto 2000
        endif
c
c        conduct newton
c
        ifstop=0
        do 1400 i=1,10
c
        h=x1-x0
        call legetayl(pol3,der3,x0,h,n,k,pol,der)
c
        x1=x1-pol/der
c
        if(abs(pol) .lt. 1.0d-12) ifstop=ifstop+1
        if(ifstop .eq. 3) goto 1600        
c
 1400 continue
 1600 continue
c
        ts(kk)=x1
        if(itype .gt. 0) whts(kk)=der
c
        x0=x1
        x1=ts(kk+1)
        pol3=pol
        der3=der
 2000 continue
c
c        put the obtained roots in the proper order
c
        do 2200 i=(n+1)/2,1,-1
c
        ts(i+n/2)=ts(i)
 2200 continue
c
        do 2400 i=1,n/2
c
        ts(i)=-ts(n-i+1)
 2400 continue
c
        if(itype .le. 0) return
c
c        put the obtained roots in the proper order
c
        do 2600 i=(n+1)/2,1,-1
c
        whts(i+n/2)=whts(i)
 2600 continue
c
        do 2800 i=1,n/2
c
        whts(i)=whts(n-i+1)
 2800 continue
c
        do 3600 i=1,n
c
        whts(i)=2/(1-ts(i)**2)/whts(i)**2
 3600 continue
c
        return
        end




}
