cccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c       
c     This is the end of the debugging code.
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine rjzeros(nu,nzeros,zs,fs)
       implicit real *8 (a-h,o-z)
       real *8 zs(1),fs(0:1)
       data xj0   /0.2404825557695773d+01/
     1      derj0 /-.5191474972894668d+00/
c
c      this function returns the first nzeros of the bessel function
c      of order nu
c
       done=1
       pi=atan(done)*4 
c
       y=nu
c
c      find first zero 
c
       a0=-y**2
       b0=0
       c0=1
c
       a1=0
       b1=1
       c1=0
c
       a2=0
       b2=0
       c2=1
c
       if (nu .eq. 0) goto 3000
c     
c      start left at x=y
c      use Pruefer to march to first zero
c
       x=y+y**(done/3)
c
       p=a2+b2*x+c2*x**2
       r=a0+b0*x+c0*x**2
c
       eps=1d-18
       call rjbvals(x,eps,fs(1),nvals,nmax)
cccc       call prinf('nvals=*',nvals,1)
c
c      value and derivative of Bessel J of order y
c      derivative from Abramovitz 9.1.27
c
cccc       m=y-1
       m=nu
       val=fs(m)
       der=(fs(m-1)-fs(m+1))/2
c       
       phistart=atan(sqrt(p/r)*der/val)     
       step=pi/2+phistart
c
       n=30 
       call odemarch(x,phistart,n,step,a2,b2,c2,a1,b1,c1,
     1     a0,b0,c0,zero)
c
cccc       call prin2('first zero after odemarch, zero=*',zero,1)
c
c      increase precision with Newton
c
       x=zero
c
       do 1600 ijk=1,100 000 00
c
       call rjbvals(x,eps,fs(1),nvals,nmax)
c
       val=fs(m)
       der=(fs(m-1)-fs(m+1))/2
       h=-val/der
       x=x+h
c
       if (abs(val/der/x) .le. 1.0d-10) goto 1800
 1600  continue
 1800  continue
c
       do 2600 i=1,2
       call rjbvals(x,eps,fs(1),nvals,nmax)       
       val=fs(m)
       der=(fs(m-1)-fs(m+1))/2
       h=-val/der
       x=x+h
 2600  continue
c
       call rjbvals(x,eps,fs(1),nvals,nmax)       
       valstart=fs(m)
       derstart=(fs(m-1)-fs(m+1))/2
c
cccc       call prin2('smallest root=*',x,1)
c
       start=x
c
       goto 3400
c
 3000  continue
c
c      if here, nu=0, use saved values
c
       start=xj0
       valstart=0
       derstart=derj0
c
 3400  continue

       ext=0 
       call specfunzeros(nzeros,start,valstart,derstart,
     1     ext,a2,b2,c2,a1,b1,c1,a0,b0,c0,zs,fs)
c
       return
       end
c
c
c
c
c
c
       subroutine bessel_eval(ier,nu,xs,nx,vals,w,lw)
       implicit real *8 (a-h,o-z)
       real *8 xs(1),vals(1),w(1)
c
c      memory management for bessel_eval0
c
       done=1.0d0
       pi=4*atan(done)
c
       nzeros=(xs(nx)-nu)/pi+100
c
       izeros=1
       lzeros=nzeros+10
c
       iders=izeros+lzeros
       lders=nzeros+10
c
       ifs=iders+lders
       lfs=lw-ifs
c
c      check if enough memory
c
       if (lfs .lt. 1000) then
           ier=32
           return
       endif
c
       call bessel_eval0(ier,nu,xs,nx,vals,w(izeros),w(iders),
     1     w(ifs))
c
       return
       end
c
c
c
       subroutine bessel_eval0(ier,nu,xs,nx,vals,zeros,ders,fs) 
       implicit real *8 (a-h,o-z)
       real *8 xs(1),vals(1),zeros(1),ders(1),fs(0:1)
       data xj0   /0.2404825557695773d+01/
     1      derj0 /-.5191474972894668d+00/
c
c 
c      This subroutine evaluates the Bessel function J_nu  
c      at the points xs. 
c      (The scheme is based on the Taylor exansion of J_nu at 
c       its roots. It is more efficient than rjbvals for evaluating
c       J_nu at a large number of points which are larger than the first 
c       root.)
c
c       needs: cjbvals.f  and gammanew_eval.f
c
c       input
c            nu - order of Bessel function J_nu         
c            xs - vector of x values at which J_nu is to be evaluated
c            nx - size of xs
c
c       output 
c           vals - vector of the values of J_nu at xs
c           ier - error code 
c                  (0 OK, 8 something went wrong)
c
c
       done=1
       pi=atan(done)*4 
c
       y=nu
c   
c      find first zero 
c      coefficients for Bessel functions
c
       a0=-y**2
       b0=0
       c0=1
c
       a1=0
       b1=1
       c1=0
c
       a2=0
       b2=0
       c2=1
c
       Nzeros=(xs(nx)-nu)/pi+10	
c
cccc       if (nu .eq. 0) goto 2000
c
c      start left at x=y
c      use Pruefer to march to first zero
c
       x=y+y**(done/3)
       if (nu .eq. 0) x=2.4d0
c
       p=a2+b2*x+c2*x**2
       r=a0+b0*x+c0*x**2
c
       eps=1d-18
       call rjbvals(x,eps,fs(1),nvals,nmax)
ccc       call prinf('nvals=*',nvals,1)
c
c     value and derivative of Bessel J of order y
c     derivative from Abramovitz 9.1.27
c
cccc       m=y-1
       m=nu
       val=fs(m)
c
       der=(fs(m-1)-fs(m+1))/2
       if (nu .eq. 0) der=-fs(m+1)
c       
       phistart=atan(sqrt(p/r)*der/val)     
       step=pi/2+phistart
c
cccc       call prin2('x=*',x,1)
cccc       call prin2('phistart=*',phistart,1)
cccc       call prin2('der=*',der,1)
cccc       call prin2('val=*',val,1)
c
cccc       stop
c
       n=30 
       call odemarch
     1    (x,phistart,n,step,a2,b2,c2,a1,b1,c1,a0,b0,c0,zero)
c
cccc      call prin2('first zero after odemarch, zero=*',zero,1)
cccc       stop
c
c      Increase precision with Newton
c
       x=zero
c
 500  continue 
       call rjbvals(x,eps,fs(1),nvals,nmax)
       val=fs(m)
       der=(fs(m-1)-fs(m+1))/2
       if (nu .eq. 0) der=-fs(m+1)
       h=-val/der
       x=x+h
       if (abs(val/der/x)>1d-10) goto 500
c
       do 1600 i=1,2
       call rjbvals(x,eps,fs(1),nvals,nmax)       
       val=fs(m)
       der=(fs(m-1)-fs(m+1))/2
       if (nu .eq. 0) der=-fs(m+1)
       h=-val/der
       x=x+h
 1600  continue
c
       call rjbvals(x,eps,fs(1),nvals,nmax)       
       valstart=fs(m)
       derstart=(fs(m-1)-fs(m+1))/2
       if (nu .eq. 0) derstart=-fs(m+1)
c
       start=x
cccc       call prin2('smallest root=*',x,1)
c
       goto 2400
c
 2000  continue
c
c      if here, nu=0, used saved values
c
       start=xj0
c
       eps=1.0d-18
       call rjbvals(xj0,eps,fs(1),nvals,nmax)       
c
cccc       valstart=fs(m)
       valstart=0
       derstart=derj0
c
 2400  continue
c
       ext=0 
       call specfunzeros(Nzeros,start,valstart,derstart,
     1     ext,a2,b2,c2,a1,b1,c1,a0,b0,c0,zeros,ders)
c
cccc       call prin2('zeros=*',zeros,nzeros)
c
       call sf_eval(a0,b0,c0,a1,b1,c1,a2,b2,c2,fs,m,
     1     zeros,nzeros,ders,xs,nx,vals,ier)
c
        return
        end
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
        subroutine sf_eval(a0,b0,c0,a1,b1,c1,a2,b2,c2,fs,m,
     1      zeros,nz,ders,xs,nx,vals,ier)
        implicit real *8 (a-h,o-z)
        real *8 zeros(1),ders(1),xs(1),vals(1),dersexp(1000),
     1      fs(0:1)
c
c     evaluate the special function u by taylor expansion
c     at xs 
c
c     input:
c            a0,b0,c0,a1,b1,c1,a2,b2,c2 - the coefficients in ODE for u
c            zeros - roots of y
c            nz - number of roots
c            ders - u' at the roots
c            xs - x at which u is to be evaluated
c                 (the xs should lie within the smallest and largest root)
c            nx - number of xs
c    output:
c            vals - values of u at xs
c            ier - error code: 0 OK  8 xs out of range, had to use
c                  rjbvals to evaluate it
c
c
        ier=0
        nders=60
c
ccc        if (xs(1).lt.zeros(1)) ier=2
c
        do 3000 ijk=1,nx
c
        if (xs(ijk) .lt. zeros(1)) goto 1200
        if (xs(ijk) .gt. zeros(nz)) goto 1200
        goto 1600
c
 1200   continue
c
        ier=8
c
        eps=1d-18
        x=xs(ijk)
        call rjbvals(x,eps,fs(1),nvals,nmax)
        val=fs(m)
        vals(ijk)=val
        goto 3000
c
 1600   continue
c
c       if here, xs(ijk) is in range, find closest
c
        do 2000 i=2,nz
        if (xs(ijk) .lt. zeros(i-1)) goto 2000
        if (xs(ijk) .gt. zeros(i)) goto 2000
        goto 2200
 2000   continue
 2200   continue
c
        dd=abs(xs(ijk)-zeros(i-1))
        dd2=abs(xs(ijk)-zeros(i))
c
        if (dd .lt. dd2) i=i-1
cccc        if (dd .gt. dd2) i=i-1
c
        x0=zeros(i)
c
        u=0
        der=ders(i)
cccc        scale=abs(xs(ijk)-x0)
        scale=1
c
        call spec_fun_scder(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      x0,u,scale,der,dersexp,nders)
c
        x=xs(ijk)
c       
        u0=0
        call evalu(x,x0,u0,dersexp,scale,nders,uval,uprime)
c
        vals(ijk)=uval
c
 3000   continue
c
        return
        end
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine specfunzeros(Nzeros,start,valstart,derstart,
     1     ext,a2,b2,c2,a1,b1,c1,a0,b0,c0,zeros,polders)
       implicit real *8 (a-h,o-z)
       dimension zeros(1),ders(10000),polders(1)                   
c
c Calculates roots of special functions 
c 
c input: 
c   Nzeros - number of non-neg roots
c   start - extreme value or root <= all non-neg roots
c   ext - 0 if start is root, 1 if start is extrem value 
c   
c    a0-c2: coefficients in ODE for special function u:
c
c  (a2+b2*x+c2*x**2) * u''(x) + (a1+b1*x+c1*x**2) * u'(x)
c   + (a0+b0*x+c0*x**2) * u(x) =0; 
c
c output: 
c   zeros - non-neg roots
c
       one=1
       pi=atan(one)*4 
       z0=0
c
c number of derivatives in taylor expansion:
c
       nders=30
c
c number of steps in ODE from one root to next:
c
       n=10
c
c
c   Setup inital value at Extremum or Zero	
c
       zeros(1)=start
       der=derstart
       polders(1)=der
cccc      call prin2('derstart=*',derstart,1)
c
c
       if (ext .eq. 1) then
c
          der=0
         phib=0
          call odemarch
     1     (zeros(1),phib,n,pi/2,a2,b2,c2,a1,b1,c1,a0,b0,c0,zero)
c
          scale=abs(zeros(1)-zero)
          call spec_fun_scder(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      zeros(1),valstart,scale,der,ders,nders)
c
          call newton(zero,zeros(1),valstart,ders,scale,nders,uprime)
          zeros(1)=zero
          der=uprime
          polders(1)=der
cccc          call prin2('der=*',der,1)
c
       endif
c
       k=1
       phib=pi/2
c
        do 500 i=1,Nzeros-1
c
          call odemarch(zeros(k),phib,n,pi,a2,b2,c2,a1,
     1                 b1,c1,a0,b0,c0,zero)
c
          z0=0
          scale=abs(zeros(k)-zero)
cc          scale=1
c
          call spec_fun_scder(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      zeros(k),z0,scale,der,ders,nders)
c
          call newton(zero,zeros(k),z0,ders,scale,nders,uprime)
c         
          der=uprime          
          k=k+1
          zeros(k)=zero
          polders(k)=der
c
 500  continue
       return
       end
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
       subroutine ODEmarch
     1    (start,phistart,n,step,a2,b2,c2,a1,b1,c1,a0,b0,c0,zero)
c
       implicit real *8 (a-h,o-z)
c   
c
       one=1
       pi=atan(one)*4 
c
cccc       CALL prini(6,13)
c      
C Solve ODE of form dt/dphi=fODE(phi,t) using a second order scheme
C in Interval [start,stop]
c
c 
       h=-step/n
       phib=phistart
       tb=start
c
       do 1000 i=1,n 
c
       phie=phib+h
       CALL fODE(tb,phib,a2,b2,c2,a1,b1,c1,a0,b0,c0,derb)
       ttemp=tb+derb*h
       CALL fODE(ttemp,phie,a2,b2,c2,a1,b1,c1,a0,b0,c0,dere)
       te=tb+(derb+dere)*h/2
c     
       tb=te
       phib=phie
c
 1000 continue 
c
       zero=te
c
       END      
c 
cccccccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine fODE(x,phi,a2,b2,c2,a1,b1,c1,a0,b0,c0,val)
c
c  a0-c2: coefficients in ODE for special function u:
c
c  (a2+b2*x+c2*x**2) * u''(x) + (a1+b1*x+c1*x**2) * u'(x)
c   + (a0+b0*x+c0*x**2) * u(x) =0; 
c
       implicit real *8 (a-h,o-z)
c
       p=a2+b2*x+c2*x**2
       q=a1+b1*x+c1*x**2
       r=a0+b0*x+c0*x**2
c
c
       pprime=b2+2*c2*x
       rprime=b0+2*c0*x
c
       temp1=(pprime*r-rprime*p)/(4*p*r)-q/(2*p)
       val=1/(temp1*sin(2*phi)-sqrt(r/p))
c    
c 
       return
       end
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      subroutine spec_fun_scder(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      x,phi,scale,der,ders,nders)
c
        implicit real *8 (a-h,o-z)
        dimension ders(1)
c
c        This subroutine uses recursion to evaluate the 
c        coefficients of the Taylor series for the user-specified 
c        special function at the point x, given the values of the
c        function and its first derivative. The said special function 
c        is defined as the solution of the e1uation 
c
c        (a2+b2*x+c2*x**2) * phi''(x) + (a1+b1*x+c1*x**2) * phi'(x)
c        + (a0+b0*x+c0*x**2) * phi(x) =0;                               (1)
c
c        with the coefficients a0,b0,c0,a1,b1,c1,a2,b2,c2 specified
c        by the user. The user also specifies the point in R where 
c        the derivatives are to be evaluated, and the values of 
c        phi and phi' at the point x.
c
c                   Input parameters:
c
c  a0,b0,c0,a1,b1,c1,a2,b2,c2 - the coefficients in (1) above
c  x - the point where the derivatives are to be calculated
c  phi - the value of the function at the point x
c  der - the value of the first derivative at the point x
c  nders - the number of derivatives to be returned
c
c        . . . evaluate the third and fourth derivatives
c
        p=a2+b2*x+c2*x**2
        q=a1+b1*x+c1*x**2
        r=a0+b0*x+c0*x**2
c
        coe10=b0 + 2*c0*x
        coe11=a0 + b1 + x*(b0 + 2*c1 + c0*x)
        coe12=a1 + b2 + x*(b1 + 2*c2 + c1*x)
        coe13=a2 + x*(b2 + c2*x)
c
c       don't use ders(0)...
c
cccc        i0=0
cccc        ders(i0)=phi
        ders(1)=der
c
        ders(2)=(-(r*phi+q*der)/p)
c
        if(nders .eq. 2) return
c
        der2=ders(2)
c
        der3=-(coe10*phi+coe11*der+coe12*der2)/coe13
        der3=-(coe10*phi+coe11*der+coe12*der2)/coe13
        ders(3)=der3
c
        if(nders .eq. 3) return
c
        coe20=2*c0
        coe21=2*(b0 + c1 + 2*c0*x)
        coe22=a0 + 2*b1 + 2*c2 + b0*x + 4*c1*x + c0*x**2
        coe23=a1 + 2*b2 + x*(b1 + 4*c2 + c1*x)
        coe24=a2 + x*(b2 + c2*x)
c
        der4=-(coe20*phi+coe21*der+coe22*der2+coe23*der3)/coe24
        ders(4)=der4
c
        if(nders .eq. 4) return
c
        ders(1)=ders(1)*scale
        ders(2)=ders(2)*scale**2
        ders(3)=ders(3)*scale**3
        ders(4)=ders(4)*scale**4
c
c        calculate higher derivatives
c
        do 2600 i=3,nders-2
c
        n=i
c
        dp2=a2+b2*x+c2*x**2
        dp1= n*(b2+2*c2*x) +a1+b1*x+c1*x**2
        d=n*(n-1)*c2+n*(b1+2*c1*x)+a0+b0*x+c0*x**2
        dm1=n*(n-1)*c1+n*(b0+2*c0*x)
        dm2=n*(n-1)*c0
c
        dd=dp1*ders(n+1)*scale+d*ders(n)*scale**2
     1     +dm1*ders(n-1)*scale**3+dm2*ders(n-2)*scale**4
        ders(n+2)=-dd/dp2
 2600 continue
c
        return
        end
c
c
ccccccccccccccccccccccccc
c
      subroutine spec_fun_ser(a0,b0,c0,a1,b1,c1,a2,b2,c2,
     1      x,phi,der,ders,nders)
c
        implicit real *8 (a-h,o-z)
        dimension ders(1)
c
c        This subroutine uses recursion to evaluate the 
c        coefficients of the Taylor series for the user-specified 
c        special function at the point x, given the values of the
c        function and its first derivative. The said special function 
c        is defined as the solution of the equation 
c
c        (a2+b2*x+c2*x**2) * phi''(x) + (a1+b1*x+c1*x**2) * phi'(x)
c        + (a0+b0*x+c0*x**2) * phi(x) =0;                               (1)
c
c        with the coefficients a0,b0,c0,a1,b1,c1,a2,b2,c2 specified
c        by the user. The user also specifies the point in R where 
c        the derivatives are to be evaluated, and the values of 
c        phi and phi' at the point x.
c
c                   Input parameters:
c
c  a0,b0,c0,a1,b1,c1,a2,b2,c2 - the coefficients in (1) above
c  x - the point where the derivatives are to be calculated
c  phi - the value of the function at the point x
c  der - the value of the first derivative at the point x
c  nders - the number of derivatives to be returned
c
c        . . . evaluate the third and fourth derivatives
c
        p=a2+b2*x+c2*x**2
        q=a1+b1*x+c1*x**2
        r=a0+b0*x+c0*x**2
c
        coe10=b0 + 2*c0*x
        coe11=a0 + b1 + x*(b0 + 2*c1 + c0*x)
        coe12=a1 + b2 + x*(b1 + 2*c2 + c1*x)
        coe13=a2 + x*(b2 + c2*x)
c
        i0=0
        ders(i0)=phi
        ders(1)=der
c
        ders(2)=-(r*phi+q*der)/p
c
        if(nders .eq. 2) return
c
        der2=ders(2)
c
        der3=-(coe10*phi+coe11*der+coe12*der2)/coe13
        der3=-(coe10*phi+coe11*der+coe12*der2)/coe13
        ders(3)=der3
c
        if(nders .eq. 3) return
c
        coe20=2*c0
        coe21=2*(b0 + c1 + 2*c0*x)
        coe22=a0 + 2*b1 + 2*c2 + b0*x + 4*c1*x + c0*x**2
        coe23=a1 + 2*b2 + x*(b1 + 4*c2 + c1*x)
        coe24=a2 + x*(b2 + c2*x)
c
        der4=-(coe20*phi+coe21*der+coe22*der2+coe23*der3)/coe24
        ders(4)=der4
c
        if(nders .eq. 4) return
c
c        calculate higher derivatives
c
        do 2600 i=3,nders-2
c
        n=i
c
        dp2=a2+b2*x+c2*x**2
        dp1= n*(b2+2*c2*x) +a1+b1*x+c1*x**2
        d=n*(n-1)*c2+n*(b1+2*c1*x)+a0+b0*x+c0*x**2
        dm1=n*(n-1)*c1+n*(b0+2*c0*x)
        dm2=n*(n-1)*c0
c
        dd=dp1*ders(n+1)+d*ders(n)+dm1*ders(n-1)+dm2*ders(n-2)
        ders(n+2)=-dd/dp2
 2600 continue
c
        return
        end
ccccccccccccccccccccccccccccccccccccccccccc
c
       subroutine evalu(x,x0,u0,ders,scale,nders,uval,uprime)
c       
       implicit real *8 (a-h,o-z)
       dimension ders(1)
c
c This subroutine evaluates a taylor expansion around (x0,u0) at x,  
c given the derivates (ders), number of derivative temrs (nders)
c
c returns: uval - value at x,  uprime - first derivative at x 
c
       h=(x-x0)
       h=h/scale
       uval=u0
       uprime=0
       fac=1 
c
       do 1000 i=1,nders
c
       uprime=uprime+ders(i)*h**(i-1)/(fac*scale)
       fac=fac*i
       uval=uval+ders(i)*h**i/(fac)   
c
 1000 continue        
c
       return
       end  
c
cccccccccccccccccccccccccccccccccccccccccc
       subroutine newton(approx,zero,zeroval,ders,scale,nders,uprime)
c
       implicit real *8 (a-h,o-z)
       dimension ders(1)
       z0=zeroval
       uprime=0
c
 1000 call evalu(approx,zero,z0,ders,scale,nders,uval,uprime)
       h=-uval/uprime
       approx=approx+h
       if (abs(uval/uprime/approx)>1d-10) goto 1000
c
       do 2000 i=1,3      
c
       call evalu(approx,zero,z0,ders,scale,nders,uval,uprime)
       h=-uval/uprime
       approx=approx+h
c
 2000 continue
c
c       
       return
       end
ccccccccccccccccccccccccccccccccccccccccc

