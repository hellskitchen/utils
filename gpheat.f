c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this file contains routines used for creating heat plots,
c       i.e. plotting the intensity of a 2d function in color
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       the following user-callable functions are available:
c
c         gpheat - plots just the heat map
c
c         gpheat2 - plots a heat map, plus one curve (or points)
c
c         gpheat3 - plots a heat map, plus two curves (or two
c             sets of points)
c
c         int2char - turns an integer between 0 and 9 into a
c             character
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc       
c
c
        subroutine gpheat(iw,m,n,target,vals,title)
        implicit real *8 (a-h,o-z)
        real *8 vals(m,n),target(2,m,n)
        character *1 a1,a10,file1(8),file11(4),title(1)
        character *4 file4
        character *8 file8
c
        equivalence (file1,file8),(file11,file4)
c
c       output the data stored in vals to a file which gnuplot can
c       read and plot as a heat map
c
c       this routine requires a very special formatting of the inputs
c       target and vals - target should be a 2 by m by n array containing
c       x,y coordinates on an m (number of y values) by n (number of x
c       values) grid - the values in vals are ordered similarly. this
c       is an artifact of gnuplot.
c
c       input:
c
c         iw - file number, same as in quaplot
c         m - number of y values on the grid
c         n - number of x values on the grid
c         target - x,y locations of grid points
c         vals - 'z' value at the x,y points
c         title - the title of the plot, should be of the form 'title*'
c
c       note that on exit, two files will be created - gniw and gn1000iw
c
c
c       first construct the file names using iw
c
        i1=mod(iw,10)
        i10=(iw-i1)/10
        call int2char(i1,a1)
        call int2char(i10,a10)

        file8='gn1000'
        file1(7)=a10
        file1(8)=a1
c
        file4='gn'
        file11(3)=a10
        file11(4)=a1

c
c       print out the contents of the scripting file, gniw
c
        iun87=87
        open(unit=iun87,file=file4)
        write(iun87,*) '# set terminal postscript'
        write(iun87,*) '# set output "plot.ps"'
        call quamesslen2(title,nchar)
        write(iun87,*) 'set title "',(title(i),i=1,nchar),'"'
        write(iun87,*) 'show title'
cccc        write(iun87,*) 'set xrange [',xmin,':',xmax,']'
cccc        write(iun87,*) 'set yrange [',ymin,':',ymax,']'
        write(iun87,*) 'set view map'
cccc        write(iun87,*) 'splot "',file8,'" matrix with image'
        write(iun87,*) 'splot "',file8,'" using 2:1:3 with image'

c
c       now print out the data file, gn1000iw
c
        iun88=88
        open(unit=iun88,file=file8)
        do 1600 i=1,m
        do 1400 j=1,n
        write(iun88,*) target(2,i,j),' ',target(1,i,j),' ',vals(i,j)
 1400 continue
 1600 continue

c
        return
        end
c
c
c
c
c
        SUBROUTINE quamesslen2(MES,nchar)
        CHARACTER *1 MES(1),AST
        DATA AST/'*'/
C
C         DETERMINE THE LENGTH OF THE MESSAGE
C
        I=0
        DO 1400 I=1,10000
        IF(MES(I).EQ.AST) GOTO 1600
        I1=I
 1400 CONTINUE
 1600 CONTINUE
c
        nchar=i1
c
cccc        do 1800 i=1,nchar
cccc        line(i)=mes(i)
cccc 1800 continue
c
         RETURN
         END
c
c
c
c
c
        subroutine int2char(n,a)
        implicit real *8 (a-h,o-z)
        character *1 a
c
        if (n .eq. 0) a='0'
        if (n .eq. 1) a='1'
        if (n .eq. 2) a='2'
        if (n .eq. 3) a='3'
        if (n .eq. 4) a='4'
        if (n .eq. 5) a='5'
        if (n .eq. 6) a='6'
        if (n .eq. 7) a='7'
        if (n .eq. 8) a='8'
        if (n .eq. 9) a='9'
c
        return
        end
