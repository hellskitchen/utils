c        PROGRAM driver
c        IMPLICIT NONE
c        REAL*8  A(3,3), B(2,2)
c        DATA a/11,21,31,12,22,32,13,23,33/

c        CALL PRINI(6,13)

c        CALL MATPR(A, 3, 3, 'A = *')
c        CALL EXTRACT_MAT(A, 3, 3, 3, 3, B, 2, 2)
c        CALL MATPR(B, 2, 2, 'B = *')

c        STOP
c        END PROGRAM driver

cccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE sumvec(v, m, value)
        IMPLICIT NONE
        INTEGER m, i
        REAL*8 v(m), value

        value = 0.0d0
        DO 1000, i = 1,m
           value = value + v(i)
 1000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE colnorm(A, m, n, j, value)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m,n), value

        value = 0
        DO 1000, i = 1,m
           value = value + A(i,j)**2
 1000   CONTINUE
        value = sqrt(value)

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE scalecol(A, m, n, j, scal)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m,n), scal

        DO 1000, i = 1,m
           A(i,j) = A(i,j)*scal
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE matrixcopy(A, m, n, B)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m,n), B(m,n), value

        value = 0
        DO 1000, j = 1,n
          DO 1200 i = 1,m
            B(i,j) = A(i,j)
 1200     CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE orthogcol(A, m, n, i, j, innerprod)
      IMPLICIT NONE
      INTEGER m, n, i, j, p
      REAL*8 A(m,n), innerprod
c     NOTE: This function assumes that the i'th column
c     of A is normalized

      innerprod = 0
      DO 1000, p = 1,m
        innerprod = innerprod + A(p,i)*A(p,j)
 1000 CONTINUE

      DO 1200, p = 1,m
        A(p,j) = A(p,j) - innerprod*A(p,i)
 1200 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE orthogcolc(Ar, Ai, m, n, i, j, ip_r, ip_i)
      IMPLICIT NONE
      INTEGER m, n, i, j, p
      REAL*8 Ar(m,n), Ai(m,n), ip_r, ip_i
c     NOTE: This function assumes that the i'th column
c     of A is normalized

      ip_r = 0
      ip_i = 0
      DO 1000, p = 1,m
        ip_r = ip_r + Ar(p,i)*Ar(p,j) + Ai(p,i)*Ai(p,j)
        ip_i = ip_i - Ai(p,i)*Ar(p,j) + Ar(p,i)*Ai(p,j)
 1000 CONTINUE

      DO 1200, p = 1,m
        Ar(p,j) = Ar(p,j) - ip_r*Ar(p,i) + ip_i*Ai(p,i)
        Ai(p,j) = Ai(p,j) - ip_i*Ar(p,i) - ip_r*Ai(p,i)
 1200 CONTINUE

      RETURN
      END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE zeromatrix(A, m, n)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8  A(m,n)

        DO 1000, j = 1,n
          DO 1200, i = 1,m
            A(i,j) = 0
 1200     CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE swapcols(A, m, n, i, j)
        IMPLICIT NONE
        INTEGER m, n, i, j, p
        REAL*8  A(m,n), tmp

        DO 1000 p = 1,m
          tmp    = A(p,i)
          A(p,i) = A(p,j)
          A(p,j) = tmp
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE submatrix(A, m, n, i, j, nrows, ncols, B)
        IMPLICIT NONE
        INTEGER m, n, i, j, nrows, ncols, s, t
        REAL*8 A(m,n), B(nrows, ncols)

        DO 1000, t = 1,ncols
           DO 1200, s = 1,nrows
              B(s,t) = A(i-1+s, j-1+t)
 1200      CONTINUE
 1000   CONTINUE
        
        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE extractcols(A, m, n, ind_cols, k, B)
        IMPLICIT NONE
        INTEGER m, n, k, ind_cols(k), i, j, colnr
        REAL*8  A(m,n), B(m,k)

        DO 1000, j = 1,k
           colnr = ind_cols(j)
           DO 1200, i = 1,m
              B(i,j) = A(i,colnr)
 1200      CONTINUE
 1000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE extractrows(A, m, n, ind_rows, k, B)
        IMPLICIT NONE
        INTEGER m, n, k, ind_rows(k), i, j, rownr
        REAL*8  A(m,n), B(k,n)

        DO 1000, i = 1,k
           rownr = ind_rows(i)
           DO 1200, j = 1,n
              B(i,j) = A(rownr,j)
 1200      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE transpose(A, m, n, B)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8  A(m,n), B(n,m)

        DO 1000, j = 1,n
           DO 1200, i = 1,m
              B(j,i) = A(i,j)
 1200      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE norml2(A, m, value)
        IMPLICIT NONE
        INTEGER m, i
        REAL*8  A(m), value

        value = 0
        DO 1000, i = 1,m
           value = value + A(i)*A(i)
 1000   CONTINUE
        value = sqrt(value)

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE insert_block(A, m, n, p, q, i, j, B)
        IMPLICIT NONE
        INTEGER m, n, p, q, i, j, s, t
        REAL*8 A(m*p, n*q), B(n,n)

        DO 1000, t = 1,n
           DO 1200 s = 1,m
c              write(*,*) (i-1)*m+s, (j-1)*n+t
              A((i-1)*m + s, (j-1)*n + t) = B(s,t)
 1200      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE testmat(A, m, n)
      IMPLICIT NONE
      INTEGER m, n, i, j
      REAL*8  A(m,n)

      DO 1000, j = 1,n
        DO 1100, i = 1,m
          A(i,j) = i + 0.1*j
 1100   CONTINUE
 1000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE print_mat(A, m, n)
      IMPLICIT NONE
      INTEGER m, n, i, j
      REAL*8 A(m, n)

 666  format (10(E15.5))
      DO 1000 i = 1, m
        WRITE(*,666) (a(i,j), j=1,n)
 1000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE print_imat(A, m, n)
      IMPLICIT NONE
      INTEGER m, n, i, j, A(m,n)

 666  format (10(i7))
      DO 1000 i = 1, m
        WRITE(*,666) (a(i,j), j=1,n)
 1000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE print_submat(a,leda,i1,i2,j1,j2)
      IMPLICIT NONE
      INTEGER leda, i1, i2, i, j1, j2, j
      REAL*8  a(*)

 111  FORMAT('Printing the submatrix [',i3,',',i3,'] x [',i3,',',i3,']')
      WRITE(*,111) i1,i2,j1,j2
 888  FORMAT(50(e15.5))
      DO 1000, i = i1,i2
        WRITE(*,888) (a((j-1)*leda + i), j = j1,j2)
 1000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE print_vec(u, m)
      IMPLICIT NONE
      INTEGER m, i, remain, istart, iend
      REAL*8 u(m)

 555  FORMAT(a26,i5)
      WRITE(*,555) 'Printing a vector of size', m
 888  FORMAT(a4,i4,a2,i4,a2,5(e15.5))
      remain = m
 1000 IF (remain .GT. 0) THEN
        istart = m - remain + 1
        iend   = m - remain + min(remain,5)
        WRITE(*,888) 'inds',istart,'-',iend, ': ',(u(i), i=istart,iend)
        remain = remain - 5
        GOTO 1000
      END IF

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE print_ivec(u, m)
      IMPLICIT NONE
      INTEGER m, i, remain, istart, iend, u(m)

 555  FORMAT(a26,i5)
      WRITE(*,555) 'Printing a vector of size', m
 888  FORMAT(a4,i4,a2,i4,a2,10(i7))
      remain = m
 1000 IF (remain .GT. 0) THEN
        istart = m - remain + 1
        iend   = m - remain + min(remain,10)
        WRITE(*,888) 'inds',istart,'-',iend, ': ',(u(i), i=istart,iend)
        remain = remain - 10
        GOTO 1000
      END IF

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE print_col(A, m, n, j)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m, n)

        DO 1000, i = 1,m
 10        FORMAT(2x,A,i3,A,i3,A,e20.14)
           WRITE(*,10) 'Element (',i,',',j,') = ',A(i,j)
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE print_row(A, m, n, i)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m, n)

        DO 1000, j = 1,n
 10        FORMAT(2x,A,i3,A,i3,A,e20.14)
           WRITE(*,10) 'Element (',i,',',j,') = ',A(i,j)
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE multiplyrows(A, m, n, L, i1, i2, w)
        IMPLICIT NONE
        INTEGER m, n, i1, i2, i, j, k, q
        REAL*8  A(m,n), L(i2-i1+1, i2-i1+1), w(*)

        k = i2 - i1 + 1
        DO 1000, j = 1,n
           DO 1200, i = 1,k
              w(i) = 0
              DO 1400, q = 1,k
                 w(i) = w(i) + L(i,q)*A(i1+q-1,j)
 1400         CONTINUE
 1200      CONTINUE
           DO 1100, i = 1,k
              A(i1+i-1,j) = w(i)
 1100      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE multiplycols(A, m, n, R, j1, j2, w)
        IMPLICIT NONE
        INTEGER m, n, j1, j2, i, j, k, q
        REAL*8  A(m,n), R(j2-j1+1, j2-j1+1), w(*)

        k = j2 - j1 + 1
        DO 1000, i = 1,m
           DO 1200, j = 1,k
              w(j) = 0
              DO 1400, q = 1,k
                 w(j) = w(j) + A(i,j1+q-1)*R(q,j)
 1400         CONTINUE
 1200      CONTINUE
           DO 1100, j = 1,k
              A(i,j1+j-1) = w(j)
 1100      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c        SUBROUTINE schur(A, f, m, k, AR, fR, work)
c        IMPLICIT NONE
c        INTEGER  m, k, i, j
c        REAL*8   A(m,m), AR(k,k), f(m), fR(k), f1(k), f2(m-k)
c        REAL*8   A11(k,k), A12(k,m-k), A21(m-k,k), A22(m-k,m-k)
c        REAL*8   work(*), cond, ftmp2(m-k), ftmp1(k)
c        REAL*8   Atmp1(m-k,k), Atmp2(k,k)
c
cc       --- Extract A11 and A21       
c        DO 1000, j = 1,k
c           DO 1200, i = 1,k
c              A11(i,j) = A(i,j)
c 1200      CONTINUE
c           DO 1400, i = (k+1),m
c              A21(i-k,j) = A(i,j)
c 1400      CONTINUE
c 1000   CONTINUE
c
cc       --- Extract A12 and A22       
c        DO 2000, j = (k+1),m
c           DO 2200, i = 1,k
c              A12(i,j-k) = A(i,j)
c 2200      CONTINUE
c           DO 2400, i = (k+1),m
c              A22(i-k,j-k) = A(i,j)
c 2400      CONTINUE
c 2000   CONTINUE
c
cc       --- Extract f1 and f2
c        DO 3000, i = 1,k
c           f1(i) = f(i)
c 3000   CONTINUE
c        DO 3200, i = (k+1),m
c           f2(i-k) = f(i)
c 3200   CONTINUE
c
cc       --- Overwrite A22 by inv(A22)
c        CALL orthom(A22, m-k, work, cond)
c
cc       --- Construct fR = f1 - A12*inv(A22)*f2
c        CALL matvecmult(A22, f2, m-k, m-k, ftmp2)
c        CALL matvecmult(A12, ftmp2, k, m-k, ftmp1)
c        CALL vecadd(f1, ftmp1, 1.0d0, -1.0d0, k, fR)
c
cc       --- Construct AR = A11 - A12*inv(A22)*A21
c        CALL matmatmult(A22, A21, m-k, m-k, k, Atmp1)
c        CALL matmatmult(A12, Atmp1, k, m-k, k, Atmp2)
c        CALL matadd(A11, Atmp2, 1.0d0, -1.0d0, k, k, AR)
c
c        RETURN
c        END
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE matvecmult(A, u, m, n, v)
c       Compute v(m) = A(m,n)*u(n)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m,n), u(n), v(m)

        IF (n .GT. 0) THEN
          DO 1000, i = 1,m
            v(i) = A(i,1)*u(1)
 1000     CONTINUE
        END IF

        DO 2000, j = 2,n
           DO 2200, i = 1,m
              v(i) = v(i) + A(i,j)*u(j)
 2200      CONTINUE
 2000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE matmatmult(A, B, m, n, p, C)
c       Compute C(m,p) = A(m,n)*B(n,p)
        IMPLICIT NONE
        INTEGER m, n, p, i, j, q
        REAL*8 A(m,n), B(n,p), C(m,p)

        DO 1000, i = 1,m
           DO 1200, q = 1,p
              C(i,q) = A(i,1)*B(1,q)
 1200      CONTINUE
 1000   CONTINUE

        DO 2000, i = 1,m
           DO 2200, q = 1,p
              DO 2400, j = 2,n
                 C(i,q) = C(i,q) + A(i,j)*B(j,q)
 2400         CONTINUE
 2200      CONTINUE
 2000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE vecadd(u, v, c1, c2, m, w)
c       Compute w(m) = c1*u(m) + c2*v(m)
        IMPLICIT NONE
        INTEGER m, i
        REAL*8  u(m), v(m), w(m), c1, c2

        DO 1000, i = 1,m
           w(i) = c1*u(i) + c2*v(i)
 1000   CONTINUE

        RETURN 
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE vecaddto(u, c, m, v)
c       Compute v(m) <- c*u(m) + v(m)
        IMPLICIT NONE
        INTEGER m, i
        REAL*8  u(m), v(m), c

        DO 1000, i = 1,m
           v(i) = c*u(i) + v(i)
 1000   CONTINUE

        RETURN 
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE matadd(A, B, c1,c2, m, n, C)
c       Compute C(m,n) = c1*A(m,n) + c2*B(m,n)
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8  A(m,n), B(m,n), C(m,n), c1, c2

        DO 1000, j = 1,n       
           DO 1200, i = 1,m
              C(i,j) = c1*A(i,j) + c2*B(i,j)
 1200      CONTINUE
 1000   CONTINUE

        RETURN 
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE EXTRACT_MAT(A, m, n, ib, jb, B, mm, nn)
c       This function extracts a submatrix B(mm,nn) of A(m,n)
c       B(1:mm, 1:nn) = A(ib:(ib+mm-1), jb:(jb+nn-1))
        IMPLICIT NONE
        INTEGER m, n, ib, jb, i, j, mm, nn, ind1, ind2
        REAL*8  A(m,n), B(mm,nn)

        DO 1000, j = 1,nn       
           ind2 = mod(jb + j - 2, n) + 1
           DO 1200, i = 1,mm
              ind1 = mod(ib + i - 2, m) + 1
              B(i,j) = A(ind1,ind2)
 1200      CONTINUE
 1000   CONTINUE

        RETURN 
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE INSERT_MAT(A, m, n, ib, jb, B, mm, nn)
c       This function inserts a submatrix B(mm,nn) into A(m,n)
c       A(ib:(ib+mm-1), jb:(jb+nn-1)) = B(1:mm, 1:nn)
        IMPLICIT NONE
        INTEGER m, n, ib, jb, i, j, mm, nn
        REAL*8  A(m,n), B(mm,nn)

        DO 1000, j = 1,nn       
           DO 1200, i = 1,mm
              A(ib - 1 + i, jb - 1 + j) = B(i,j)
 1200      CONTINUE
 1000   CONTINUE

        RETURN 
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE EXTRACT_VEC(u, m, ib, v, mm)
c       This function extracts a subvector...
        IMPLICIT NONE
        INTEGER m, ib, i, mm
        REAL*8  u(m), v(mm)

        DO 1000, i = 1,mm
           v(i) = u(ib - 1 + i)
 1000   CONTINUE

        RETURN 
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c        SUBROUTINE AXplusBY(A, x, B, y, m, nA, nB, z)
c       Compute z = A*x + B*y
c        IMPLICIT NONE
c        INTEGER m, nA, nB, i, j
c        REAL*8 A(m,nA), B(m, nB), x(nA), y(nB), Ax(m), By(m), z(m)
c
c        DO 1000, i = 1,m
c           Ax(i) = A(i,1)*x(1)
c           By(i) = B(i,1)*y(1)
c 1000   CONTINUE
c
c        DO 2000, j = 2,nA
c           DO 2200, i = 1,m
c              Ax(i) = Ax(i) + A(i,j)*x(j)
c 2200      CONTINUE
c 2000   CONTINUE
c
c        DO 3000, j = 2,nB
c           DO 3200, i = 1,m
c              By(i) = By(i) + B(i,j)*y(j)
c 3200      CONTINUE
c 3000   CONTINUE
c
c        DO 4000, i = 1,m
c           z(i) = Ax(i) + By(i)
c 4000   CONTINUE
c
c        RETURN
c        END
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE eye(A, m, c)
c       Create a diagonal matrix A of size m x m
        IMPLICIT NONE
        INTEGER m, i, j
        REAL*8 A(m,m), c

        DO 1000, j = 1,m
           DO 1200, i = 1,m
              A(i,j) = 0.0d0
 1200      CONTINUE
           A(j,j) = c
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE zero(A, m)
c       Create a zero vector
        IMPLICIT NONE
        INTEGER m, i
        REAL*8 A(m)

        DO 1000, i = 1,m
           A(i) = 0.0d0
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE VECCOPY(u, m, v)
c       Copy v = u
        IMPLICIT NONE
        INTEGER m, i
        REAL*8 u(m), v(m)

        DO 1000, i = 1,m
           v(i) = u(i)
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE VECiCOPY(u, m, v)
c       Copy v = u for integer vectors
        IMPLICIT NONE
        INTEGER m, i, u(m), v(m)

        DO 1000, i = 1,m
           v(i) = u(i)
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE MATCOPY(A, m, n, B)
c       Copy B = A
        IMPLICIT NONE
        INTEGER m, n, i, j
        REAL*8 A(m,n), B(m,n)

        DO 1000, j = 1,n
           DO 1200, i = 1,m
              B(i,j) = A(i,j)
 1200      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE FINDMAX(A, m, amax)
        IMPLICIT NONE
        INTEGER i, m, imax
        REAL*8  A(m), tmp, amax

        imax = 0
        amax = 0.0d0
        DO 1000, i = 1,m
           tmp = abs(A(i))
           IF (tmp .GT. amax) THEN
              amax = tmp
              imax = i
           END IF
 1000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE mergeblockstr(A, B, mA, mB, n)
c
c       Overwrite A by 
c
c       A <- [ A   ]
c            [ B^t ]
c
c       Input:  A(mA,n), B(n,mB)
c       Output: A(mA+mB,n)
c
        IMPLICIT NONE
        INTEGER mA, mB, n, i, j, jinv, iinv
        REAL*8  A(*), B(*)

        DO 1000, jinv = 1,n
          j = n - jinv + 1
          DO 1200, iinv = 1,mA
            i = mA - iinv + 1
            A((j-1)*(mA+mB) + i) = A((j-1)*mA + i)
 1200     CONTINUE
 1000   CONTINUE

        DO 2000, j = 1,n
          DO 2200, i = 1,mB
            A((j-1)*(mA+mB) + mA + i) = B((i-1)*n + j)
 2200     CONTINUE
 2000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE mergeblocks(A, B, mA, mB, n)
c
c       Overwrite A by 
c
c       A <- [ A ]
c            [ B ]
c
c       Input:  A(mA,n), B(mB,n)
c       Output: A(mA+mB,n)
c
        IMPLICIT NONE
        INTEGER mA, mB, n, i, j, jinv, iinv
        REAL*8  A(*), B(*)

        DO 1000, jinv = 1,n
          j = n - jinv + 1
          DO 1200, iinv = 1,mA
            i = mA - iinv + 1
            A((j-1)*(mA+mB) + i) = A((j-1)*mA + i)
 1200     CONTINUE
 1000   CONTINUE

        DO 2000, j = 1,n
          DO 2200, i = 1,mB
            A((j-1)*(mA+mB) + mA + i) = B((j-1)*mB + i)
 2200     CONTINUE
 2000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c        SUBROUTINE mergeblockstr(A, B, mA, mB, n, AB)
c
c       A(mA+mB,n) <- [ A   ]
c                     [ B^t ]
c
c        IMPLICIT NONE
c        INTEGER mA, mB, n, i, j
c        REAL*8  A(mA,n), B(mB,n), AB(mA+mB,n)

c        DO 1000, j = 1,n
c           DO 1200, i = 1,mA
c              AB(i,j)    = A(i,j)
c 1200      CONTINUE
c           DO 1400, i = 1,mB
c              AB(mA+i,j) = B(i,j)
c 1400      CONTINUE
c 1000   CONTINUE

c        RETURN
c        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE mergeblocks2(A, B, mA, mB, n, AB)
c
c       Merge the block matrices A(mA,n) and B(mB,n) to form
c
c       AB(mA+mB,n) = [ A(mA,n) ]
c                     [ B(mB,n) ]
c
        IMPLICIT NONE
        INTEGER mA, mB, n, i, j
        REAL*8  A(mA,n), B(mB,n), AB(mA+mB,n)

        DO 1000, j = 1,n
           DO 1200, i = 1,mA
              AB(i,j)    = A(i,j)
 1200      CONTINUE
           DO 1400, i = 1,mB
              AB(mA+i,j) = B(i,j)
 1400      CONTINUE
 1000   CONTINUE

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE mergeblockstr2(A, B, mA, mB, n, AB)
        IMPLICIT NONE
        INTEGER mA, mB, n, i, j, tra, trb
        REAL*8  A(n,mA), B(n,mB), AB(mA+mB,n)

        DO 1000, j = 1,n
           DO 1200, i = 1,mA
              AB(i,j)    = A(j,i)
 1200      CONTINUE
           DO 1400, i = 1,mB
              AB(mA+i,j) = B(j,i)
 1400      CONTINUE
 1000   CONTINUE

        RETURN
        END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc

        SUBROUTINE l2_norm(a, n, val)
        IMPLICIT NONE
        INTEGER n, i
        REAL*8  val, a(n)

        val = 0.0d0
        DO 1000, i = 1,n
           val = val + a(i)*a(i)
 1000   CONTINUE
c        val = val/n
        val = sqrt(val)

        RETURN
        END

cccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE permute_cols(A, m, n, ind, Atmp)
      IMPLICIT NONE
      REAL*8  A(m,n), Atmp(m,n)
      INTEGER m, n, i, j, ind(n), wlen

      DO 1000, j = 1,n
        DO 1100, i = 1,m
          Atmp(i,j) = A(i,ind(j))
 1100   CONTINUE
 1000 CONTINUE

      CALL matcopy(Atmp, m, n, A)
      
      RETURN
      END

cccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE permute_rows(A, m, n, ind, Atmp)
      IMPLICIT NONE
      REAL*8  A(m,n), Atmp(m,n)
      INTEGER m, n, i, j, ind(n), wlen

      DO 1000, j = 1,n
        DO 1100, i = 1,m
          Atmp(i,j) = A(ind(i),j)
 1100   CONTINUE
 1000 CONTINUE

      CALL matcopy(Atmp, m, n, A)
      
      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c This function partitions a matrix A as follows:
c
c   A(m,n) = [ A11(ms,  ns)   A12(ms,  n-ns) ]
c            [ A21(m-ms,ns)   A22(m-ms,n-ns) ]
c
      SUBROUTINE split_in_four(A, m, n, ms, ns, A11, A12, A21, A22)
      IMPLICIT NONE
      REAL*8  A(m,n),A11(ms,ns),A12(ms,n-ns),A21(m-ms,ns),A22(m-ms,n-ns)
      INTEGER m,n,ms,ns,i,j

      DO 1000, j = 1,ns
        DO 1100, i = 1,ms
          A11(i,j) = A(i,j)
 1100   CONTINUE
        DO 1200, i = 1,(m-ms)
          A21(i,j) = A(ms+i,j)
 1200   CONTINUE
 1000 CONTINUE

      DO 2000, j = 1,n-ns
        DO 2100, i = 1,ms
          A12(i,j) = A(i,ns+j)
 2100   CONTINUE
        DO 2200, i = 1,(m-ms)
          A22(i,j) = A(ms+i,ns+j)
 2200   CONTINUE
 2000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c This function partitions a matrix A as follows:
c
c   A(m,n) = [ A1(m,ns)   A2(m,n-ns) ]
c
      SUBROUTINE split_in_two_ver(A, m, n, ns, A1, A2)
      IMPLICIT NONE
      REAL*8  A(m,n),A1(m,ns),A2(m,n-ns)
      INTEGER m,n,ns,i,j

      DO 1000, j = 1,ns
        DO 1100, i = 1,m
          A1(i,j) = A(i,j)
 1100   CONTINUE
 1000 CONTINUE

      DO 2000, j = 1,n-ns
        DO 2100, i = 1,m
          A2(i,j) = A(i,ns+j)
 2100   CONTINUE
 2000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c This function partitions a matrix A as follows:
c
c   A(m,n) = [ A1(ms,  n) ]
c            [ A2(m-ms,n) ]
c
      SUBROUTINE split_in_two_hor(A, m, n, ms, A1, A2)
      IMPLICIT NONE
      REAL*8  A(m,n),A1(ms,n),A2(m-ms,n)
      INTEGER m,n,ms,i,j

      DO 1000, j = 1,n
        DO 1100, i = 1,ms
          A1(i,j) = A(i,j)
 1100   CONTINUE
        DO 1200, i = 1,(m-ms)
          A2(i,j) = A(ms+i,j)
 1200   CONTINUE
 1000 CONTINUE

      RETURN
      END

cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c This function forms the union of four blocks as follows:
c   A(m,n) = [ A11(ms,  ns)   A12(ms,  n-ns) ]
c            [ A21(m-ms,ns)   A22(m-ms,n-ns) ]
c
      SUBROUTINE merge_four(A11,A12,A21,A22,n,m,ns,ms,A)
      IMPLICIT NONE
      REAL*8  A(m,n),A11(ms,ns),A12(ms,n-ns),A21(m-ms,ns),A22(m-ms,n-ns)
      INTEGER m,n,ms,ns,i,j

      DO 1000, j = 1,ns
        DO 1100, i = 1,ms
          A(i,j) = A11(i,j)
 1100   CONTINUE
        DO 1200, i = 1,(m-ms)
          A(ms+i,j) = A21(i,j)
 1200   CONTINUE
 1000 CONTINUE

      DO 2000, j = 1,n-ns
        DO 2100, i = 1,ms
          A(i,ns+j) = A12(i,j)
 2100   CONTINUE
        DO 2200, i = 1,(m-ms)
          A(ms+i,ns+j) = A22(i,j)
 2200   CONTINUE
 2000 CONTINUE

      RETURN
      END


cccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c Small debugging tool to compare two vectors
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE compare(u,v,n)
      IMPLICIT NONE
      INTEGER n,i
      REAL*8  u(*),v(*),diff

      diff = 0.0d0
      DO 1000, i = 1,n
         diff = diff + abs(u(i)-v(i))
 1000 CONTINUE
      WRITE(*,*) 'The difference = ',diff

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE comparei(u,v,n)
      IMPLICIT NONE
      INTEGER n,u(*),v(*),diff,i

      diff = 0
      DO 1000, i = 1,n
         diff = diff + abs(u(i)-v(i))
 1000 CONTINUE
      WRITE(*,*) 'The difference = ',diff

      RETURN
      END

cccccccccccccccccccccccccccccccccccccccccccccccccccccc
