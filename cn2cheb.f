c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this is the end of the debugging code and the beginning
c       of the 2d nested chebyshev interpolation code
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c       This file contains 4 user callable routines to be used
c       for 2d adaptive chebyshev interpolation:
c
c       rcn2all - creates a nested 2d tensor product chebyshev
c                 expansion for a user defined real function
c
c       ccn2all - creates a nested 2d tensor product chebyshev
c                 expansion for a user defined complex function
c
c       rcn2eva - using a previously created expansion by rcn2all,
c                 evalutes said expansion at an arbitrary point
c
c       ccn2eva - using a previously created expansion by ccn2all,
c                 evalutes said expansion at an arbitrary point
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine rcn2eva(x,y,w,val,derx,dery)
        implicit real *8 (a-h,o-z)
        real *8 w(1)
c
c       using the nested 2d chebyshev data in w, evaluate the
c       approximated real function at the point x,y.  further
c       management and actual evaluation is done in rcn2eva0
c
c       input:
c           x,y - the point in 2d where we are to evaluate at
c           w   - the array construct by a previous call to rcn2all
c
c       output:
c           val - the function evaluate at x,y
c           derx,dery - the x and y partial derivatives of the
c                 function evaluated at x,y
c
c       first unpack w
c
        k=w(1)
        a=w(2)
        b=w(3)
        c=w(4)
        d=w(5)
c
        eps=w(6)
        numint=w(7)
c
        iab2=w(11)
        lab2=w(12)
        iwww2=w(13)
        keepwww=w(14)
        iw2=w(15)
        lw2=w(16)
        keepw7=w(17)
c
c       find which interval the point is in
c
        call cn2findint(ier,x,y,w(iab2),numint,intnum,loc)
c
c       now evaluate at x,y using the coefs at loc
c
        loc2=iw2+loc-1
        call rcn2eva0(x,y,w(loc2),w(iwww2),val,derx,dery)
c
        return
        end
c
c
c
c
c
        subroutine ccn2eva(x,y,w,val)
        implicit real *8 (a-h,o-z)
        real *8 w(1)
        complex *16 val,derx,dery
c
c       using the nested 2d chebyshev data in w, evaluate the
c       approximated complex function at the point x,y.  further
c       management and actual evaluation is done in ccn2eva0
c
c       input:
c           x,y - the point in 2d where we are to evaluate at
c           w   - the array construct by a previous call to ccn2all
c
c       output:
c           val - the function evaluate at x,y
c           derx,dery - the x and y partial derivatives of the
c                 function evaluated at x,y
c
c       first unpack w
c
        k=w(1)
        a=w(2)
        b=w(3)
        c=w(4)
        d=w(5)
c
        eps=w(6)
        numint=w(7)
c
        iab2=w(11)
        lab2=w(12)
        iwww2=w(13)
        keepwww=w(14)
        iw2=w(15)
        lw2=w(16)
        keepw7=w(17)
c
c       find which interval the point is in
c
        call cn2findint(ier,x,y,w(iab2),numint,intnum,loc)
c
c       now evaluate at x,y using the coefs at loc
c
        loc2=iw2+loc-1
        call ccn2eva0(x,y,w(loc2),w(iwww2),val,derx,dery)
c
        return
        end
c
c
c
c
c
        subroutine rcn2eva0(x,y,w,www,val,derx,dery)
        implicit real *8 (a-h,o-z)
        real *8 w(1),www(1)
c
c       using the information stored in w, evaluate the real expansion
c       at the point x,y - note that x,y had better be in the region
c       [a,b] x [c,d] or else you'll get garbage!
c
c       input:
c           x,y - the point at which to evaluate the function
c           w   - an array constructed by a previous call to rcn2one
c           www - an array constructed by a previous call to
c                 ch2deva1 with a=-1, b=-1, c=2, and k=k
c
c       output:
c           val - the value of the function at x,y
c           derx,dery - the x and y partial derivatives of the function
c
c       unpack and proceed accordingly
c
        a1=-1
        b1=-1
        c1=2
c
        icoefs=10
        k=w(1)
        a=w(2)
        b=w(3)
        c=w(4)
        d=w(5)
c
        x1=(x-a)/(b-a)*c1+a1
        y1=(y-c)/(d-c)*c1+b1
        call ch2deva1(w(icoefs),k,x1,y1,val,derx,dery,www)
c
        return
        end
c
c
c
c
c
        subroutine ccn2eva0(x,y,w,www,val,derx,dery)
        implicit real *8 (a-h,o-z)
        real *8 w(1),www(1)
        complex *16 val,derx,dery
c
c       using the information stored in w, evaluate the complex expansion
c       at the point x,y - note that x,y had better be in the region
c       [a,b] x [c,d] or else you'll get garbage!
c
c       input:
c           x,y - the point at which to evaluate the function
c           w   - an array constructed by a previous call to ccn2one
c           www - an array constructed by a previous call to
c                 ch2deva1 with a=-1, b=-1, c=2, and k=k
c
c       output:
c           val - the value of the function at x,y
c           derx,dery - the x and y partial derivatives of the function
c
c       unpack and proceed accordingly
c
        a1=-1
        b1=-1
        c1=2
c
        icoefs=10
        k=w(1)
        a=w(2)
        b=w(3)
        c=w(4)
        d=w(5)
c
        x1=(x-a)/(b-a)*c1+a1
        y1=(y-c)/(d-c)*c1+b1
        call cch2deva1(w(icoefs),k,x1,y1,val,derx,dery,www)
c
        return
        end
c
c
c
c
c
        subroutine cn2findint(ier,x,y,abcds,nn,intnum,loc)
        implicit real *8 (a-h,o-z)
        real *8 w(1),abcds(6,1)
        save
        data intold/-10/,ithresh/10/,indexold/-10/
c
c       find the 2d interval that contains the point x,y, and
c       return it in intnum
c
c       input:
c           x,y - the point at which to find a containing interval
c           abcds - a (6,nn) array containing all the endpoints of
c                 the intervals and their locations in the 'master'
c                 array (see rcn2all0 and ccn2all0)
c           nn  - the second dimension of abcds
c
c       output:
c           intnum - the interval number which contains x,y
c           loc - the location in the 'master' array of the
c                 interval information
c
c       check to see if in the last interval, then use direct 
c       scan for now...
c
        if (intold .lt. 0) goto 1600
c
        a=abcds(1,indexold)
        b=abcds(2,indexold)
        c=abcds(3,indexold)
        d=abcds(4,indexold)
c
        if ((a .le. x) .and. (x .le. b) .and. (c .le. y) 
     1     .and. (y .le. d)) then
            intnum=abcds(6,i)
            loc=abcds(5,i)
            goto 2200
        endif
c
 1600   continue
c
c       not in the previous interval, direct search now
c
        ier=0
        intnum=-7
        do 2000 i=1,nn
c
        a=abcds(1,i)
        b=abcds(2,i)
        c=abcds(3,i)
        d=abcds(4,i)
c
        if ((a .le. x) .and. (x .le. b) .and. (c .le. y) 
     1     .and. (y .le. d)) then
            intnum=abcds(6,i)
            loc=abcds(5,i)
            indexold=i
            goto 2200
        endif
c
 2000   continue
 2200   continue
c
        if (intnum .lt. 0) ier=16
c
        return
        end
c
c
c
c
c
        subroutine rcn2all(ier,k,eps,a,b,c,d,rfunc,par1,par2,
     1       par3,par4,numint,w,lw,keepw)
        implicit real *8 (a-h,o-z)
        real *8 w(1),par1(1),par2(1),par3(1),par4(1)
        external rfunc
c
c       construct all coefficient expansions and intervals for
c       rfunc on the rectangle [a,b] x [c,d].  this is a memory management
c       routine, the magic happens in rcn2all0
c
c       input:
c           k   - the order of the expansion to be used on each
c                 rectangle
c           eps - the precision to which the expansion should be accurate
c                 on each rectangle
c           a,b,c,d - the coordiates of the rectangle [a,b] x [c,d]
c           rfunc - user define real subroutine whose calling sequence must
c                 be rfunc(x,y,par1,par2,par3,par4,val)
c           w   - large work array
c           lw  - the length of w in real *8
c
c       output:
c           ier - ier 0 means everything is ok, anything else means there
c                 wasn't enough room to store all the intervals
c           w   - array that must be used in calls to rcn2eva to evaluate
c                 rfunc's expansions
c           keepw - the first keepw real *8 elements of w must not be
c                 changed
c
c
        ier=0
        maxints=2048*16
c
        w(1)=k
        w(2)=a
        w(3)=b
        w(4)=c
        w(5)=d
        w(6)=eps
c
        done=1.0d0
        ntot=0
        mm=maxints
c
        do 1200 i=1,10000
        ntot=ntot+mm
        mm=mm/4
        if (mm .eq. 0) goto 1400
 1200   continue
 1400   continue
c
        ntot=ntot+100
c
        iab=50
        lab=6*ntot+50
c
        iz=iab+lab
        lz=2*(2*k)**2+50
c
        icoefs=iz+lz
        lcoefs=(2*k)**2+50
c
        iff=icoefs+lcoefs
        lff=(2*k)**2+50
c
        iwww=iff+lff
        lwww=4*2*k**2+4*6*k+100+1000
c
        iw7=iwww+lwww
        lw7=10+2*2*k*k+10+50
c
        iw=iw7+lw7
        if (iw .gt. lw) then
          ier=1024
          call prinf('w is wwwaaaaayyyy too small!',iff,0)
          return
        endif
c
        call rcn2all0(ier,k,eps,a,b,c,d,rfunc,par1,par2,par3,
     1       par4,numint,maxints,w(iab),w(iz),w(icoefs),w(iff),
     2       w(iwww),w(iw7),w(iw),keepw)
c
        w(7)=numint
c
c       now rearrange everything inside of w
c
        iab2=50
        lab2=6*numint+20
        call cn2copy(lab,w(iab),w(iab2))
c
c       store a copy from ch2dinit so it doesn't need to be called
c       in the evaluation routines
c
        a1=-1
        b1=-1
        c1=2
        ifinit=1
        call ch2dinit(a1,b1,c1,k,ifinit,w(iz),w(iwww))
c
        iwww2=iab2+lab2
        keepwww=2*k**2+6*k+100+1000
        call cn2copy(keepwww,w(iwww),w(iwww2))
c
        iw2=iwww2+keepwww
        lw2=keepw
        call cn2copy(lw2,w(iw),w(iw2))
c
        w(11)=iab2
        w(12)=lab2
        w(13)=iwww2
        w(14)=keepwww
        w(15)=iw2
        w(16)=lw2
c
c       save the step size in w to pull out individual grids
c
        keepw7=k*k+20
        w(17)=keepw7
c
        keepw2=iw2+lw2
        keepw=keepw2
        if (keepw .gt. lw) then
          ier=256
          call prinf('w is too small, but only at the end*',iff,0)
          return
        endif
c
        return
        end
c
c
c
c
c
        subroutine rcn2all0(ier,k,eps,a,b,c,d,rfunc,par1,par2,
     1       par3,par4,numint,maxints,abcds,z,coefs,f,
     2       www,w7,w,keepw)
        implicit real *8 (a-h,o-z)
        real *8 w(1),www(1),z(2,2*k,2*k),abcds(6,1),w7(1)
        real *8 f(2*k,2*k),coefs(2*k,2*k),par1(1),par2(1),par3(1),
     1       par4(1)
        external rfunc
c
        keepw=0
c
c       initialize the expansion 2*k-point routine on [-1,1] x [-1,1]
c
        a1=-1
        b1=-1
        c1=2
        ifinit=1
        call ch2dinit(a1,b1,c1,2*k,ifinit,z,www)
c
c       now recursively divide [a,b] x [c,d] to find intervals where
c       a k-point expansion is good enough
c
        numint=1
        abcds(1,numint)=a
        abcds(2,numint)=b
        abcds(3,numint)=c
        abcds(4,numint)=d
        abcds(5,numint)=-7
        abcds(6,numint)=-7
c
        keepint=0
        do 3000 ijk=1,1000 000
c
        ifdone=1
        nnn=numint
        do 2400 i=1,nnn
c
c       if the ith interval has not been processed, process it and
c       mark it processed
c
        if (abcds(5,i) .gt. 0) goto 2400
c
        abcds(5,i)=1
        a2=abcds(1,i)
        b2=abcds(2,i)
        c2=abcds(3,i)
        d2=abcds(4,i)
c        
        call rcn2one(ier2,eps,a2,b2,c2,d2,k,rfunc,par1,par2,par3,par4,
     1       f,coefs,z,www,w7,keepw7)
cccc        call prinf('after ccn2one, ier2=*',ier2,1)
c
c       if the discritization is good, mark it and store it, along with
c       the interval number and location of coefs in w -
c       if it's bad, subdivide it into 4 pieces
c
        if (ier2 .eq. 0) then
            keepint=keepint+1
            abcds(6,i)=keepint+.1
c
            ikeepint=(keepint-1)*keepw7+1
            call cn2copy(keepw7,w7,w(ikeepint))
            keepw=keepw+keepw7
            abcds(5,i)=ikeepint+.1
c
            go to 2400
        endif
c
        if (ier2 .ne.0) then
c
            ifdone=0
            if (numint+4 .gt. maxints) then
                ier=64
                call prinf('maxints too small!!, ier=*',ier,1)
                return
            endif
c
            numint=numint+1
            abcds(1,numint)=a2
            abcds(2,numint)=(a2+b2)/2
            abcds(3,numint)=c2
            abcds(4,numint)=(c2+d2)/2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
            numint=numint+1
            abcds(1,numint)=a2
            abcds(2,numint)=(a2+b2)/2
            abcds(3,numint)=(c2+d2)/2
            abcds(4,numint)=d2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
            numint=numint+1
            abcds(1,numint)=(a2+b2)/2
            abcds(2,numint)=b2
            abcds(3,numint)=c2
            abcds(4,numint)=(c2+d2)/2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
            numint=numint+1
            abcds(1,numint)=(a2+b2)/2
            abcds(2,numint)=b2
            abcds(3,numint)=(c2+d2)/2
            abcds(4,numint)=d2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
 2000   continue
        endif
c
 2400   continue
c
cccc        call prin2('abcds=*',abcds,6*numint)
        if (ifdone .eq. 1) goto 3200
c
 3000   continue
 3200   continue
c
        if (ifdone .eq. 0) then
          call prinf('ifdone is 0!!! bomb!!!*',a,0)
          ier=1024
          return
        endif
c
c       sort the boxes and throw away ones that had to be
c       subdivided
c
        iii=0
        do 4000 i=1,numint
c
        if (abcds(6,i) .gt. 0) then
            iii=iii+1
            abcds(1,iii)=abcds(1,i)
            abcds(2,iii)=abcds(2,i)
            abcds(3,iii)=abcds(3,i)
            abcds(4,iii)=abcds(4,i)
            abcds(5,iii)=abcds(5,i)
            abcds(6,iii)=abcds(6,i)
        endif
c        
 4000   continue
c
        numint=iii
c
c       now sort the intervals according to the first coordinate,
c       leave the y intervals unsorted for now
c
        call cn2bubble(abcds,numint)
cccc        call prin2('after sorting, abcds=*',abcds,6*numint)
cccc        call cn2sorty(abcds,numint)        
c
        return
        end
c
c
c
c
c
        subroutine ccn2all(ier,k,eps,a,b,c,d,cfunc,par1,par2,
     1       par3,par4,numint,w,lw,keepw)
        implicit real *8 (a-h,o-z)
        real *8 w(1)
        complex *16 par1(1),par2(1),par3(1),par4(1)
        external cfunc
c
c       construct all coefficient expansions and intervals for
c       cfunc on the rectangle [a,b] x [c,d].  this is a memory management
c       routine, the magic happens in ccn2all0
c
c       input:
c           k   - the order of the expansion to be used on each
c                 rectangle
c           eps - the precision to which the expansion should be accurate
c                 on each rectangle
c           a,b,c,d - the coordiates of the rectangle [a,b] x [c,d]
c           rfunc - user define complex subroutine whose calling sequence must
c                 be cfunc(x,y,par1,par2,par3,par4,val)
c           w   - large work array
c           lw  - the length of w in real *8
c
c       output:
c           ier - ier 0 means everything is ok, anything else means there
c                 wasn't enough room to store all the intervals
c           w   - array that must be used in calls to ccn2eva to evaluate
c                 cfunc's expansions
c           keepw - the first keepw real *8 elements of w must not be
c                 changed
c
c
        maxints=2048*16
c
        w(1)=k
        w(2)=a
        w(3)=b
        w(4)=c
        w(5)=d
        w(6)=eps
c
        done=1.0d0
        ntot=0
        mm=maxints
c
        do 1200 i=1,10000
        ntot=ntot+mm
        mm=mm/4
        if (mm .eq. 0) goto 1400
 1200   continue
 1400   continue
c
        ntot=ntot+100
c
        iab=50
        lab=6*ntot+50
c
        iz=iab+lab
        lz=2*(2*k)**2+50
c
        icoefs=iz+lz
        lcoefs=2*(2*k)**2+50
c
        iff=icoefs+lcoefs
        lff=2*(2*k)**2+50
c
        iwww=iff+lff
        lwww=4*2*k**2+4*6*k+100+1000
c
        iw7=iwww+lwww
        lw7=10+2*2*k*k+10+50
c
        iw=iw7+lw7
        if (iw .gt. lw) then
          ier=1024
          call prinf('w is wwwaaaaayyyy too small!',iff,0)
          return
        endif
c
        call ccn2all0(ier,k,eps,a,b,c,d,cfunc,par1,par2,par3,
     1       par4,numint,maxints,w(iab),w(iz),w(icoefs),w(iff),
     2       w(iwww),w(iw7),w(iw),keepw)
c
        w(7)=numint
c
c       now rearrange everything inside of w
c
        iab2=50
        lab2=6*numint+20
        call cn2copy(lab,w(iab),w(iab2))
c
c       store a copy from ch2dinit so it doesn't need to be called
c       in the evaluation routines
c
        a1=-1
        b1=-1
        c1=2
        ifinit=1
        call ch2dinit(a1,b1,c1,k,ifinit,w(iz),w(iwww))
c
        iwww2=iab2+lab2
        keepwww=2*k**2+6*k+100+1000
        call cn2copy(keepwww,w(iwww),w(iwww2))
c
        iw2=iwww2+keepwww
        lw2=keepw
        call cn2copy(lw2,w(iw),w(iw2))
c
        w(11)=iab2
        w(12)=lab2
        w(13)=iwww2
        w(14)=keepwww
        w(15)=iw2
        w(16)=lw2
c
c       save the step size in w to pull out individual grids
c
        keepw7=2*k*k+20
        w(17)=keepw7
c
        keepw2=iw2+lw2
        keepw=keepw2
        if (keepw .gt. lw) then
          ier=256
          call prinf('w is too small, but only at the end*',iff,0)
          return
        endif
c
        return
        end
c
c
c
c
c
        subroutine ccn2all0(ier,k,eps,a,b,c,d,cfunc,par1,par2,
     1       par3,par4,numint,maxints,abcds,z,coefs,f,
     2       www,w7,w,keepw)
        implicit real *8 (a-h,o-z)
        real *8 w(1),www(1),z(2,2*k,2*k),abcds(6,1),w7(1)
        complex *16 f(2*k,2*k),coefs(2*k,2*k),par1(1),par2(1),par3(1),
     1       par4(1)
        external cfunc
c
        keepw=0
c
c       initialize the expansion 2*k-point routine on [-1,1] x [-1,1]
c
        a1=-1
        b1=-1
        c1=2
        ifinit=1
        call ch2dinit(a1,b1,c1,2*k,ifinit,z,www)
c
c       now recursively divide [a,b] x [c,d] to find intervals where
c       a k-point expansion is good enough
c
        numint=1
        abcds(1,numint)=a
        abcds(2,numint)=b
        abcds(3,numint)=c
        abcds(4,numint)=d
        abcds(5,numint)=-7
        abcds(6,numint)=-7
c
        keepint=0
        do 3000 ijk=1,1000 000
c
        ifdone=1
        nnn=numint
        do 2400 i=1,nnn
c
c       if the ith interval has not been processed, process it and
c       mark it processed
c
        if (abcds(5,i) .gt. 0) goto 2400
c
        abcds(5,i)=1
        a2=abcds(1,i)
        b2=abcds(2,i)
        c2=abcds(3,i)
        d2=abcds(4,i)
c        
        call ccn2one(ier2,eps,a2,b2,c2,d2,k,cfunc,par1,par2,par3,par4,
     1       f,coefs,z,www,w7,keepw7)
cccc        call prinf('after ccn2one, ier2=*',ier2,1)
c
c       if the discritization is good, mark it and store it, along with
c       the interval number and location of coefs in w -
c       if it's bad, subdivide it into 4 pieces
c
        if (ier2 .eq. 0) then
            keepint=keepint+1
            abcds(6,i)=keepint+.1
c
            ikeepint=(keepint-1)*keepw7+1
            call cn2copy(keepw7,w7,w(ikeepint))
            keepw=keepw+keepw7
            abcds(5,i)=ikeepint+.1
c
            go to 2400
        endif
c
        if (ier2 .ne.0) then
c
            ifdone=0
            if (numint+4 .gt. maxints) then
                ier=64
                call prinf('maxints too small!!, ier=*',ier,1)
                return
            endif
c
            numint=numint+1
            abcds(1,numint)=a2
            abcds(2,numint)=(a2+b2)/2
            abcds(3,numint)=c2
            abcds(4,numint)=(c2+d2)/2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
            numint=numint+1
            abcds(1,numint)=a2
            abcds(2,numint)=(a2+b2)/2
            abcds(3,numint)=(c2+d2)/2
            abcds(4,numint)=d2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
            numint=numint+1
            abcds(1,numint)=(a2+b2)/2
            abcds(2,numint)=b2
            abcds(3,numint)=c2
            abcds(4,numint)=(c2+d2)/2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
            numint=numint+1
            abcds(1,numint)=(a2+b2)/2
            abcds(2,numint)=b2
            abcds(3,numint)=(c2+d2)/2
            abcds(4,numint)=d2
            abcds(5,numint)=-7
            abcds(6,numint)=-7
c
 2000   continue
        endif
c
 2400   continue
c
cccc        call prin2('abcds=*',abcds,6*numint)
        if (ifdone .eq. 1) goto 3200
c
 3000   continue
 3200   continue
c
        if (ifdone .eq. 0) then
          call prinf('ifdone is 0!!! bomb!!!*',a,0)
          ier=1024
          stop
          return
        endif
c
c       sort the boxes and throw away ones that had to be
c       subdivided
c
        iii=0
        do 4000 i=1,numint

        if (abcds(6,i) .gt. 0) then
            iii=iii+1
            abcds(1,iii)=abcds(1,i)
            abcds(2,iii)=abcds(2,i)
            abcds(3,iii)=abcds(3,i)
            abcds(4,iii)=abcds(4,i)
            abcds(5,iii)=abcds(5,i)
            abcds(6,iii)=abcds(6,i)
        endif
        
 4000   continue
c
        numint=iii
c
c       now sort the intervals according to the first coordinate,
c       leave the y intervals unsorted for now
c
        call cn2bubble(abcds,numint)
cccc        call prin2('after sorting, abcds=*',abcds,6*numint)
cccc        call cn2sorty(abcds,numint)        
c
        return
        end
c
c
c
c
c
        subroutine cn2copy(n,x,y)
        implicit real *8 (a-h,o-z)
        real *8 x(1),y(1)
c
        do 1200 i=1,n
        y(i)=x(i)
 1200   continue
c
        return
        end
c
c
c
c
c
        subroutine cn2bubble(ab,nn)
        implicit real *8 (a-h,o-z)
        save
        dimension ab(6,1)
c 
c       sort the array ab with respect to the first coordinate
c 
        do 2000 i=1,nn
        do 1200 j=1,i-1
        if(ab(1,i) .ge. ab(1,j) ) goto 1200
c
        d=ab(1,i)
        ab(1,i)=ab(1,j)
        ab(1,j)=d
c 
        d=ab(2,i)
        ab(2,i)=ab(2,j)
        ab(2,j)=d
c
        d=ab(3,i)
        ab(3,i)=ab(3,j)
        ab(3,j)=d
c
        d=ab(4,i)
        ab(4,i)=ab(4,j)
        ab(4,j)=d
c
        d=ab(5,i)
        ab(5,i)=ab(5,j)
        ab(5,j)=d
c
        d=ab(6,i)
        ab(6,i)=ab(6,j)
        ab(6,j)=d
c
 1200 continue
 2000 continue
        return
        end
c 
c 
c 
c 
c
        subroutine rcn2one(ier,eps,a,b,c,d,k,rfunc,par1,par2,par3,par4,
     1       f,coefs,z,www,w,keepw)
        implicit real *8 (a-h,o-z)
        real *8 w(1),z(2,2*k,2*k),www(1)
        real *8 par1(1),par2(1),par3(1),par4(1),val,f(2*k,2*k),
     1       coefs(2*k,2*k),val2,val3,derx,dery
c
c       test the k-point chebyshev expansion of rfunc on [a,b] x [c,d]
c       and see if it is integrated/interpolated to accuracy eps
c
c       input:
c           eps - the precision to which we demand rfunc to be
c                 interpolated on the rectangle
c           a,b,c,d - the coordinates of the rectangle [a,b] x [c,d]
c           rfunc - user defind real subroutine which must have the
c                 calling sequence rfunc(x,y,par1,par2,par3,par4,val)
c           www - must have been created from a call to ch2init with
c                 a=-1, b=-1, c=2, and n=2*k
c
c       output:
c           ier - ier=0 is good - ier not 0 means function was not
c                 interpolated to precision eps on the rectangle
c           f   - contains the function values at the (2*k)**2 tensor
c                 product chebyshev nodes
c           coefs - contains the (2*k)**2 expansion coefficients of
c                 f on the recntangle
c           z   - contains (2*k)**2 chebyshev tensor product nodes
c           w   - contains information on a k**2 point expansion of
c                 rfunc
c           keepw - the first keepw real *8 elements of w should not
c                 be changed
c
c       we always need to scale our function to fit on the square to
c       work properly with ch2dinit.f
c
        a1=-1
        b1=-1
        c1=2
c
c       now evaluate our function at every point in z
c
        n=2*k
        do 2000 i=1,n
        do 1800 j=1,n
        x=a+(z(1,j,i)-a1)/c1*(b-a)
        y=c+(z(2,j,i)-b1)/c1*(d-c)
        call rfunc(x,y,par1,par2,par3,par4,val)
        f(j,i)=val
 1800   continue
 2000   continue
c
c       now get the real coefficients of the function f
c
        call ch2dexp(f,n,coefs,www)
c
c       check the last half of the coefficients to see how
c       accurate the k-point expansion is
c
        eps2=eps**2
        dd=0
        dd0=0
        do 2400 i=1,n
        do 2200 j=1,n
        if (i .gt. k .or. j .gt. k) dd=dd+coefs(j,i)*(coefs(j,i))
        if (i .le. k .and. j .le. k) 
     1       dd0=dd0+coefs(j,i)*(coefs(j,i))
 2200   continue
 2400   continue
c
        ier=0
cccc        call prin2('dd=*',dd,1)
cccc        call prin2('eps2=*',eps2,1)
        if (dd .gt. eps2) then
cccc        if (dd .gt. dd0*eps2) then
          ier=16
          return
        endif
c
c       if we're here, store the interval and coefs in w
c
        keepw=0
        w(1)=k+.1
        w(2)=a
        w(3)=b
        w(4)=c
        w(5)=d
c
        icoefs=10
        lcoefs=k*k+10
c
        call rcpcoefs(k,coefs,w(10))
        keepw=icoefs+lcoefs+10
c
        return
        end
c
c
c
c
c
        subroutine ccn2one(ier,eps,a,b,c,d,k,cfunc,par1,par2,par3,par4,
     1       f,coefs,z,www,w,keepw)
        implicit real *8 (a-h,o-z)
        real *8 w(1),z(2,2*k,2*k),www(1)
        complex *16 par1(1),par2(1),par3(1),par4(1),val,f(2*k,2*k),
     1       coefs(2*k,2*k),val2,val3,derx,dery
c
c       test the k-point chebyshev expansion of rfunc on [a,b] x [c,d]
c       and see if it is integrated/interpolated to accuracy eps
c
c       input:
c           eps - the precision to which we demand rfunc to be
c                 interpolated on the rectangle
c           a,b,c,d - the coordinates of the rectangle [a,b] x [c,d]
c           rfunc - user defind real subroutine which must have the
c                 calling sequence rfunc(x,y,par1,par2,par3,par4,val)
c           www - must have been created from a call to ch2init with
c                 a=-1, b=-1, c=2, and n=2*k
c
c       output:
c           ier - ier=0 is good - ier not 0 means function was not
c                 interpolated to precision eps on the rectangle
c           f   - contains the function values at the (2*k)**2 tensor
c                 product chebyshev nodes
c           coefs - contains the (2*k)**2 expansion coefficients of
c                 f on the recntangle
c           z   - contains (2*k)**2 chebyshev tensor product nodes
c           w   - contains information on a k**2 point expansion of
c                 rfunc
c           keepw - the first keepw real *8 elements of w should not
c                 be changed
c
c       we always need to scale our function to fit on the square to
c       work properly with ch2dinit.f
c
        a1=-1
        b1=-1
        c1=2
c
c       now evaluate our function at every point in z
c
        n=2*k
        do 2000 i=1,n
        do 1800 j=1,n
        x=a+(z(1,j,i)-a1)/c1*(b-a)
        y=c+(z(2,j,i)-b1)/c1*(d-c)
        call cfunc(x,y,par1,par2,par3,par4,val)
        f(j,i)=val
 1800   continue
 2000   continue
c
c       now get the complex coefficients of the function f
c
cccc        call prin2('f=*',f,n*n)
        call cch2dexp(f,n,coefs,www)
c
c       check the last half of the coefficients to see how
c       accurate the k-point expansion is
c
        eps2=eps**2
        dd=0
        dd0=0
        do 2400 i=1,n
        do 2200 j=1,n
        if (i .gt. k .or. j .gt. k) dd=dd+coefs(j,i)*conjg(coefs(j,i))
        if (i .le. k .and. j .le. k) 
     1       dd0=dd0+coefs(j,i)*conjg(coefs(j,i))
 2200   continue
 2400   continue
c
        ier=0
        call prin2('dd=*',dd,1)
cccc        call prin2('eps2=*',eps2,1)
        if (dd .gt. eps2) then
cccc        if (dd .gt. dd0*eps2) then
          ier=16
          return
        endif
c
c       if we're here, store the interval and coefs in w
c
        keepw=0
        w(1)=k+.1
        w(2)=a
        w(3)=b
        w(4)=c
        w(5)=d
c
        icoefs=10
        lcoefs=k*k*2+10
c
        call ccpcoefs(k,coefs,w(10))
        keepw=icoefs+lcoefs+10
c
        return
        end
c
c
c
c
c
        subroutine ccpcoefs(k,coefs,w)
        implicit real *8 (a-h,o-z)
        complex *16 coefs(2*k,2*k),w(k,k)
c
c       copy k**2 complex coefficients from (2*k)**2 coefficients
c
        do 1400 i=1,k
        do 1200 j=1,k
        w(j,i)=coefs(j,i)
 1200   continue
 1400   continue
c
        return
        end
c
c
c
c
c
        subroutine rcpcoefs(k,coefs,w)
        implicit real *8 (a-h,o-z)
        real *8 coefs(2*k,2*k),w(k,k)
c
c       copy k**2 real coefficients from (2*k)**2 coefficients
c
        do 1400 i=1,k
        do 1200 j=1,k
        w(j,i)=coefs(j,i)
 1200   continue
 1400   continue
c
        return
        end
