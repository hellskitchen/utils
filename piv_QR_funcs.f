c This file contains four functions for computing a column pivoted
c QR factorization of a given matrix A:
c
c    piv_QR3r    REAL    arithmetic, PRESET nr of columns
c    piv_QR3c    COMPLEX arithmetic, PRESET nr of columns
c    piv_QR4r    REAL    arithmetic, VARIABLE nr of columns
c    piv_QR4c    COMPLEX arithmetic, VARIABLE nr of columns
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE piv_QR3r(Q, m, n, k, R, indvec)

c     Fixed depth pivoted QR factorization of a REAL matrix A:
c        A(:, indvec) = Q*R   (approximately)
c
c     INPUT:  A(m,n)     matrix that is stored in the array Q
c             k          the preset depth of the factorization
c
c     OUTPUT: Q(m,k)     matrix with orthonormal columns
c             R(k,n)     coefficient matrix
c             indvec(n)  permutation vector

      IMPLICIT NONE
      INTEGER m, n, k, i, j, linind
      INTEGER jmax, itmp, ind1, ind2, indvec(n)
      REAL*8 colmax, tmp, rjj, rij
      REAL*8 Q(m,n), R(k,n)
      CHARACTER*80 string

c     Set indvec = 1,2,...,n
      DO 900, i = 1,n
        indvec(i) = i
 900  CONTINUE

      DO 1000, j = 1,k

c       Set the leading entries of R to zero.
        DO 1050, i = 1,(j-1)
          R(j,i) = 0.0d0
 1050   CONTINUE

c       Find the pivot.
        jmax = j     
        CALL colnorm(Q, m, n, j, colmax)
        DO 1200, i = j+1,n
          CALL colnorm(Q, m, n, i, tmp)
          IF (tmp .GT. colmax) THEN
            colmax = tmp
            jmax = i
          END IF
 1200   CONTINUE

c       Swap the largest column with column nr j.
        CALL swapcols(Q,m,n,jmax,j)
        CALL swapcols(R,k,n,jmax,j)

c       Update the index vector.
        itmp         = indvec(jmax)
        indvec(jmax) = indvec(j)
        indvec(j)    = itmp

c       Set the pivot and normalize the j'th column.
        rjj    = colmax
        R(j,j) = rjj
        CALL scalecol(Q, m, n, j, 1/rjj)

c       Do the "superfluous" orthogonalization 
c       of Q(:,j) w.r.t. Q(:,1:(j-1))
        DO 1600, i = 1,(j-1)
          CALL orthogcol(Q, m, n, i, j, tmp)
 1600   CONTINUE
        CALL colnorm( Q, m, n, j,   rjj)     
        CALL scalecol(Q, m, n, j, 1/rjj)

c       Orthogonalize Q(:,(j+1):n) w.r.t Q(:,j)
        DO 1800, i = (j+1),n
          CALL orthogcol(Q, m, n, j, i, rij)
          R(j,i) = rij
 1800   CONTINUE

 1000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE piv_QR4r(Q, m, n, acc, k, R, indvec)

c     Variable depth pivoted QR factorization of a REAL matrix A:
c        A(:, indvec) = Q*R   (approximately)
c
c     INPUT:  A(m,n)     matrix that is stored in the array Q
c             acc        the accuracy that must be met
c
c     OUTPUT: k          the depth of the factorization
c             Q(m,k)     matrix with orthonormal columns
c             R(k,n)     coefficient matrix
c             indvec(n)  permutation vector

      IMPLICIT NONE
      INTEGER m, n, k, i, j, k_new, linind
      INTEGER jmax, itmp, breakflag, ind1, ind2, indvec(n)
      REAL*8 colmax, tmp, rjj, rij
      REAL*8 Q(m,n), R(k,n), acc, pivsum
      CHARACTER*80 string

c     Set indvec = 1,2,...,n
      DO 900, i = 1,n
        indvec(i) = i
 900  CONTINUE

      breakflag = 0
      DO 1000, j = 1,k

c       Set the leading entries of R to zero.
        DO 1050, i = 1,(j-1)
          R(j,i) = 0.0d0
 1050   CONTINUE

c       Find the pivot.
        jmax = j     
        CALL colnorm(Q, m, n, j, colmax)
        pivsum = colmax*colmax     
        DO 1200, i = j+1,n
          CALL colnorm(Q, m, n, i, tmp)
          pivsum = pivsum + tmp*tmp
          IF (tmp .GT. colmax) THEN
            colmax = tmp
            jmax = i
          END IF
 1200   CONTINUE

c       Check to see if the sum of pivots is smaller than acc.
        IF (pivsum .LT. (acc*acc)) THEN
          breakflag = 1
          GOTO 1005
        END IF

c       Swap the largest column with column nr j.
        CALL swapcols(Q,m,n,jmax,j)
        CALL swapcols(R,k,n,jmax,j)

c       Update the index vector.
        itmp         = indvec(jmax)
        indvec(jmax) = indvec(j)
        indvec(j)    = itmp

c       Set the pivot and normalize the j'th column.
        rjj    = colmax
        R(j,j) = rjj
        CALL scalecol(Q, m, n, j, 1/rjj)

c       Do the "superfluous" orthogonalization 
c       of Q(:,j) w.r.t. Q(:,1:(j-1))
        DO 1600, i = 1,(j-1)
          CALL orthogcol(Q, m, n, i, j, tmp)
 1600   CONTINUE
        CALL colnorm( Q, m, n, j,   rjj)     
        CALL scalecol(Q, m, n, j, 1/rjj)

c       Orthogonalize Q(:,(j+1):n) w.r.t Q(:,j)
        DO 1800, i = (j+1),n
          CALL orthogcol(Q, m, n, j, i, rij)
          R(j,i) = rij
 1800   CONTINUE

 1000 CONTINUE
 1005 CONTINUE

c     If the loop was broken, we need to update k and
c     reshuffle R to reflect the new leading index
      IF (breakflag .EQ. 1) THEN
        k_new = j-1
        DO 2000, j = 1,n
          DO 2100, i = 1,k_new
            linind = (j-1)*k_new + i
            ind1 = mod(linind-1, k)+1
            ind2 = (linind - ind1)/k + 1
c            write(string, '(i8,i8,i8,i8)') i, j, ind1, ind2
c            CALL mexWarnMsgTxt(string)
            R(ind1,ind2) = R(i,j)
 2100     CONTINUE
 2000   CONTINUE
        k = k_new
      END IF

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE piv_QR3c(Qr, Qi, m, n, k, Rr, Ri, indvec)

c     Fixed depth pivoted QR factorization of a COMPLEX matrix A:
c        A(:, indvec) = Q*R   (approximately)
c
c     INPUT:  Ar(m,n)    real part of matrix that is stored in the array Qr
c             Ai(m,n)    imag part of matrix that is stored in the array Qi
c             k          the preset depth of the factorization
c
c     OUTPUT: Qr(m,k)    real part of matrix with orthonormal columns
c             Qi(m,k)    imag part of matrix with orthonormal columns
c             Rr(k,n)    real part of coefficient matrix
c             Ri(k,n)    imag part of coefficient matrix
c             indvec(n)  permutation vector

      IMPLICIT NONE
      INTEGER m, n, k, i, j, linind
      INTEGER jmax, itmp, ind1, ind2, indvec(n)
      REAL*8 colmax, tmp, rjj, rij, tmp1, tmp2
      REAL*8 Qr(m,n), Qi(m,n), Rr(k,n), Ri(k,n)
      CHARACTER*80 string

c     Set indvec = 1,2,...,n
      DO 900, i = 1,n
        indvec(i) = i
 900  CONTINUE

      DO 1000, j = 1,k

c       Set the leading entries of R to zero.
        DO 1050, i = 1,(j-1)
          Rr(j,i) = 0.0d0
          Ri(j,i) = 0.0d0
 1050   CONTINUE

c       Find the pivot.
        jmax = j     
        CALL colnorm(Qr, m, n, j, tmp1)
        CALL colnorm(Qi, m, n, j, tmp2)
        colmax = sqrt(tmp1*tmp1 + tmp2*tmp2)
        DO 1200, i = j+1,n
          CALL colnorm(Qr, m, n, i, tmp1)
          CALL colnorm(Qi, m, n, i, tmp2)
          tmp = sqrt(tmp1*tmp1 + tmp2*tmp2)
          IF (tmp .GT. colmax) THEN
            colmax = tmp
            jmax = i
          END IF
 1200   CONTINUE

c       Swap the largest column with column nr j.
        CALL swapcols(Qr,m,n,jmax,j)
        CALL swapcols(Qi,m,n,jmax,j)
        CALL swapcols(Rr,k,n,jmax,j)
        CALL swapcols(Ri,k,n,jmax,j)

c       Update the index vector.
        itmp         = indvec(jmax)
        indvec(jmax) = indvec(j)
        indvec(j)    = itmp

c       Set the pivot and normalize the j'th column.
        rjj     = colmax
        Rr(j,j) = rjj
        Ri(j,j) = 0
        CALL scalecol(Qr, m, n, j, 1/rjj)
        CALL scalecol(Qi, m, n, j, 1/rjj)

c       Do the "superfluous" orthogonalization 
c       of Q(:,j) w.r.t. Q(:,1:(j-1))
        DO 1600, i = 1,(j-1)
          CALL orthogcolc(Qr, Qi, m, n, i, j, tmp1, tmp2)
 1600   CONTINUE
        CALL colnorm(Qr, m, n, j,   tmp1)     
        CALL colnorm(Qi, m, n, j,   tmp2)     
        rjj = sqrt(tmp1*tmp1 + tmp2*tmp2)
        CALL scalecol(Qr, m, n, j, 1/rjj)
        CALL scalecol(Qi, m, n, j, 1/rjj)

c       Orthogonalize Q(:,(j+1):n) w.r.t Q(:,j)
        DO 1800, i = (j+1),n
          CALL orthogcolc(Qr, Qi, m, n, j, i, tmp1, tmp2)
          Rr(j,i) = tmp1
          Ri(j,i) = tmp2
 1800   CONTINUE

 1000 CONTINUE

      RETURN
      END

ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      SUBROUTINE piv_QR4c(Qr, Qi, m, n, acc, k, Rr, Ri, indvec)

c     Variable depth pivoted QR factorization of a COMPLEX matrix A:
c        A(:, indvec) = Q*R   (approximately)
c
c     INPUT:  Ar(m,n)    real part of matrix that is stored in the array Qr
c             Ai(m,n)    imag part of matrix that is stored in the array Qi
c             acc        the accuracy that must be met
c
c     OUTPUT: k          the depth of the factorization
c             Qr(m,k)    real part of matrix with orthonormal columns
c             Qi(m,k)    imag part of matrix with orthonormal columns
c             Rr(k,n)    real part of coefficient matrix
c             Ri(k,n)    imag part of coefficient matrix
c             indvec(n)  permutation vector

      IMPLICIT NONE
      INTEGER m, n, k, i, j, k_new, linind
      INTEGER jmax, itmp, breakflag, ind1, ind2, indvec(n)
      REAL*8 colmax, tmp, rjj, rij, tmp1, tmp2
      REAL*8 Qr(m,n), Qi(m,n), Rr(k,n), Ri(k,n), acc, pivsum
      CHARACTER*80 string

c     Set indvec = 1,2,...,n
      DO 900, i = 1,n
        indvec(i) = i
 900  CONTINUE

      breakflag = 0
      DO 1000, j = 1,k

c       Set the leading entries of R to zero.
        DO 1050, i = 1,(j-1)
          Rr(j,i) = 0.0d0
          Ri(j,i) = 0.0d0
 1050   CONTINUE

c       Find the pivot.
        jmax = j     
        CALL colnorm(Qr, m, n, j, tmp1)
        CALL colnorm(Qi, m, n, j, tmp2)
        colmax = sqrt(tmp1*tmp1 + tmp2*tmp2)
        pivsum = colmax*colmax
        DO 1200, i = j+1,n
          CALL colnorm(Qr, m, n, i, tmp1)
          CALL colnorm(Qi, m, n, i, tmp2)
          tmp = sqrt(tmp1*tmp1 + tmp2*tmp2)
          pivsum = pivsum + tmp*tmp
          IF (tmp .GT. colmax) THEN
            colmax = tmp
            jmax = i
          END IF
 1200   CONTINUE

c       Check to see if the sum of pivots is smaller than acc.
        IF (pivsum .LT. (acc*acc)) THEN
          breakflag = 1
          GOTO 1005
        END IF

c       Swap the largest column with column nr j.
        CALL swapcols(Qr,m,n,jmax,j)
        CALL swapcols(Qi,m,n,jmax,j)
        CALL swapcols(Rr,k,n,jmax,j)
        CALL swapcols(Ri,k,n,jmax,j)

c       Update the index vector.
        itmp         = indvec(jmax)
        indvec(jmax) = indvec(j)
        indvec(j)    = itmp

c       Set the pivot and normalize the j'th column.
        rjj     = colmax
        Rr(j,j) = rjj
        Ri(j,j) = 0
        CALL scalecol(Qr, m, n, j, 1/rjj)
        CALL scalecol(Qi, m, n, j, 1/rjj)

c       Do the "superfluous" orthogonalization 
c       of Q(:,j) w.r.t. Q(:,1:(j-1))
        DO 1600, i = 1,(j-1)
          CALL orthogcolc(Qr, Qi, m, n, i, j, tmp1, tmp2)
 1600   CONTINUE
        CALL colnorm(Qr, m, n, j,   tmp1)     
        CALL colnorm(Qi, m, n, j,   tmp2)     
        rjj = sqrt(tmp1*tmp1 + tmp2*tmp2)
        CALL scalecol(Qr, m, n, j, 1/rjj)
        CALL scalecol(Qi, m, n, j, 1/rjj)

c       Orthogonalize Q(:,(j+1):n) w.r.t Q(:,j)
        DO 1800, i = (j+1),n
          CALL orthogcolc(Qr, Qi, m, n, j, i, tmp1, tmp2)
          Rr(j,i) = tmp1
          Ri(j,i) = tmp2
 1800   CONTINUE

 1000 CONTINUE
 1005 CONTINUE

c     If the loop was broken, we need to update k and
c     reshuffle R to reflect the new leading index
      IF (breakflag .EQ. 1) THEN
        k_new = j-1
        DO 2000, j = 1,n
          DO 2100, i = 1,k_new
            linind = (j-1)*k_new + i
            ind1 = mod(linind-1, k)+1
            ind2 = (linind - ind1)/k + 1
c            write(string, '(i8,i8,i8,i8)') i, j, ind1, ind2
c            CALL mexWarnMsgTxt(string)
            Rr(ind1,ind2) = Rr(i,j)
            Ri(ind1,ind2) = Ri(i,j)
 2100     CONTINUE
 2000   CONTINUE
        k = k_new
      END IF

      RETURN
      END


