
void ztranspose_(int *m, int *n, double _Complex *a);

void dinverse_(int *n, double *a, int *info, double *ainv);

void dmatvec_(int *m, int *n, double *a, double *x, double *y);

void zmatvec_(int *m, int *n, double _Complex *a, double _Complex *x,
              double _Complex *y);

void zmatmat_(int *m, int *n, double _Complex *a, int *k,
              double _Complex *b, double _Complex *c);

void dcopy_(int *n, double *a, int *incx, double *x, int *incy);
